<?PHP

/*
	====================================================================


	====================================================================

	====================================================================
*/


/**
 * @desc this class will handle all MySQL queries. It's built in error
 *		 checking can be disabled by setting the 2nd paramater to "no"
 *		 on the functions query and fetchArr. Example:
 *				$sqldb->query('SELECT * FROM users', 'no');
 * 				$sqldb->fetchArr('SELECT * FROM mail', 'no');
 *				$sqldb->fetchObj('SELECT id, lname FROM users', 'no');
 * 		 This way you can check for errors yourself. Otherwise the error
 *	 	 function is called and so is die().
 */
class sqldb
{

    /**
     * @return VOID
     * @desc	Constructor. Gets MySQL info from config.php
     */
    function sqldb()
    {

	$this->HOST 	= HOST;
	$this->USER 	= USER;
	$this->PASS 	= PASS;
	$this->DB   	= DB;


    }


    /***************************************************************************************************
     * @return VOID
     * @param STRING
     * @param STRING
     * @param INT
     * @desc error() will make a simple page that displays the error,
     *		 the error number, and the SQL query that caused the error.
     *		 This function is always called by other member functions
     *		 when there was an error, unless the user specifically says
     *		 not to.
     ***************************************************************************************************/
    function error($SQL, $ERROR, $NUM)
    {
        echo "<b>MySQL has encountered an Error when trying to process:</b> <br />";
        echo $SQL."<br />";
        echo "MySQL Reported the following:</b> <br />";
        echo $ERROR;
        echo "Error Number:</b><br>";
        echo $NUM;
        die();
        return;
    }


    /***************************************************************************************************
     * @return RESOURCE
     * @param STRING
     * @param STRING
     * @desc will connect to MySQL, run the query, close connection
     *		 then see if an error happened. will return the raw value
     *		 of a mysql_query.
     ***************************************************************************************************/
    function query($SQL, $ERR = "yes")
    {
        $db = mysql_connect($this->HOST, $this->USER, $this->PASS)
        	or $this->error("Connection", mysql_error(), mysql_errno());
        if(!mysql_select_db($this->DB, $db)) {
	        $this->error("Selecting Database Error", mysql_error(), mysql_errno());
	        die();
        }

         mysql_query("SET NAMES 'utf8'");

         $result = mysql_query($SQL);

        if(mysql_error()) {
            if($ERR == "no") { // this will only happen if the user explicitly wants it.
                mysql_close($db);
                return FALSE;
            }
            $error = mysql_error();
            $errno = mysql_errno();
            mysql_close($db);
	$this->error($SQL, $error, $errno); // defalut is to die and give an error message.
            die();
        }
        mysql_close($db);
        return($result);
    }
    /***************************************************************************************************
     * @return RESOURCE
     * @param STRING
     * @param STRING
     * @desc = same as query above except this returns the insert_id
     *		rather than the result resource
     *		 
     ***************************************************************************************************/
    function insert($SQL, $ERR = "yes")
    {
        $db = mysql_connect($this->HOST, $this->USER, $this->PASS)
        	or $this->error("Connection", mysql_error(), mysql_errno());
        if(!mysql_select_db($this->DB, $db)) {
	        $this->error("Selecting Database Error", mysql_error(), mysql_errno());
	        die();
        }

         mysql_query("SET NAMES 'utf8'");

         $result = mysql_query($SQL);
         $insert_id = mysql_insert_id();
        if(mysql_error()) {
            if($ERR == "no") { // this will only happen if the user explicitly wants it.
                mysql_close($db);
                return FALSE;
            }
            $error = mysql_error();
            $errno = mysql_errno();
            mysql_close($db);
	$this->error($SQL, $error, $errno); // defalut is to die and give an error message.
            die();
        }
        mysql_close($db);
        return($insert_id);
    }
    /***************************************************************************************************
     * @return OBJECT
     * @param STRING
     * @param STRING
     * @desc getNumRows, Purpose: Return row count, MySQL version
     ***************************************************************************************************/
function getNumRows($result){
	return mysql_num_rows($result);
}
function getNumRowsObj($result){
	return count($result);
}

    /***************************************************************************************************
     * @return OBJECT
     * @param STRING
     * @param STRING
     * @desc same idea as fetchArr, but will return an object
     ***************************************************************************************************/
    function fetchSingleObj($SQL, $ERR = "yes")
    {
        $db = mysql_connect($this->HOST, $this->USER, $this->PASS)
        	or die($this->error("Connection", mysql_error(), mysql_errno()));
        if(!mysql_select_db($this->DB, $db)) {
	        $this->error("Selecting Database Error", mysql_error(), mysql_errno());
	        die();
        }
         mysql_query("SET NAMES 'utf8'");
        $result = mysql_query($SQL);

        if(mysql_error()) {
            if($ERR == "no") { // this will only happen if the user explicitly wants it.
                mysql_close($db);
                return FALSE;
            }
            $error = mysql_error();
            $errno = mysql_errno();
            mysql_close($db);
	$this->error($SQL, $error, $errno); // defalut is to die and give an error message.
            die();
        }
        mysql_close($db);
        $result = mysql_fetch_object($result);
        return($result);
    }

    /***************************************************************************************************
     * @return ARRAY
     * @param STRING
     * @param STRING
     * @desc given an SQL query, this function will execute both
     *		 mysql_query() and then mysql_fetch_array() and return the
     *		 results of the latter
     ***************************************************************************************************/
    function fetchSingleArr($SQL, $ERR = "yes")
    {
        $db = mysql_connect($this->HOST, $this->USER, $this->PASS)
        	or die($this->error("Connection", mysql_error(), mysql_errno()));
        if(!mysql_select_db($this->DB, $db))
		{
		$this->error("Selecting Database Error", mysql_error(), mysql_errno());
	        	die();
        		}
         mysql_query("SET NAMES 'utf8'");
        $result = mysql_query($SQL);

        if(mysql_error()) 
	{
            	if($ERR == "no") { // this will only happen if the user explicitly wants it.
                mysql_close($db);
                return FALSE;
            	}
            $error = mysql_error();
            $errno = mysql_errno();
            mysql_close($db);
			$this->error($SQL, $error, $errno); // defalut is to die and give an error message.
            die();
        }
        mysql_close($db);
      return mysql_fetch_assoc($result);
    }

  /***************************************************************************************************
     * @return OBJECT
     * @param STRING
     * @param STRING
     * @desc same idea as fetchArr, but will return an object
     ***************************************************************************************************/
    function fetchObj($SQL, $ERR = "yes")
    {
        $db = mysql_connect($this->HOST, $this->USER, $this->PASS)
        	or die($this->error("Connection", mysql_error(), mysql_errno()));
        if(!mysql_select_db($this->DB, $db)) {
	        $this->error("Selecting Database Error", mysql_error(), mysql_errno());
	        die();
        }
         mysql_query("SET NAMES 'utf8'");
        $result = mysql_query($SQL);

        if(mysql_error()) {
            if($ERR == "no") { // this will only happen if the user explicitly wants it.
                mysql_close($db);
                return FALSE;
            }
            $error = mysql_error();
            $errno = mysql_errno();
            mysql_close($db);
	$this->error($SQL, $error, $errno); // defalut is to die and give an error message.
            die();
        }
        mysql_close($db);
       // $result = mysql_fetch_object($result);
        $count = 0;
        while($row = mysql_fetch_object($result))
            {

             $returnArr[$count] = $row;
             $count++;

            }
        return($returnArr);
    }

    /***************************************************************************************************
     * @return ARRAY
     * @param STRING
     * @param STRING
     * @desc given an SQL query, this function will execute both
     *		 mysql_query() and then mysql_fetch_array() and return the
     *		 results of the latter
     ***************************************************************************************************/
    function fetchArr($SQL, $ERR = "yes")
    {
        $db = mysql_connect($this->HOST, $this->USER, $this->PASS)
        	or die($this->error("Connection", mysql_error(), mysql_errno()));
        if(!mysql_select_db($this->DB, $db))
		{
		$this->error("Selecting Database Error", mysql_error(), mysql_errno());
	        	die();
        		}
         mysql_query("SET NAMES 'utf8'");
        $result = mysql_query($SQL);

        if(mysql_error()) 
	{
            	if($ERR == "no") { // this will only happen if the user explicitly wants it.
                mysql_close($db);
                return FALSE;
            	}
            $error = mysql_error();
            $errno = mysql_errno();
            mysql_close($db);
			$this->error($SQL, $error, $errno); // defalut is to die and give an error message.
            die();
        }
        mysql_close($db);
       // $result = mysql_fetch_object($result);
        $count = 0;
        while($row = mysql_fetch_assoc($result))
            {

             $returnArr[$count] = $row;
             $count++;

            }
        return($returnArr);
    }
} // #### END SQLDB CLASS <<#######################


?>
