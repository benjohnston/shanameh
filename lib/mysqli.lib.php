<?php
class DB {
    public function __construct($user, $password, $database, $host = 'localhost') {
	$this->user = $user;
	$this->password = $password;
	$this->database = $database;
	$this->host = $host;
	}
    protected function connect() {
	return new mysqli($this->host, $this->user, $this->password, $this->database);
	}

    public function insert($sql) {
	$db = $this->connect();
	if($stmt = $db->prepare($sql)) { 
	  $stmt->execute();
	  if ( $stmt->affected_rows ) {
		return $stmt->insert_id;
	  }
	}
	else { die('could not prepare statement'); }
	return false;
}


    public function query($sql) {
	$db = $this->connect();
	$stmt = $db->prepare($sql);
	$stmt->execute();
	
	if ( $stmt->affected_rows ) {
		return true;
	}
	return false;
}
    public function fetchSingleArr($query) {
	$db = $this->connect();
	if($stmt = $db->prepare($query)) {
	   $stmt->execute();
	   if($result = $stmt->get_result()) {
	     return $result->fetch_array();
	   }
	   else { return false; }
	}
}

    public function fetchArr($query) {
	$db = $this->connect();
	if($stmt = $db->prepare($query)) {
	   $stmt->execute();
	   $result = $stmt->get_result();
	   while ($row = $result->fetch_array()) {
		$results[] = $row;
	   }
	   return $results;
	}
}

    public function fetchSingleObj($query) {
	$db = $this->connect();
	if($stmt = $db->prepare($query)) {
	   $stmt->execute();
	   if($result = $stmt->get_result()) {
	     return $result->fetch_object();
	   }
	   else { return false; }
	}
}

    public function fetchObj($query) {
	$db = $this->connect();
	if($stmt = $db->prepare($query)) {
	   $stmt->execute();
	   $result = $stmt->get_result();
	   if($result->num_rows != 0)
	   {
	   while ($row = $result->fetch_object()) {
		$results[] = $row;
	   }
	   return $results;
	   }
	}
}

} // end class
