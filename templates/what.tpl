{mask:main}<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html lang="en"> <!--<![endif]-->
<head>

	<!-- Basic Page Needs
  ================================================== -->
	<meta charset="utf-8">
	<title>{header}</title>
	<meta name="description" content="">
	<meta name="author" content="">

	<!-- Mobile Specific Metas
  ================================================== -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<!-- CSS
  ================================================== -->

	<link rel="stylesheet" href="css/{css}">	

	<!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

	<!-- Favicons
	================================================== -->
	<link rel="shortcut icon" href="images/favicon.ico">

<style>
td {
  border:solid 1px black;
}
.content {
  width:700px;
  margin:0 auto;
}

</style>

</head>
<body>



	<div class="content" style="border:solid 1px green;">
	<h1>View of All	Available Manuscript Pages, Listed by Dynasty, Shah and Episode</h1>
	
	<table border="1" cellpadding="2" cellspacing="0">
	<TR>
	<TD COLSPAN="2" STYLE="font-weight:bold;color:white;background-color:black;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860197" STYLE="color:white;" TARGET="almagest">1  PISHDADIAN</A>
	</TD>
</TR>

<TR>
	<TD COLSPAN="2" STYLE="font-weight:bold;color:white;background-color:gray;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860199" STYLE="color:white;" TARGET="almagest">1.1 KAYUMARS</A>
	</TD>
</TR>

<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860201" TARGET="almagest" NAME="01.01.01.01.01"> <SPAN STYLE="color:green;font-size:9pt;">
			[01.01.01.01.01]
		</SPAN>
		   <SPAN>
			 The Greatness of Kayumars and the Envy of Ahriman
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792828,'57G-4-2','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792828&t=" ALT="Kayumars instructs son to destroy divs [57G-4-2]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=868532" TARGET="almagest">57G-4-2</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860205" TARGET="almagest" NAME="01.01.01.01.02"> <SPAN STYLE="color:green;font-size:9pt;">
			[01.01.01.01.02]
		</SPAN>
		   <SPAN>
			 Siyamak is killed by the Div
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860208" TARGET="almagest" NAME="01.01.01.01.03"> <SPAN STYLE="color:green;font-size:9pt;">
			[01.01.01.01.03]
		</SPAN>
		   <SPAN>
			 Hushang and Kayumars go to fight the Black Div
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR>
	<TD COLSPAN="2" STYLE="font-weight:bold;color:white;background-color:gray;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860211" STYLE="color:white;" TARGET="almagest">1.2 HUSHANG</A>
	</TD>
</TR>

<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860213" TARGET="almagest" NAME="01.02.01.01.01"> <SPAN STYLE="color:green;font-size:9pt;">
			[01.02.01.01.01]
		</SPAN>
		   <SPAN>
			 The Accession of Hushang and his civilising Arts
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860216" TARGET="almagest" NAME="01.02.01.01.02"> <SPAN STYLE="color:green;font-size:9pt;">
			[01.02.01.01.02]
		</SPAN>
		   <SPAN>
			 The Feast of Sadah is founded
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR>
	<TD COLSPAN="2" STYLE="font-weight:bold;color:white;background-color:gray;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860218" STYLE="color:white;" TARGET="almagest">1.3 TAHMURAS</A>
	</TD>
</TR>

<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860220" TARGET="almagest" NAME="01.03.01.01.01"> <SPAN STYLE="color:green;font-size:9pt;">
			[01.03.01.01.01]
		</SPAN>
		   <SPAN>
			 Tahmuras ascends the Throne, invents new Arts, subdues the Divs, and dies
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR>
	<TD COLSPAN="2" STYLE="font-weight:bold;color:white;background-color:gray;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860224" STYLE="color:white;" TARGET="almagest">1.4 JAMSHID</A>
	</TD>
</TR>

<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860226" TARGET="almagest" NAME="01.04.01.01.01"> <SPAN STYLE="color:green;font-size:9pt;">
			[01.04.01.01.01]
		</SPAN>
		   <SPAN>
			 The Greatness and Fall of Jamshid
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792885,'58G-10-2','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792885&t=" ALT="Jamshid enthroned [58G-10-2]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=869472" TARGET="almagest">58G-10-2</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860231" TARGET="almagest" NAME="01.04.01.01.02"> <SPAN STYLE="color:green;font-size:9pt;">
			[01.04.01.01.02]
		</SPAN>
		   <SPAN>
			 The Story of Zahhak and his Father
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860234" TARGET="almagest" NAME="01.04.01.01.03"> <SPAN STYLE="color:green;font-size:9pt;">
			[01.04.01.01.03]
		</SPAN>
		   <SPAN>
			 Iblis as Cook
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860236" TARGET="almagest" NAME="01.04.01.01.04"> <SPAN STYLE="color:green;font-size:9pt;">
			[01.04.01.01.04]
		</SPAN>
		   <SPAN>
			 The Fortunes of Jamshid go to Wrack
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792833,'57G-8-1','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792833&t=" ALT="Jamshid halved before Zahhak [57G-8-1]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=868537" TARGET="almagest">57G-8-1</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR>
	<TD COLSPAN="2" STYLE="font-weight:bold;color:white;background-color:gray;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860239" STYLE="color:white;" TARGET="almagest">1.5 ZAHHAK</A>
	</TD>
</TR>

<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860241" TARGET="almagest" NAME="01.05.01.01.01"> <SPAN STYLE="color:green;font-size:9pt;">
			[01.05.01.01.01]
		</SPAN>
		   <SPAN>
			 The Evil Customs of Zahhak and the Device of Irma'il and Karma'il
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792912,'ILKH-5-1','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792912&t=" ALT="Zahhak enthroned [ILKH-5-1]" HEIGHT="100"></A>
				</TD>
				
				 <TD>
					<A HREF="javascript:viewImage(792911,'56G-26-1','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792911&t=" ALT="Zahhak enthroned [56G-26-1]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=869119" TARGET="almagest">ILKH-5-1</A>
				</TD>
				
				 <TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=868728" TARGET="almagest">56G-26-1</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860244" TARGET="almagest" NAME="01.05.01.01.02"> <SPAN STYLE="color:green;font-size:9pt;">
			[01.05.01.01.02]
		</SPAN>
		   <SPAN>
			 Zahhak sees Faridun in a Dream
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860248" TARGET="almagest" NAME="01.05.01.01.03"> <SPAN STYLE="color:green;font-size:9pt;">
			[01.05.01.01.03]
		</SPAN>
		   <SPAN>
			 The Birth of Faridun
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860250" TARGET="almagest" NAME="01.05.01.01.04"> <SPAN STYLE="color:green;font-size:9pt;">
			[01.05.01.01.04]
		</SPAN>
		   <SPAN>
			 Faridun questions his Mother about his Origin
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792913,'ILKH-6-1','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792913&t=" ALT="Faridun questions mother [ILKH-6-1]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=869122" TARGET="almagest">ILKH-6-1</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860253" TARGET="almagest" NAME="01.05.01.01.05"> <SPAN STYLE="color:green;font-size:9pt;">
			[01.05.01.01.05]
		</SPAN>
		   <SPAN>
			 The Story of Zahhak and Kavah the Smith
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860257" TARGET="almagest" NAME="01.05.01.01.06"> <SPAN STYLE="color:green;font-size:9pt;">
			[01.05.01.01.06]
		</SPAN>
		   <SPAN>
			 Faridun goes to Battle with Zahhak
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792910,'58G-15-2','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792910&t=" ALT="Faridun fords river on way to fight Zahhak [58G-15-2]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=869114" TARGET="almagest">58G-15-2</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860260" TARGET="almagest" NAME="01.05.01.01.07"> <SPAN STYLE="color:green;font-size:9pt;">
			[01.05.01.01.07]
		</SPAN>
		   <SPAN>
			 Faridun sees the Sisters of Jamshid
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860263" TARGET="almagest" NAME="01.05.01.01.08"> <SPAN STYLE="color:green;font-size:9pt;">
			[01.05.01.01.08]
		</SPAN>
		   <SPAN>
			 The Story of Faridun and the Minister of Zahhak
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860265" TARGET="almagest" NAME="01.05.01.01.09"> <SPAN STYLE="color:green;font-size:9pt;">
			[01.05.01.01.09]
		</SPAN>
		   <SPAN>
			 Faridun binds Zahhak
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792914,'ILKH-7-1','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792914&t=" ALT="Faridun defeats Zahhak [ILKH-7-1]" HEIGHT="100"></A>
				</TD>
				
				 <TD>
					<A HREF="javascript:viewImage(792838,'59G-13-1','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792838&t=" ALT="Faridun defeat Zahhak [59G-13-1]" HEIGHT="100"></A>
				</TD>
				
				 <TD>
					<A HREF="javascript:viewImage(792796,'56G-39-1','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792796&t=" ALT="Zahhak brought before Faridun [56G-39-1]" HEIGHT="100"></A>
				</TD>
				
				 <TD>
					<A HREF="javascript:viewImage(792725,'PECK-22-1','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792725&t=" ALT="Zahhak brought before Faridun [PECK-22-1]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=869127" TARGET="almagest">ILKH-7-1</A>
				</TD>
				
				 <TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=868847" TARGET="almagest">59G-13-1</A>
				</TD>
				
				 <TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=868787" TARGET="almagest">56G-39-1</A>
				</TD>
				
				 <TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=868414" TARGET="almagest">PECK-22-1</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR>
	<TD COLSPAN="2" STYLE="font-weight:bold;color:white;background-color:gray;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860271" STYLE="color:white;" TARGET="almagest">1.6  FARIDUN</A>
	</TD>
</TR>

<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860273" TARGET="almagest" NAME="01.06.01.01.01"> <SPAN STYLE="color:green;font-size:9pt;">
			[01.06.01.01.01]
		</SPAN>
		   <SPAN>
			 Faridun ascends the Throne
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860276" TARGET="almagest" NAME="01.06.01.01.02"> <SPAN STYLE="color:green;font-size:9pt;">
			[01.06.01.01.02]
		</SPAN>
		   <SPAN>
			 Faridun sends Jandal to Yaman
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860279" TARGET="almagest" NAME="01.06.01.01.03"> <SPAN STYLE="color:green;font-size:9pt;">
			[01.06.01.01.03]
		</SPAN>
		   <SPAN>
			 The King of Yaman answers Jandal
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860281" TARGET="almagest" NAME="01.06.01.01.04"> <SPAN STYLE="color:green;font-size:9pt;">
			[01.06.01.01.04]
		</SPAN>
		   <SPAN>
			 the Sons of Faridun go to the King of Yaman
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860284" TARGET="almagest" NAME="01.06.01.01.05"> <SPAN STYLE="color:green;font-size:9pt;">
			[01.06.01.01.05]
		</SPAN>
		   <SPAN>
			 Sarv proves the Sons of Faridun by Sorcery
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860286" TARGET="almagest" NAME="01.06.01.01.06"> <SPAN STYLE="color:green;font-size:9pt;">
			[01.06.01.01.06]
		</SPAN>
		   <SPAN>
			 Faridun makes Trial of his Sons
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792916,'ILKH-10-1','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792916&t=" ALT="Faridun tests sons [ILKH-10-1]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=869133" TARGET="almagest">ILKH-10-1</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860289" TARGET="almagest" NAME="01.06.01.01.07"> <SPAN STYLE="color:green;font-size:9pt;">
			[01.06.01.01.07]
		</SPAN>
		   <SPAN>
			 Faridun divides the World among his Sons
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860292" TARGET="almagest" NAME="01.06.01.01.08"> <SPAN STYLE="color:green;font-size:9pt;">
			[01.06.01.01.08]
		</SPAN>
		   <SPAN>
			 Sakila grows envious of Iraj
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860294" TARGET="almagest" NAME="01.06.01.01.09"> <SPAN STYLE="color:green;font-size:9pt;">
			[01.06.01.01.09]
		</SPAN>
		   <SPAN>
			 Sakila and Tur send a Message to Faridun
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860296" TARGET="almagest" NAME="01.06.01.01.10"> <SPAN STYLE="color:green;font-size:9pt;">
			[01.06.01.01.10]
		</SPAN>
		   <SPAN>
			 Faridun makes Answer to his Sons
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860298" TARGET="almagest" NAME="01.06.01.01.11"> <SPAN STYLE="color:green;font-size:9pt;">
			[01.06.01.01.11]
		</SPAN>
		   <SPAN>
			 Iraj goes to his Brothers
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860301" TARGET="almagest" NAME="01.06.01.01.12"> <SPAN STYLE="color:green;font-size:9pt;">
			[01.06.01.01.12]
		</SPAN>
		   <SPAN>
			 Iraj is killed by his Brothers
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860304" TARGET="almagest" NAME="01.06.01.01.13"> <SPAN STYLE="color:green;font-size:9pt;">
			[01.06.01.01.13]
		</SPAN>
		   <SPAN>
			 Faridun receives Tidings of the Murder of Iraj
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792919,'ILKH-12-2','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792919&t=" ALT="Iraj coffin brought to Faridun [ILKH-12-2]" HEIGHT="100"></A>
				</TD>
				
				 <TD>
					<A HREF="javascript:viewImage(792918,'ILKH-12-1','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792918&t=" ALT="Iraj coffin brought to Faridun [ILKH-12-1]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=869146" TARGET="almagest">ILKH-12-2</A>
				</TD>
				
				 <TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=869141" TARGET="almagest">ILKH-12-1</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860307" TARGET="almagest" NAME="01.06.01.01.14"> <SPAN STYLE="color:green;font-size:9pt;">
			[01.06.01.01.14]
		</SPAN>
		   <SPAN>
			 a Daughter is Born to Iraj
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860309" TARGET="almagest" NAME="01.06.01.01.15"> <SPAN STYLE="color:green;font-size:9pt;">
			[01.06.01.01.15]
		</SPAN>
		   <SPAN>
			 The Birth of Manuchihr
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860311" TARGET="almagest" NAME="01.06.01.01.16"> <SPAN STYLE="color:green;font-size:9pt;">
			[01.06.01.01.16]
		</SPAN>
		   <SPAN>
			 Sakila and Tur have Tidings of Manuchihr
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860313" TARGET="almagest" NAME="01.06.01.01.17"> <SPAN STYLE="color:green;font-size:9pt;">
			[01.06.01.01.17]
		</SPAN>
		   <SPAN>
			 Faridun receives his Sons' Message
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860315" TARGET="almagest" NAME="01.06.01.01.18"> <SPAN STYLE="color:green;font-size:9pt;">
			[01.06.01.01.18]
		</SPAN>
		   <SPAN>
			 Faridun makes Answer to his Sons
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860317" TARGET="almagest" NAME="01.06.01.01.19"> <SPAN STYLE="color:green;font-size:9pt;">
			[01.06.01.01.19]
		</SPAN>
		   <SPAN>
			 Faridun sends Manuchihr to fight Tur and Sakila
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860319" TARGET="almagest" NAME="01.06.01.01.20"> <SPAN STYLE="color:green;font-size:9pt;">
			[01.06.01.01.20]
		</SPAN>
		   <SPAN>
			 Manuchihr attacks the Host of Tur
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860322" TARGET="almagest" NAME="01.06.01.01.21"> <SPAN STYLE="color:green;font-size:9pt;">
			[01.06.01.01.21]
		</SPAN>
		   <SPAN>
			 Tur is killed by Manuchihr
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792879,'58G-27-2','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792879&t=" ALT="Manuchihr kills Tur [58G-27-2]" HEIGHT="100"></A>
				</TD>
				
				 <TD>
					<A HREF="javascript:viewImage(792800,'56G-53-1','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792800&t=" ALT="Manuchihr kills Tur [56G-53-1]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=868996" TARGET="almagest">58G-27-2</A>
				</TD>
				
				 <TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=868808" TARGET="almagest">56G-53-1</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860325" TARGET="almagest" NAME="01.06.01.01.22"> <SPAN STYLE="color:green;font-size:9pt;">
			[01.06.01.01.22]
		</SPAN>
		   <SPAN>
			 Minunchihr writes to announce his Victory to Faridun
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860327" TARGET="almagest" NAME="01.06.01.01.23"> <SPAN STYLE="color:green;font-size:9pt;">
			[01.06.01.01.23]
		</SPAN>
		   <SPAN>
			 Qaran takes the Castle of the Alans
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860330" TARGET="almagest" NAME="01.06.01.01.24"> <SPAN STYLE="color:green;font-size:9pt;">
			[01.06.01.01.24]
		</SPAN>
		   <SPAN>
			 Kakui, the Grandson of Zahhak, attacks the Iranians
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860332" TARGET="almagest" NAME="01.06.01.01.25"> <SPAN STYLE="color:green;font-size:9pt;">
			[01.06.01.01.25]
		</SPAN>
		   <SPAN>
			 Sakila flees and is killed by Minunchihr
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860335" TARGET="almagest" NAME="01.06.01.01.26"> <SPAN STYLE="color:green;font-size:9pt;">
			[01.06.01.01.26]
		</SPAN>
		   <SPAN>
			 The Head of Sakila is sent to Faridun
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860337" TARGET="almagest" NAME="01.06.01.01.27"> <SPAN STYLE="color:green;font-size:9pt;">
			[01.06.01.01.27]
		</SPAN>
		   <SPAN>
			 The Death of Faridun
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR>
	<TD COLSPAN="2" STYLE="font-weight:bold;color:white;background-color:gray;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860339" STYLE="color:white;" TARGET="almagest">1.7 MANUCHIHR</A>
	</TD>
</TR>

<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860341" TARGET="almagest" NAME="01.07.01.01.01"> <SPAN STYLE="color:green;font-size:9pt;">
			[01.07.01.01.01]
		</SPAN>
		   <SPAN>
			 Minunchihr ascends the Throne and makes an Oration
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792899,'58G-30-1','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792899&t=" ALT="Manuchihr enthroned [58G-30-1]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=869072" TARGET="almagest">58G-30-1</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860344" TARGET="almagest" NAME="01.07.01.01.02"> <SPAN STYLE="color:green;font-size:9pt;">
			[01.07.01.01.02]
		</SPAN>
		   <SPAN>
			 The Birth of Zal
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860348" TARGET="almagest" NAME="01.07.01.01.03"> <SPAN STYLE="color:green;font-size:9pt;">
			[01.07.01.01.03]
		</SPAN>
		   <SPAN>
			 Sam has a Dream touching the Case of his Son
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860350" TARGET="almagest" NAME="01.07.01.01.04"> <SPAN STYLE="color:green;font-size:9pt;">
			[01.07.01.01.04]
		</SPAN>
		   <SPAN>
			 Minunchihr takes Knowledge of the Case of Sam and Zal
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860358" TARGET="almagest" NAME="01.07.01.01.05"> <SPAN STYLE="color:green;font-size:9pt;">
			[01.07.01.01.05]
		</SPAN>
		   <SPAN>
			 Zal goes back to Zabulistan
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860360" TARGET="almagest" NAME="01.07.01.01.06"> <SPAN STYLE="color:green;font-size:9pt;">
			[01.07.01.01.06]
		</SPAN>
		   <SPAN>
			 Sam gives the Kingdom to Zal
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860363" TARGET="almagest" NAME="01.07.01.01.07"> <SPAN STYLE="color:green;font-size:9pt;">
			[01.07.01.01.07]
		</SPAN>
		   <SPAN>
			 Zal visits Mihrab of Kabul
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860366" TARGET="almagest" NAME="01.07.01.01.08"> <SPAN STYLE="color:green;font-size:9pt;">
			[01.07.01.01.08]
		</SPAN>
		   <SPAN>
			 Rudabah takes Counsel with her Damsels
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860368" TARGET="almagest" NAME="01.07.01.01.09"> <SPAN STYLE="color:green;font-size:9pt;">
			[01.07.01.01.09]
		</SPAN>
		   <SPAN>
			 Rudabah's Damsels go to see Zal
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792846,'59G-32-2','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792846&t=" ALT="Zal shoots fowl [59G-32-2]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=868871" TARGET="almagest">59G-32-2</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860372" TARGET="almagest" NAME="01.07.01.01.10"> <SPAN STYLE="color:green;font-size:9pt;">
			[01.07.01.01.10]
		</SPAN>
		   <SPAN>
			 the Damsels returns to Rudabah
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860374" TARGET="almagest" NAME="01.07.01.01.11"> <SPAN STYLE="color:green;font-size:9pt;">
			[01.07.01.01.11]
		</SPAN>
		   <SPAN>
			 Zal goes to Rudabah
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792893,'58G-37-1','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792893&t=" ALT="Zal meets Rudabah [58G-37-1]" HEIGHT="100"></A>
				</TD>
				
				 <TD>
					<A HREF="javascript:viewImage(792726,'PECK-38-1','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792726&t=" ALT="Zal meets Rudabah [PECK-38-1]" HEIGHT="100"></A>
				</TD>
				
				 <TD>
					<A HREF="javascript:viewImage(792847,'59G-34-1','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792847&t=" ALT="Zal meets Rudabah [59G-34-1]" HEIGHT="100"></A>
				</TD>
				
				 <TD>
					<A HREF="javascript:viewImage(792920,'ILKH-17-2','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792920&t=" ALT="Zal meets Rudabah [ILKH-17-2]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=869045" TARGET="almagest">58G-37-1</A>
				</TD>
				
				 <TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=868489" TARGET="almagest">PECK-38-1</A>
				</TD>
				
				 <TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=868876" TARGET="almagest">59G-34-1</A>
				</TD>
				
				 <TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=869149" TARGET="almagest">ILKH-17-2</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860377" TARGET="almagest" NAME="01.07.01.01.12"> <SPAN STYLE="color:green;font-size:9pt;">
			[01.07.01.01.12]
		</SPAN>
		   <SPAN>
			 Zal consults the Archimages in the Matter of Rudabah
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860379" TARGET="almagest" NAME="01.07.01.01.13"> <SPAN STYLE="color:green;font-size:9pt;">
			[01.07.01.01.13]
		</SPAN>
		   <SPAN>
			 Zal writes to Sam to explain the Case
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860381" TARGET="almagest" NAME="01.07.01.01.14"> <SPAN STYLE="color:green;font-size:9pt;">
			[01.07.01.01.14]
		</SPAN>
		   <SPAN>
			 Sam consults the Mubad in the Matter of Rudabah
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860383" TARGET="almagest" NAME="01.07.01.01.15"> <SPAN STYLE="color:green;font-size:9pt;">
			[01.07.01.01.15]
		</SPAN>
		   <SPAN>
			 Sindukht hears of the Case of Rudabah
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792921,'ILKH-19-1','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792921&t=" ALT="Sindukht learns of Zal, Rudabah [ILKH-19-1]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=869152" TARGET="almagest">ILKH-19-1</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860386" TARGET="almagest" NAME="01.07.01.01.16"> <SPAN STYLE="color:green;font-size:9pt;">
			[01.07.01.01.16]
		</SPAN>
		   <SPAN>
			 Mihrab is made aware of his Daughter's Case
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860388" TARGET="almagest" NAME="01.07.01.01.17"> <SPAN STYLE="color:green;font-size:9pt;">
			[01.07.01.01.17]
		</SPAN>
		   <SPAN>
			 Manuchihr hears of the Case of Zal and Rudabah
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860390" TARGET="almagest" NAME="01.07.01.01.18"> <SPAN STYLE="color:green;font-size:9pt;">
			[01.07.01.01.18]
		</SPAN>
		   <SPAN>
			 Sam comes to Manuchihr
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860393" TARGET="almagest" NAME="01.07.01.01.19"> <SPAN STYLE="color:green;font-size:9pt;">
			[01.07.01.01.19]
		</SPAN>
		   <SPAN>
			 Sam goes to fight with Mihrab
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860395" TARGET="almagest" NAME="01.07.01.01.20"> <SPAN STYLE="color:green;font-size:9pt;">
			[01.07.01.01.20]
		</SPAN>
		   <SPAN>
			 Zal goes on a Mission to Manuchihr
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860398" TARGET="almagest" NAME="01.07.01.01.21"> <SPAN STYLE="color:green;font-size:9pt;">
			[01.07.01.01.21]
		</SPAN>
		   <SPAN>
			 Mihrab is wroth with Sindukht
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860400" TARGET="almagest" NAME="01.07.01.01.22"> <SPAN STYLE="color:green;font-size:9pt;">
			[01.07.01.01.22]
		</SPAN>
		   <SPAN>
			 Sam comforts Sindukht
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860404" TARGET="almagest" NAME="01.07.01.01.23"> <SPAN STYLE="color:green;font-size:9pt;">
			[01.07.01.01.23]
		</SPAN>
		   <SPAN>
			 Zal comes to Manuchihr with Sam's Letter
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792922,'ILKH-22-1','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792922&t=" ALT="Manuchihr, Zal [ILKH-22-1]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=869157" TARGET="almagest">ILKH-22-1</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860408" TARGET="almagest" NAME="01.07.01.01.24"> <SPAN STYLE="color:green;font-size:9pt;">
			[01.07.01.01.24]
		</SPAN>
		   <SPAN>
			 the Mubad questions Zal
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860411" TARGET="almagest" NAME="01.07.01.01.25"> <SPAN STYLE="color:green;font-size:9pt;">
			[01.07.01.01.25]
		</SPAN>
		   <SPAN>
			 Zal answers the Mubad
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(795102,'59G-42-2','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=795102&t=" ALT="Zal before Manuchihr after Mubad questioning [59G-42-2]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=869424" TARGET="almagest">59G-42-2</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860414" TARGET="almagest" NAME="01.07.01.01.26"> <SPAN STYLE="color:green;font-size:9pt;">
			[01.07.01.01.26]
		</SPAN>
		   <SPAN>
			 Zal displays his Accomplishment before Manuchihr
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860417" TARGET="almagest" NAME="01.07.01.01.27"> <SPAN STYLE="color:green;font-size:9pt;">
			[01.07.01.01.27]
		</SPAN>
		   <SPAN>
			 Manuchihr's Answer to Sam's Letter
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860419" TARGET="almagest" NAME="01.07.01.01.28"> <SPAN STYLE="color:green;font-size:9pt;">
			[01.07.01.01.28]
		</SPAN>
		   <SPAN>
			 Zal comes to Sam
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792802,'56G-63-2','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792802&t=" ALT="Zal marries Rudabah [56G-63-2]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=868822" TARGET="almagest">56G-63-2</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860422" TARGET="almagest" NAME="01.07.01.01.29"> <SPAN STYLE="color:green;font-size:9pt;">
			[01.07.01.01.29]
		</SPAN>
		   <SPAN>
			 The Story of the Birth of Rustam
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792826,'57G-41-2','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792826&t=" ALT="Rustam birth [57G-41-2]" HEIGHT="100"></A>
				</TD>
				
				 <TD>
					<A HREF="javascript:viewImage(792803,'56G-73-2','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792803&t=" ALT="Rustam birth [56G-73-2]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=868546" TARGET="almagest">57G-41-2</A>
				</TD>
				
				 <TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=868827" TARGET="almagest">56G-73-2</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860426" TARGET="almagest" NAME="01.07.01.01.30"> <SPAN STYLE="color:green;font-size:9pt;">
			[01.07.01.01.30]
		</SPAN>
		   <SPAN>
			 Sam comes to see Rustam
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792909,'58G-49-1','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792909&t=" ALT="Sam visits Zal, Rustam [58G-49-1]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=869106" TARGET="almagest">58G-49-1</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860429" TARGET="almagest" NAME="01.07.01.01.31"> <SPAN STYLE="color:green;font-size:9pt;">
			[01.07.01.01.31]
		</SPAN>
		   <SPAN>
			 Rustam kills the White Elephant
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792827,'57G-43-1','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792827&t=" ALT="Rustam slays White Elephant [57G-43-1]" HEIGHT="100"></A>
				</TD>
				
				 <TD>
					<A HREF="javascript:viewImage(792804,'56G-75-2','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792804&t=" ALT="Rustam slays White Elephant [56G-75-2]" HEIGHT="100"></A>
				</TD>
				
				 <TD>
					<A HREF="javascript:viewImage(792851,'59G-47-1','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792851&t=" ALT="Rustam slays White Elephant [59G-47-1]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=868551" TARGET="almagest">57G-43-1</A>
				</TD>
				
				 <TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=868830" TARGET="almagest">56G-75-2</A>
				</TD>
				
				 <TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=868889" TARGET="almagest">59G-47-1</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860432" TARGET="almagest" NAME="01.07.01.01.32"> <SPAN STYLE="color:green;font-size:9pt;">
			[01.07.01.01.32]
		</SPAN>
		   <SPAN>
			 Rustam go to Mount Sipand
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860435" TARGET="almagest" NAME="01.07.01.01.33"> <SPAN STYLE="color:green;font-size:9pt;">
			[01.07.01.01.33]
		</SPAN>
		   <SPAN>
			 Rustam writes a Letter announcing his Victory to Zal
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860437" TARGET="almagest" NAME="01.07.01.01.34"> <SPAN STYLE="color:green;font-size:9pt;">
			[01.07.01.01.34]
		</SPAN>
		   <SPAN>
			 The Letter of Zal to Sam
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860439" TARGET="almagest" NAME="01.07.01.01.35"> <SPAN STYLE="color:green;font-size:9pt;">
			[01.07.01.01.35]
		</SPAN>
		   <SPAN>
			 Manuchihr's last Counsels to his Son
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR>
	<TD COLSPAN="2" STYLE="font-weight:bold;color:white;background-color:gray;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860442" STYLE="color:white;" TARGET="almagest">1.8 NAWZAR</A>
	</TD>
</TR>

<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860444" TARGET="almagest" NAME="01.08.01.01.01"> <SPAN STYLE="color:green;font-size:9pt;">
			[01.08.01.01.01]
		</SPAN>
		   <SPAN>
			 Nawzar succeeds to the Throne
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860447" TARGET="almagest" NAME="01.08.01.01.02"> <SPAN STYLE="color:green;font-size:9pt;">
			[01.08.01.01.02]
		</SPAN>
		   <SPAN>
			 Pashang hears of the Death of Manuchihr
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860449" TARGET="almagest" NAME="01.08.01.01.03"> <SPAN STYLE="color:green;font-size:9pt;">
			[01.08.01.01.03]
		</SPAN>
		   <SPAN>
			 Afrasiyab comes to the Land of Iran
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860451" TARGET="almagest" NAME="01.08.01.01.04"> <SPAN STYLE="color:green;font-size:9pt;">
			[01.08.01.01.04]
		</SPAN>
		   <SPAN>
			 Barman and Qubad fight together and how Qubad is killed
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792852,'59G-51-2','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792852&t=" ALT="Qobad fights Barman [59G-51-2]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=868892" TARGET="almagest">59G-51-2</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860456" TARGET="almagest" NAME="01.08.01.01.05"> <SPAN STYLE="color:green;font-size:9pt;">
			[01.08.01.01.05]
		</SPAN>
		   <SPAN>
			 Afrasiyab fights with Nawzar the second Time
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860458" TARGET="almagest" NAME="01.08.01.01.06"> <SPAN STYLE="color:green;font-size:9pt;">
			[01.08.01.01.06]
		</SPAN>
		   <SPAN>
			 Nawzar fights with Afrasiyab the third Time
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860461" TARGET="almagest" NAME="01.08.01.01.07"> <SPAN STYLE="color:green;font-size:9pt;">
			[01.08.01.01.07]
		</SPAN>
		   <SPAN>
			 Nawzar is taken by Afrasiyab
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860465" TARGET="almagest" NAME="01.08.01.01.08"> <SPAN STYLE="color:green;font-size:9pt;">
			[01.08.01.01.08]
		</SPAN>
		   <SPAN>
			 Visah finds his Son that had been killed
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860467" TARGET="almagest" NAME="01.08.01.01.09"> <SPAN STYLE="color:green;font-size:9pt;">
			[01.08.01.01.09]
		</SPAN>
		   <SPAN>
			 Shamasas and Khazarvan invade Zabulistan
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860469" TARGET="almagest" NAME="01.08.01.01.10"> <SPAN STYLE="color:green;font-size:9pt;">
			[01.08.01.01.10]
		</SPAN>
		   <SPAN>
			 Zal comes to help Mihrab
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860474" TARGET="almagest" NAME="01.08.01.01.11"> <SPAN STYLE="color:green;font-size:9pt;">
			[01.08.01.01.11]
		</SPAN>
		   <SPAN>
			 Nawzar is killed by Afrasiyab
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792853,'59G-54-2','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792853&t=" ALT="Afrasiyab executes Nawzar [59G-54-2]" HEIGHT="100"></A>
				</TD>
				
				 <TD>
					<A HREF="javascript:viewImage(792924,'ILKH-25-1','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792924&t=" ALT="Afrasiyab executes Nawzar [ILKH-25-1]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=869361" TARGET="almagest">59G-54-2</A>
				</TD>
				
				 <TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=869165" TARGET="almagest">ILKH-25-1</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860478" TARGET="almagest" NAME="01.08.01.01.12"> <SPAN STYLE="color:green;font-size:9pt;">
			[01.08.01.01.12]
		</SPAN>
		   <SPAN>
			 Zal has Tidings of the Death of Nawzar
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860480" TARGET="almagest" NAME="01.08.01.01.13"> <SPAN STYLE="color:green;font-size:9pt;">
			[01.08.01.01.13]
		</SPAN>
		   <SPAN>
			 Ighriras is killed by his Brother
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR>
	<TD COLSPAN="2" STYLE="font-weight:bold;color:white;background-color:gray;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860482" STYLE="color:white;" TARGET="almagest">1.9 ZAV</A>
	</TD>
</TR>

<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860484" TARGET="almagest" NAME="01.09.01.01.01"> <SPAN STYLE="color:green;font-size:9pt;">
			[01.09.01.01.01]
		</SPAN>
		   <SPAN>
			 Zav is elected Shah
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792926,'ILKH-26-2','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792926&t=" ALT="Zav makes peace with Turks, fixes boundary [ILKH-26-2]" HEIGHT="100"></A>
				</TD>
				
				 <TD>
					<A HREF="javascript:viewImage(792925,'ILKH-26-1','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792925&t=" ALT="Zav enthroned [ILKH-26-1]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=869175" TARGET="almagest">ILKH-26-2</A>
				</TD>
				
				 <TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=869170" TARGET="almagest">ILKH-26-1</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR>
	<TD COLSPAN="2" STYLE="font-weight:bold;color:white;background-color:gray;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860488" STYLE="color:white;" TARGET="almagest">1.10  GARSHASP</A>
	</TD>
</TR>

<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860490" TARGET="almagest" NAME="01.10.01.01.01"> <SPAN STYLE="color:green;font-size:9pt;">
			[01.10.01.01.01]
		</SPAN>
		   <SPAN>
			 Garshasp succeeds to the Throne and dies, and how Afrasiyab invades Iran
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860492" TARGET="almagest" NAME="01.10.01.01.02"> <SPAN STYLE="color:green;font-size:9pt;">
			[01.10.01.01.02]
		</SPAN>
		   <SPAN>
			 Rustam catches Rakhsh
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792907,'58G-58-2','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792907&t=" ALT="Rustam catches Rakhsh [58G-58-2]" HEIGHT="100"></A>
				</TD>
				
				 <TD>
					<A HREF="javascript:viewImage(792727,'PECK-54-1','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792727&t=" ALT="Rustam catches Rakhsh [PECK-54-1]" HEIGHT="100"></A>
				</TD>
				
				 <TD>
					<A HREF="javascript:viewImage(792854,'59G-57-2','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792854&t=" ALT="Rustam catches Rakhsh [59G-57-2]" HEIGHT="100"></A>
				</TD>
				
				 <TD>
					<A HREF="javascript:viewImage(792829,'57G-52-1','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792829&t=" ALT="Rustam catches Rakhsh [57G-52-1]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=869103" TARGET="almagest">58G-58-2</A>
				</TD>
				
				 <TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=868509" TARGET="almagest">PECK-54-1</A>
				</TD>
				
				 <TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=868897" TARGET="almagest">59G-57-2</A>
				</TD>
				
				 <TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=868556" TARGET="almagest">57G-52-1</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860495" TARGET="almagest" NAME="01.10.01.01.03"> <SPAN STYLE="color:green;font-size:9pt;">
			[01.10.01.01.03]
		</SPAN>
		   <SPAN>
			 Zal leads the Host against Afrasiyab
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860497" TARGET="almagest" NAME="01.10.01.01.04"> <SPAN STYLE="color:green;font-size:9pt;">
			[01.10.01.01.04]
		</SPAN>
		   <SPAN>
			 Rustam brings Kay Qubad from Mount Alburz
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792855,'59G-59-1','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792855&t=" ALT="Rustam slays Qolun [59G-59-1]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=868900" TARGET="almagest">59G-59-1</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR>
	<TD COLSPAN="2" STYLE="font-weight:bold;color:white;background-color:black;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860501" STYLE="color:white;" TARGET="almagest">2  KAYANIAN</A>
	</TD>
</TR>

<TR>
	<TD COLSPAN="2" STYLE="font-weight:bold;color:white;background-color:gray;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860503" STYLE="color:white;" TARGET="almagest">2.1 KAY QUBAD</A>
	</TD>
</TR>

<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860505" TARGET="almagest" NAME="02.01.01.01.01"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.01.01.01.01]
		</SPAN>
		   <SPAN>
			 Kay Qubad ascends the Throne and wars against Turan
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860509" TARGET="almagest" NAME="02.01.01.01.02"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.01.01.01.02]
		</SPAN>
		   <SPAN>
			 Rustam fights with Afrasiyab
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792878,'58G-61-1','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792878&t=" ALT="Rustam catches Afrasiyab by belt, lifts him [58G-61-1]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=868991" TARGET="almagest">58G-61-1</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860514" TARGET="almagest" NAME="02.01.01.01.03"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.01.01.01.03]
		</SPAN>
		   <SPAN>
			 Afrasiyab comes to his Father
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860517" TARGET="almagest" NAME="02.01.01.01.04"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.01.01.01.04]
		</SPAN>
		   <SPAN>
			 Pashang sues to Kay Qubad for Peace
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860519" TARGET="almagest" NAME="02.01.01.01.05"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.01.01.01.05]
		</SPAN>
		   <SPAN>
			 Kay Qubad comes to Istakhr of Pars
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR>
	<TD COLSPAN="2" STYLE="font-weight:bold;color:white;background-color:gray;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860522" STYLE="color:white;" TARGET="almagest">2.2 KAI KA'US</A>
	</TD>
</TR>

<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860524" TARGET="almagest" NAME="02.02.01.01.01"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.02.01.01.01]
		</SPAN>
		   <SPAN>
			 The Prelude
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860526" TARGET="almagest" NAME="02.02.01.01.02"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.02.01.01.02]
		</SPAN>
		   <SPAN>
			 Kaus sits upon the Throne and is tempted to invade Mazandaran
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860530" TARGET="almagest" NAME="02.02.01.01.03"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.02.01.01.03]
		</SPAN>
		   <SPAN>
			 Zal gives Counsel to Kaus
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860533" TARGET="almagest" NAME="02.02.01.01.04"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.02.01.01.04]
		</SPAN>
		   <SPAN>
			 Kaus goes to Mazandaran
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860536" TARGET="almagest" NAME="02.02.01.01.05"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.02.01.01.05]
		</SPAN>
		   <SPAN>
			 The Message of Kai Ka'us to Zal and Rustam
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860538" TARGET="almagest" NAME="02.02.01.02.01"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.02.01.02.01]
		</SPAN>
		   <SPAN>
			 First Course. Rakhsh fights a Lion
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792856,'59G-65-1','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792856&t=" ALT="Rustam first feat: Rakhsh kills lion [59G-65-1]" HEIGHT="100"></A>
				</TD>
				
				 <TD>
					<A HREF="javascript:viewImage(792903,'58G-66-1','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792903&t=" ALT="Rustam first feat: Rakhsh kills lion [58G-66-1]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=868905" TARGET="almagest">59G-65-1</A>
				</TD>
				
				 <TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=869375" TARGET="almagest">58G-66-1</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860543" TARGET="almagest" NAME="02.02.01.02.02"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.02.01.02.02]
		</SPAN>
		   <SPAN>
			 Second Course. Rustam finds a Spring
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860546" TARGET="almagest" NAME="02.02.01.02.03"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.02.01.02.03]
		</SPAN>
		   <SPAN>
			 Third Course. Rustam fights a Dragon
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792884,'58G-67-1','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792884&t=" ALT="Rustam third feat: Slays dragon [58G-67-1]" HEIGHT="100"></A>
				</TD>
				
				 <TD>
					<A HREF="javascript:viewImage(792830,'57G-59-2','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792830&t=" ALT="Rustam third feat: Slays dragon [57G-59-2]" HEIGHT="100"></A>
				</TD>
				
				 <TD>
					<A HREF="javascript:viewImage(792857,'59G-66-2','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792857&t=" ALT="Rustam third feat: Slays dragon [59G-66-2]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=869018" TARGET="almagest">58G-67-1</A>
				</TD>
				
				 <TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=868559" TARGET="almagest">57G-59-2</A>
				</TD>
				
				 <TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=869429" TARGET="almagest">59G-66-2</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860549" TARGET="almagest" NAME="02.02.01.02.04"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.02.01.02.04]
		</SPAN>
		   <SPAN>
			 Fourth Course. Rustam kills a Witch
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792872,'58G-68-1','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792872&t=" ALT="Rustam fourth feat: Cleaves witch [58G-68-1]" HEIGHT="100"></A>
				</TD>
				
				 <TD>
					<A HREF="javascript:viewImage(792858,'59G-67-1','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792858&t=" ALT="Rustam fourth feat: Cleaves witch [59G-67-1]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=868965" TARGET="almagest">58G-68-1</A>
				</TD>
				
				 <TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=869364" TARGET="almagest">59G-67-1</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860552" TARGET="almagest" NAME="02.02.01.02.05"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.02.01.02.05]
		</SPAN>
		   <SPAN>
			 Fifth Course. Rustam takes Awlad captive
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792859,'59G-68-1','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792859&t=" ALT="Rustam fifth feat: Lassoes Awlad [59G-68-1]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=868910" TARGET="almagest">59G-68-1</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860556" TARGET="almagest" NAME="02.02.01.02.06"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.02.01.02.06]
		</SPAN>
		   <SPAN>
			 Sixth Course. Rustam fights with Arzhang Div
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792889,'58G-69-2','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792889&t=" ALT="Rustam sixth feat: Kills Arzhang [58G-69-2]" HEIGHT="100"></A>
				</TD>
				
				 <TD>
					<A HREF="javascript:viewImage(792860,'59G-69-1','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792860&t=" ALT="Rustam sixth feat: Kills Arzhang [59G-69-1]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=869032" TARGET="almagest">58G-69-2</A>
				</TD>
				
				 <TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=868915" TARGET="almagest">59G-69-1</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860559" TARGET="almagest" NAME="02.02.01.02.07"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.02.01.02.07]
		</SPAN>
		   <SPAN>
			 Seventh Course. Rustam kills the White Div
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792728,'PECK-62-2','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792728&t=" ALT="Rustam seventh feat: Cuts Hand and Leg from White Div [PECK-62-2]" HEIGHT="100"></A>
				</TD>
				
				 <TD>
					<A HREF="javascript:viewImage(792861,'59G-70-1','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792861&t=" ALT="Rustam seventh feat: Slays White Div [59G-70-1]" HEIGHT="100"></A>
				</TD>
				
				 <TD>
					<A HREF="javascript:viewImage(792876,'58G-71-1','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792876&t=" ALT="Rustam seventh feat: Slays White Div [58G-71-1]" HEIGHT="100"></A>
				</TD>
				
				 <TD>
					<A HREF="javascript:viewImage(792805,'56G-96-2','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792805&t=" ALT="Rustam seventh feat: Slays White Div [56G-96-2]" HEIGHT="100"></A>
				</TD>
				
				 <TD>
					<A HREF="javascript:viewImage(792831,'57G-62-1','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792831&t=" ALT="Rustam seventh feat: Slays White Div [57G-62-1]" HEIGHT="100"></A>
				</TD>
				
				 <TD>
					<A HREF="javascript:viewImage(792891,'58G-70-1','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792891&t=" ALT="Rustam enters cave where Kai Ka'us, men chained [58G-70-1]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=868514" TARGET="almagest">PECK-62-2</A>
				</TD>
				
				 <TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=868920" TARGET="almagest">59G-70-1</A>
				</TD>
				
				 <TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=868983" TARGET="almagest">58G-71-1</A>
				</TD>
				
				 <TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=868833" TARGET="almagest">56G-96-2</A>
				</TD>
				
				 <TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=868564" TARGET="almagest">57G-62-1</A>
				</TD>
				
				 <TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=869035" TARGET="almagest">58G-70-1</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860567" TARGET="almagest" NAME="02.02.01.02.08"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.02.01.02.08]
		</SPAN>
		   <SPAN>
			 Kaus writes to the King of Mazandaran
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860569" TARGET="almagest" NAME="02.02.01.02.09"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.02.01.02.09]
		</SPAN>
		   <SPAN>
			 Rustam goes on an Embassy to the King of Mazandaran
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860572" TARGET="almagest" NAME="02.02.01.02.10"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.02.01.02.10]
		</SPAN>
		   <SPAN>
			 Kaus fights with the King of Mazandaran
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792927,'ILKH-32-2','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792927&t=" ALT="Kai Ka'us Pahlavan fight King Mazandaran [ILKH-32-2]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=869180" TARGET="almagest">ILKH-32-2</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860581" TARGET="almagest" NAME="02.02.01.02.11"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.02.01.02.11]
		</SPAN>
		   <SPAN>
			 Kaus returns to the Land of Iran and farewells Rustam
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792773,'56G-100-1','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792773&t=" ALT="King Mazandaran dies [56G-100-1]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=868677" TARGET="almagest">56G-100-1</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860584" TARGET="almagest" NAME="02.02.02.01.01"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.02.02.01.01]
		</SPAN>
		   <SPAN>
			 Kai Ka'us wars with the King of Hamavaran
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860586" TARGET="almagest" NAME="02.02.02.01.02"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.02.02.01.02]
		</SPAN>
		   <SPAN>
			 Kai Ka'us asks to Wife Sudabah, the Daughter of the King of Hamavaran
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860589" TARGET="almagest" NAME="02.02.02.01.03"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.02.02.01.03]
		</SPAN>
		   <SPAN>
			 the King of Hamavaran makes Kaus Prisoner
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860593" TARGET="almagest" NAME="02.02.02.01.04"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.02.02.01.04]
		</SPAN>
		   <SPAN>
			 Afrasiyab invades the Land of Iran
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860595" TARGET="almagest" NAME="02.02.02.01.05"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.02.02.01.05]
		</SPAN>
		   <SPAN>
			 Rustam sends a Message to the King of Hamavaran
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860597" TARGET="almagest" NAME="02.02.02.01.06"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.02.02.01.06]
		</SPAN>
		   <SPAN>
			 Rustam fights with Three Kings and delivered Kaus
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860602" TARGET="almagest" NAME="02.02.02.01.07"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.02.02.01.07]
		</SPAN>
		   <SPAN>
			 Kaus sends a Message to Afrasiyab
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860604" TARGET="almagest" NAME="02.02.02.01.08"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.02.02.01.08]
		</SPAN>
		   <SPAN>
			 Kaus orders the World
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860607" TARGET="almagest" NAME="02.02.02.01.09"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.02.02.01.09]
		</SPAN>
		   <SPAN>
			 Kaus, beguiled by Iblis, ascended the Sky
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792906,'58G-78-2','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792906&t=" ALT="Kai Ka'us air-borne [58G-78-2]" HEIGHT="100"></A>
				</TD>
				
				 <TD>
					<A HREF="javascript:viewImage(792862,'59G-78-1','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792862&t=" ALT="Kai Ka'us air-borne [59G-78-1]" HEIGHT="100"></A>
				</TD>
				
				 <TD>
					<A HREF="javascript:viewImage(792774,'56G-105-2','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792774&t=" ALT="Kai Ka'us air-borne [56G-105-2]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=869100" TARGET="almagest">58G-78-2</A>
				</TD>
				
				 <TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=868923" TARGET="almagest">59G-78-1</A>
				</TD>
				
				 <TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=868682" TARGET="almagest">56G-105-2</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860611" TARGET="almagest" NAME="02.02.02.01.10"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.02.02.01.10]
		</SPAN>
		   <SPAN>
			 Rustam brought back Kaus
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860614" TARGET="almagest" NAME="02.02.02.02.01"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.02.02.02.01]
		</SPAN>
		   <SPAN>
			 Rustam goes with the Seven Warriors to the Hunting-ground of Afrasiyab
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860617" TARGET="almagest" NAME="02.02.02.02.02"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.02.02.02.02]
		</SPAN>
		   <SPAN>
			 Rustam fights with the Turanian
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860621" TARGET="almagest" NAME="02.02.02.02.03"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.02.02.02.03]
		</SPAN>
		   <SPAN>
			 Pilsam fights with the Iranians
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860625" TARGET="almagest" NAME="02.02.02.02.04"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.02.02.02.04]
		</SPAN>
		   <SPAN>
			 Afrasiyab flees from the Battlefield
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860628" TARGET="almagest" NAME="02.02.03.01.01"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.02.03.01.01]
		</SPAN>
		   <SPAN>
			 The Prelude
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860630" TARGET="almagest" NAME="02.02.03.01.02"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.02.03.01.02]
		</SPAN>
		   <SPAN>
			 Rustam goes to the Chace
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860634" TARGET="almagest" NAME="02.02.03.01.03"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.02.03.01.03]
		</SPAN>
		   <SPAN>
			 Rustam comes to the City of Samangan
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860636" TARGET="almagest" NAME="02.02.03.01.04"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.02.03.01.04]
		</SPAN>
		   <SPAN>
			 Tahminah, the Daughter of the King of Samangan, comes to Rustam
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792892,'58G-82-2','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792892&t=" ALT="Tahminah visits Rustam [58G-82-2]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=869040" TARGET="almagest">58G-82-2</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860639" TARGET="almagest" NAME="02.02.03.01.05"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.02.03.01.05]
		</SPAN>
		   <SPAN>
			 The Birth of Suhrab
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860641" TARGET="almagest" NAME="02.02.03.01.06"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.02.03.01.06]
		</SPAN>
		   <SPAN>
			 Suhrab chose his Charger
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860644" TARGET="almagest" NAME="02.02.03.01.07"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.02.03.01.07]
		</SPAN>
		   <SPAN>
			 Afrasiyab sends Barman and Human to Suhrab
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860646" TARGET="almagest" NAME="02.02.03.01.08"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.02.03.01.08]
		</SPAN>
		   <SPAN>
			 Suhrab comes to White Castle
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860648" TARGET="almagest" NAME="02.02.03.01.09"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.02.03.01.09]
		</SPAN>
		   <SPAN>
			 Suhrab fights with Gurdafarid
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792863,'59G-84-1','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792863&t=" ALT="Suhrab fights Gurdafarid [59G-84-1]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=868926" TARGET="almagest">59G-84-1</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860652" TARGET="almagest" NAME="02.02.03.01.10"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.02.03.01.10]
		</SPAN>
		   <SPAN>
			 The Letter of Gazhdaham to Kaus
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860654" TARGET="almagest" NAME="02.02.03.01.11"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.02.03.01.11]
		</SPAN>
		   <SPAN>
			 Suhrab takes White Castle
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860656" TARGET="almagest" NAME="02.02.03.01.12"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.02.03.01.12]
		</SPAN>
		   <SPAN>
			 Kaus writes to Rustam and summons him from Zabulistan
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860658" TARGET="almagest" NAME="02.02.03.01.13"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.02.03.01.13]
		</SPAN>
		   <SPAN>
			 Kaus is wroth with Rustam
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860662" TARGET="almagest" NAME="02.02.03.01.14"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.02.03.01.14]
		</SPAN>
		   <SPAN>
			 Kaus and Rustam lead forth the Host
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860664" TARGET="almagest" NAME="02.02.03.01.15"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.02.03.01.15]
		</SPAN>
		   <SPAN>
			 Rustam kills Zhindah Razm
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860667" TARGET="almagest" NAME="02.02.03.01.16"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.02.03.01.16]
		</SPAN>
		   <SPAN>
			 Suhrab asks Hajir the Names of the Chiefs of Iran
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792904,'58G-89-1','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792904&t=" ALT="Suhrab, Hajir view Iranian camp [58G-89-1]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=869087" TARGET="almagest">58G-89-1</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860670" TARGET="almagest" NAME="02.02.03.01.17"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.02.03.01.17]
		</SPAN>
		   <SPAN>
			 Suhrab attacks the Army of Kaus
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860672" TARGET="almagest" NAME="02.02.03.01.18"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.02.03.01.18]
		</SPAN>
		   <SPAN>
			 Rustam fights with Suhrab
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860677" TARGET="almagest" NAME="02.02.03.01.19"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.02.03.01.19]
		</SPAN>
		   <SPAN>
			 Rustam and Suhrab return to Camp
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860679" TARGET="almagest" NAME="02.02.03.01.20"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.02.03.01.20]
		</SPAN>
		   <SPAN>
			 Suhrab overthrows Rustam
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792890,'58G-92-2','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792890&t=" ALT="Suhrab overthrows Rustam in their second combat [58G-92-2]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=869464" TARGET="almagest">58G-92-2</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860682" TARGET="almagest" NAME="02.02.03.01.21"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.02.03.01.21]
		</SPAN>
		   <SPAN>
			 Suhrab is killed by Rustam
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792729,'PECK-77-2','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792729&t=" ALT="Rustam Stabs Suhrab [PECK-77-2]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=868519" TARGET="almagest">PECK-77-2</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860686" TARGET="almagest" NAME="02.02.03.01.22"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.02.03.01.22]
		</SPAN>
		   <SPAN>
			 Rustam asks Kaus for an Elixir
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860688" TARGET="almagest" NAME="02.02.03.01.23"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.02.03.01.23]
		</SPAN>
		   <SPAN>
			 Rustam laments for Suhrab
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792832,'57G-85-1','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792832&t=" ALT="Rustam mourns Suhrab [57G-85-1]" HEIGHT="100"></A>
				</TD>
				
				 <TD>
					<A HREF="javascript:viewImage(792864,'59G-93-2','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792864&t=" ALT="Rustam mourns Suhrab [59G-93-2]" HEIGHT="100"></A>
				</TD>
				
				 <TD>
					<A HREF="javascript:viewImage(792874,'58G-93-2','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792874&t=" ALT="Rustam mourns Suhrab [58G-93-2]" HEIGHT="100"></A>
				</TD>
				
				 <TD>
					<A HREF="javascript:viewImage(792775,'56G-121-1','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792775&t=" ALT="Rustam mourns Suhrab [56G-121-1]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=868569" TARGET="almagest">57G-85-1</A>
				</TD>
				
				 <TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=868931" TARGET="almagest">59G-93-2</A>
				</TD>
				
				 <TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=868975" TARGET="almagest">58G-93-2</A>
				</TD>
				
				 <TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=868687" TARGET="almagest">56G-121-1</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860691" TARGET="almagest" NAME="02.02.03.01.24"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.02.03.01.24]
		</SPAN>
		   <SPAN>
			 Rustam returns to Zabulistan
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792895,'58G-95-2','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792895&t=" ALT="Suhrab's Mother (Tahminah) Mourns his Death [58G-95-2]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=869051" TARGET="almagest">58G-95-2</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860695" TARGET="almagest" NAME="02.02.03.01.25"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.02.03.01.25]
		</SPAN>
		   <SPAN>
			 Suhrab's Mother receives the Tidings of his Death
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860697" TARGET="almagest" NAME="02.02.04.01.01"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.02.04.01.01]
		</SPAN>
		   <SPAN>
			 The Prelude
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860699" TARGET="almagest" NAME="02.02.04.01.02"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.02.04.01.02]
		</SPAN>
		   <SPAN>
			 The Story of the Mother of Siyavash
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792865,'59G-96-2','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792865&t=" ALT="Tus, Gudarz encounter maiden in forest [59G-96-2]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=869432" TARGET="almagest">59G-96-2</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860702" TARGET="almagest" NAME="02.02.04.01.03"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.02.04.01.03]
		</SPAN>
		   <SPAN>
			 The Birth of Siyavash
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860705" TARGET="almagest" NAME="02.02.04.01.04"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.02.04.01.04]
		</SPAN>
		   <SPAN>
			 Siyavash arrives from Zabulistan
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860707" TARGET="almagest" NAME="02.02.04.01.05"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.02.04.01.05]
		</SPAN>
		   <SPAN>
			 The Death of the Mother of Siyavash
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860709" TARGET="almagest" NAME="02.02.04.01.06"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.02.04.01.06]
		</SPAN>
		   <SPAN>
			 Sudabah falls in Love with Siyavash
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860711" TARGET="almagest" NAME="02.02.04.01.07"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.02.04.01.07]
		</SPAN>
		   <SPAN>
			 Siyavash visits Sudabah
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860714" TARGET="almagest" NAME="02.02.04.01.08"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.02.04.01.08]
		</SPAN>
		   <SPAN>
			 Siyavash visits the Bower the second Time
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860716" TARGET="almagest" NAME="02.02.04.01.09"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.02.04.01.09]
		</SPAN>
		   <SPAN>
			 Siyavash visits the Bower the third Time
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860719" TARGET="almagest" NAME="02.02.04.01.10"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.02.04.01.10]
		</SPAN>
		   <SPAN>
			 Sudabah beguiles Kaus
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860722" TARGET="almagest" NAME="02.02.04.01.11"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.02.04.01.11]
		</SPAN>
		   <SPAN>
			 Sudabah and a Sorceress devise a Scheme
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860724" TARGET="almagest" NAME="02.02.04.01.12"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.02.04.01.12]
		</SPAN>
		   <SPAN>
			 Kaus inquires into the Matter of the Babes
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860726" TARGET="almagest" NAME="02.02.04.01.13"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.02.04.01.13]
		</SPAN>
		   <SPAN>
			 Siyavash passes through the Fire
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792894,'58G-102-1','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792894&t=" ALT="Fire ordeal Siyavash [58G-102-1]" HEIGHT="100"></A>
				</TD>
				
				 <TD>
					<A HREF="javascript:viewImage(792834,'57G-93-1','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792834&t=" ALT="Fire ordeal Siyavash [57G-93-1]" HEIGHT="100"></A>
				</TD>
				
				 <TD>
					<A HREF="javascript:viewImage(792835,'59G-102-1','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792835&t=" ALT="Fire ordeal Siyavash [59G-102-1]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=869048" TARGET="almagest">58G-102-1</A>
				</TD>
				
				 <TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=868574" TARGET="almagest">57G-93-1</A>
				</TD>
				
				 <TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=868836" TARGET="almagest">59G-102-1</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860730" TARGET="almagest" NAME="02.02.04.01.14"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.02.04.01.14]
		</SPAN>
		   <SPAN>
			 Siyavash begs Sudabah's Life of his Father
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860732" TARGET="almagest" NAME="02.02.04.01.15"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.02.04.01.15]
		</SPAN>
		   <SPAN>
			 Kaus hears of the Coming of Afrasiyab
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860734" TARGET="almagest" NAME="02.02.04.01.16"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.02.04.01.16]
		</SPAN>
		   <SPAN>
			 Siyavash leads forth the Host
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860736" TARGET="almagest" NAME="02.02.04.01.17"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.02.04.01.17]
		</SPAN>
		   <SPAN>
			 The Letter of Siyavash to Kai Ka'us
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860738" TARGET="almagest" NAME="02.02.04.01.18"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.02.04.01.18]
		</SPAN>
		   <SPAN>
			 The Answer of Kai Ka'us to the Letter of Siyavash
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860740" TARGET="almagest" NAME="02.02.04.01.19"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.02.04.01.19]
		</SPAN>
		   <SPAN>
			 Afrasiyab has a Dream and is afraid
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860742" TARGET="almagest" NAME="02.02.04.01.20"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.02.04.01.20]
		</SPAN>
		   <SPAN>
			 Afrasiyab inquires of the Sages concerning his Dream
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860744" TARGET="almagest" NAME="02.02.04.01.21"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.02.04.01.21]
		</SPAN>
		   <SPAN>
			 Afrasiyab takes Counsel with the Nobles
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860746" TARGET="almagest" NAME="02.02.04.01.22"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.02.04.01.22]
		</SPAN>
		   <SPAN>
			 Garsivaz comes to Siyavash
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860750" TARGET="almagest" NAME="02.02.04.01.23"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.02.04.01.23]
		</SPAN>
		   <SPAN>
			 Siyavash makes a Treaty with Afrasiyab
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860752" TARGET="almagest" NAME="02.02.04.01.24"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.02.04.01.24]
		</SPAN>
		   <SPAN>
			 Siyavash sends Rustam to Kaus
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860754" TARGET="almagest" NAME="02.02.04.01.25"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.02.04.01.25]
		</SPAN>
		   <SPAN>
			 Rustam gives the Message to Kaus
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860756" TARGET="almagest" NAME="02.02.04.01.26"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.02.04.01.26]
		</SPAN>
		   <SPAN>
			 Kaus sends Rustam to Sistan
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860758" TARGET="almagest" NAME="02.02.04.01.27"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.02.04.01.27]
		</SPAN>
		   <SPAN>
			 The Answer of Kaus to the Letter of Siyavash
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860760" TARGET="almagest" NAME="02.02.04.01.28"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.02.04.01.28]
		</SPAN>
		   <SPAN>
			 Siyavash takes Counsel with Bahram and Zangah
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860763" TARGET="almagest" NAME="02.02.04.01.29"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.02.04.01.29]
		</SPAN>
		   <SPAN>
			 Zangah goes to Afrasiyab
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860765" TARGET="almagest" NAME="02.02.04.01.30"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.02.04.01.30]
		</SPAN>
		   <SPAN>
			 Afrasiyab writes to Siyavash
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860767" TARGET="almagest" NAME="02.02.04.01.31"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.02.04.01.31]
		</SPAN>
		   <SPAN>
			 Siyavash gives up the Host to Bahram
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860770" TARGET="almagest" NAME="02.02.04.01.32"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.02.04.01.32]
		</SPAN>
		   <SPAN>
			 The Interview of Siyavash with Afrasiyab
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860773" TARGET="almagest" NAME="02.02.04.01.33"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.02.04.01.33]
		</SPAN>
		   <SPAN>
			 Siyavash displays his Prowess before Afrasiyab
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792836,'59G-112-1','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792836&t=" ALT="Siyavash plays polo before Afrasiyab [59G-112-1]" HEIGHT="100"></A>
				</TD>
				
				 <TD>
					<A HREF="javascript:viewImage(792869,'58G-111-1','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792869&t=" ALT="Siyavash plays polo before Afrasiyab [58G-111-1]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=868839" TARGET="almagest">59G-112-1</A>
				</TD>
				
				 <TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=868958" TARGET="almagest">58G-111-1</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860777" TARGET="almagest" NAME="02.02.04.01.34"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.02.04.01.34]
		</SPAN>
		   <SPAN>
			 Afrasiyab and Siyavash go to the Chase
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792776,'56G-139-1','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792776&t=" ALT="Siyavash hunts onager with Afrasiyab [56G-139-1]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=868690" TARGET="almagest">56G-139-1</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860780" TARGET="almagest" NAME="02.02.04.01.35"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.02.04.01.35]
		</SPAN>
		   <SPAN>
			 Piran gives his Daughter to Siyavash
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860782" TARGET="almagest" NAME="02.02.04.01.36"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.02.04.01.36]
		</SPAN>
		   <SPAN>
			 Piran speaks to Siyavash about Farangis
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860784" TARGET="almagest" NAME="02.02.04.01.37"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.02.04.01.37]
		</SPAN>
		   <SPAN>
			 Piran speaks with Afrasiyab
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792731,'PECK-110-2','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792731&t=" ALT="Afrasiyab, in pursuit of Kay Khusraw, meets Piran [PECK-110-2]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=868339" TARGET="almagest">PECK-110-2</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860787" TARGET="almagest" NAME="02.02.04.01.38"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.02.04.01.38]
		</SPAN>
		   <SPAN>
			 The Wedding of Farangis and Siyavash
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860790" TARGET="almagest" NAME="02.02.04.01.39"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.02.04.01.39]
		</SPAN>
		   <SPAN>
			 Afrasiyab bestows a Province on Siyavash
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860793" TARGET="almagest" NAME="02.02.04.01.40"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.02.04.01.40]
		</SPAN>
		   <SPAN>
			 Siyavash builds Gang Dizh
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860795" TARGET="almagest" NAME="02.02.04.01.41"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.02.04.01.41]
		</SPAN>
		   <SPAN>
			 Siyavash discourses with Piran about the Future
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860797" TARGET="almagest" NAME="02.02.04.01.42"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.02.04.01.42]
		</SPAN>
		   <SPAN>
			 Afrasiyab sends Piran into the Provinces
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860799" TARGET="almagest" NAME="02.02.04.01.43"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.02.04.01.43]
		</SPAN>
		   <SPAN>
			 Siyavash builds Siyavashgird
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860801" TARGET="almagest" NAME="02.02.04.01.44"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.02.04.01.44]
		</SPAN>
		   <SPAN>
			 Piran visits Siyavashgird
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792730,'PECK-95-1','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792730&t=" ALT="Piran visits Siyavashgird [PECK-95-1]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=869419" TARGET="almagest">PECK-95-1</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860805" TARGET="almagest" NAME="02.02.04.01.45"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.02.04.01.45]
		</SPAN>
		   <SPAN>
			 Afrasiyab sends Garsivaz to Siyavash
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860808" TARGET="almagest" NAME="02.02.04.01.46"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.02.04.01.46]
		</SPAN>
		   <SPAN>
			 The Birth of Furud, the Son of Siyavash
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860810" TARGET="almagest" NAME="02.02.04.01.47"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.02.04.01.47]
		</SPAN>
		   <SPAN>
			 Siyavash plays at Polo
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860813" TARGET="almagest" NAME="02.02.04.01.48"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.02.04.01.48]
		</SPAN>
		   <SPAN>
			 Garsivaz returns and speaks Evil before Afrasiyab
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860817" TARGET="almagest" NAME="02.02.04.01.49"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.02.04.01.49]
		</SPAN>
		   <SPAN>
			 Garsivaz returns to Siyavash
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860820" TARGET="almagest" NAME="02.02.04.01.50"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.02.04.01.50]
		</SPAN>
		   <SPAN>
			 The Letter of Siyavash to Afrasiyab
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860822" TARGET="almagest" NAME="02.02.04.01.51"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.02.04.01.51]
		</SPAN>
		   <SPAN>
			 Afrasiyab comes to fight with Siyavash
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860824" TARGET="almagest" NAME="02.02.04.01.52"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.02.04.01.52]
		</SPAN>
		   <SPAN>
			 Siyavash has a Dream
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860826" TARGET="almagest" NAME="02.02.04.01.53"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.02.04.01.53]
		</SPAN>
		   <SPAN>
			 Siyavash takes Counsel with Bahram and Zangah
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860828" TARGET="almagest" NAME="02.02.04.01.54"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.02.04.01.54]
		</SPAN>
		   <SPAN>
			 Siyeawush is taken by Afrasiyab
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792806,'57G-111-2','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792806&t=" ALT="Siyavash taken prisoner [57G-111-2]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=868579" TARGET="almagest">57G-111-2</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860831" TARGET="almagest" NAME="02.02.04.01.55"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.02.04.01.55]
		</SPAN>
		   <SPAN>
			 Farangis bewails herself before Afrasiyab
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860833" TARGET="almagest" NAME="02.02.04.01.56"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.02.04.01.56]
		</SPAN>
		   <SPAN>
			 Siyavash is killed by Guruy
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792868,'58G-121-1','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792868&t=" ALT="Guruy executes Siyavash [58G-121-1]" HEIGHT="100"></A>
				</TD>
				
				 <TD>
					<A HREF="javascript:viewImage(792777,'56G-152-1','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792777&t=" ALT="Guruy executes Siyavash [56G-152-1]" HEIGHT="100"></A>
				</TD>
				
				 <TD>
					<A HREF="javascript:viewImage(792837,'59G-123-2','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792837&t=" ALT="Guruy executes Siyavash [59G-123-2]" HEIGHT="100"></A>
				</TD>
				
				 <TD>
					<A HREF="javascript:viewImage(792807,'57G-113-1','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792807&t=" ALT="Guruy executes Siyavash [57G-113-1]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=868955" TARGET="almagest">58G-121-1</A>
				</TD>
				
				 <TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=868695" TARGET="almagest">56G-152-1</A>
				</TD>
				
				 <TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=868844" TARGET="almagest">59G-123-2</A>
				</TD>
				
				 <TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=868584" TARGET="almagest">57G-113-1</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860836" TARGET="almagest" NAME="02.02.04.01.57"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.02.04.01.57]
		</SPAN>
		   <SPAN>
			 Piran saves Farangis
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860838" TARGET="almagest" NAME="02.02.04.01.58"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.02.04.01.58]
		</SPAN>
		   <SPAN>
			 The Birth of Kay Khusraw
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860840" TARGET="almagest" NAME="02.02.04.01.59"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.02.04.01.59]
		</SPAN>
		   <SPAN>
			 Piran entrusts Kay Khusraw to the Shepherds
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860842" TARGET="almagest" NAME="02.02.04.01.60"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.02.04.01.60]
		</SPAN>
		   <SPAN>
			 Piran brings Kay Khusraw before Afrasiyab
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860845" TARGET="almagest" NAME="02.02.04.01.61"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.02.04.01.61]
		</SPAN>
		   <SPAN>
			 Kay Khusraw goes to Siyavashgird
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860847" TARGET="almagest" NAME="02.02.05.01.01"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.02.05.01.01]
		</SPAN>
		   <SPAN>
			 Firdawsi's Lament over his old Age
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860849" TARGET="almagest" NAME="02.02.05.01.02"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.02.05.01.02]
		</SPAN>
		   <SPAN>
			 Kaus hears of the Case of Siyavash
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860852" TARGET="almagest" NAME="02.02.05.01.03"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.02.05.01.03]
		</SPAN>
		   <SPAN>
			 Rustam comes to Kaus
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860854" TARGET="almagest" NAME="02.02.05.01.04"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.02.05.01.04]
		</SPAN>
		   <SPAN>
			 Rustam kills Sudabah and led forth the Host
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860857" TARGET="almagest" NAME="02.02.05.01.05"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.02.05.01.05]
		</SPAN>
		   <SPAN>
			 Faramarz kills Varazad
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860860" TARGET="almagest" NAME="02.02.05.01.06"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.02.05.01.06]
		</SPAN>
		   <SPAN>
			 Surkhah leads his Troops to fight with Rustam
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860865" TARGET="almagest" NAME="02.02.05.01.07"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.02.05.01.07]
		</SPAN>
		   <SPAN>
			 Afrasiyab leads forth the Host to avenge his Son
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860867" TARGET="almagest" NAME="02.02.05.01.08"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.02.05.01.08]
		</SPAN>
		   <SPAN>
			 Pilsam is killed by Rustam
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792778,'56G-159-2','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792778&t=" ALT="Rustam slays Pilsam [56G-159-2]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=868698" TARGET="almagest">56G-159-2</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860870" TARGET="almagest" NAME="02.02.05.01.09"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.02.05.01.09]
		</SPAN>
		   <SPAN>
			 Afrasiyab flees from Rustam
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792901,'58G-127-2','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792901&t=" ALT="Rustam fights Afrasiyab, Human, Turanian [58G-127-2]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=869077" TARGET="almagest">58G-127-2</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860873" TARGET="almagest" NAME="02.02.05.01.10"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.02.05.01.10]
		</SPAN>
		   <SPAN>
			 Afrasiyab sends Kay Khusraw to Khutan
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860875" TARGET="almagest" NAME="02.02.05.01.11"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.02.05.01.11]
		</SPAN>
		   <SPAN>
			 Rustam reigns over Turan for Seven Years
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860878" TARGET="almagest" NAME="02.02.05.01.12"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.02.05.01.12]
		</SPAN>
		   <SPAN>
			 Zavarah goes to the Hunting-ground of Siyavash
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860880" TARGET="almagest" NAME="02.02.05.01.13"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.02.05.01.13]
		</SPAN>
		   <SPAN>
			 Rustam harries the Land of Turan
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860882" TARGET="almagest" NAME="02.02.05.01.14"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.02.05.01.14]
		</SPAN>
		   <SPAN>
			 Rustam returns to Iran
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860884" TARGET="almagest" NAME="02.02.05.01.15"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.02.05.01.15]
		</SPAN>
		   <SPAN>
			 Gudarz has a Dream of Kay Khusraw
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860886" TARGET="almagest" NAME="02.02.05.01.16"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.02.05.01.16]
		</SPAN>
		   <SPAN>
			 Giv goes to Tuan in Quest of Kay Khusraw
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860888" TARGET="almagest" NAME="02.02.05.01.17"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.02.05.01.17]
		</SPAN>
		   <SPAN>
			 The Finding of Kay Khusraw
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792867,'58G-131-1','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792867&t=" ALT="Giv finds Kay Khusraw [58G-131-1]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=868947" TARGET="almagest">58G-131-1</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860891" TARGET="almagest" NAME="02.02.05.01.18"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.02.05.01.18]
		</SPAN>
		   <SPAN>
			 Giv and Kay Khusraw go to Siyavashgird
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860893" TARGET="almagest" NAME="02.02.05.01.19"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.02.05.01.19]
		</SPAN>
		   <SPAN>
			 Kay Khusraw wins Bihzad
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792866,'58G-133-1','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792866&t=" ALT="Kay Khusraw, Farangis watch Giv defeat Turanian [58G-133-1]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=868942" TARGET="almagest">58G-133-1</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860896" TARGET="almagest" NAME="02.02.05.01.20"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.02.05.01.20]
		</SPAN>
		   <SPAN>
			 Farangis goes with Kay Khusraw and Giv to Iran
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860898" TARGET="almagest" NAME="02.02.05.01.21"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.02.05.01.21]
		</SPAN>
		   <SPAN>
			 Kulbad and Nastihan flee from Giv
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860900" TARGET="almagest" NAME="02.02.05.01.22"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.02.05.01.22]
		</SPAN>
		   <SPAN>
			 Piran pursues Kay Khusraw
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860902" TARGET="almagest" NAME="02.02.05.01.23"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.02.05.01.23]
		</SPAN>
		   <SPAN>
			 Piran contends with Giv
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860904" TARGET="almagest" NAME="02.02.05.01.24"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.02.05.01.24]
		</SPAN>
		   <SPAN>
			 Piran is taken by Giv
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792882,'58G-134-2','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792882&t=" ALT="Giv leads captive Piran before Kay Khusraw, Farangis [58G-134-2]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=869007" TARGET="almagest">58G-134-2</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860908" TARGET="almagest" NAME="02.02.05.01.25"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.02.05.01.25]
		</SPAN>
		   <SPAN>
			 Farangis deliveres Piran from Giv
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860910" TARGET="almagest" NAME="02.02.05.01.26"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.02.05.01.26]
		</SPAN>
		   <SPAN>
			 Afrasiyab finds Piran on the Way
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860912" TARGET="almagest" NAME="02.02.05.01.27"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.02.05.01.27]
		</SPAN>
		   <SPAN>
			 Giv disputes with the Toll-man
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860915" TARGET="almagest" NAME="02.02.05.01.28"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.02.05.01.28]
		</SPAN>
		   <SPAN>
			 Kay Khusraw crosses the Jayhun
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792881,'58G-136-1','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792881&t=" ALT="Kay Khusraw crosses Oxus with Farangis, Giv [58G-136-1]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=869002" TARGET="almagest">58G-136-1</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860918" TARGET="almagest" NAME="02.02.05.01.29"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.02.05.01.29]
		</SPAN>
		   <SPAN>
			 Kay Khusraw comes to Isfahan
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860920" TARGET="almagest" NAME="02.02.05.01.30"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.02.05.01.30]
		</SPAN>
		   <SPAN>
			 Kay Khusraw comes to Kaus
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860922" TARGET="almagest" NAME="02.02.05.01.31"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.02.05.01.31]
		</SPAN>
		   <SPAN>
			 Tus refuses Allegiance to Kay Khusraw
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860924" TARGET="almagest" NAME="02.02.05.01.32"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.02.05.01.32]
		</SPAN>
		   <SPAN>
			 Gudarz is wroth with Tus
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860926" TARGET="almagest" NAME="02.02.05.01.33"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.02.05.01.33]
		</SPAN>
		   <SPAN>
			 Gudarz and Tus go before Kaus on the Matter of the Kingship
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860930" TARGET="almagest" NAME="02.02.05.01.34"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.02.05.01.34]
		</SPAN>
		   <SPAN>
			 Tus and Fariburz go to the Castle of Bahman and comes back foiled
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860932" TARGET="almagest" NAME="02.02.05.01.35"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.02.05.01.35]
		</SPAN>
		   <SPAN>
			 Kay Khusraw goes to the Castle of Bahman and takes it
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792898,'58G-139-1','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792898&t=" ALT="Kay Khusraw army beseiges Bahman castle [58G-139-1]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=869067" TARGET="almagest">58G-139-1</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860935" TARGET="almagest" NAME="02.02.05.01.36"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.02.05.01.36]
		</SPAN>
		   <SPAN>
			 Kay Khusraw returns in Triumph
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860937" TARGET="almagest" NAME="02.02.05.01.37"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.02.05.01.37]
		</SPAN>
		   <SPAN>
			 Kaus sets Khusraw upon the Throne of Kingship
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR>
	<TD COLSPAN="2" STYLE="font-weight:bold;color:white;background-color:gray;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860941" STYLE="color:white;" TARGET="almagest">2.3 KAY KHUSRAW</A>
	</TD>
</TR>

<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860943" TARGET="almagest" NAME="02.03.01.01.01"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.01.01.01]
		</SPAN>
		   <SPAN>
			 The Prelude
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860945" TARGET="almagest" NAME="02.03.01.01.02"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.01.01.02]
		</SPAN>
		   <SPAN>
			 the Nobles do Homage to Kay Khusraw
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792779,'56G-173-2','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792779&t=" ALT="Rustam, Zal before Kay Khusraw [56G-173-2]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=868703" TARGET="almagest">56G-173-2</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860948" TARGET="almagest" NAME="02.03.01.01.03"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.01.01.03]
		</SPAN>
		   <SPAN>
			 Kay Khusraw makes a Progress through his Realm
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860950" TARGET="almagest" NAME="02.03.01.01.04"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.01.01.04]
		</SPAN>
		   <SPAN>
			 Kay Khusraw swears to Kai Ka'us to take Vengeance on Afrasiyab
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860953" TARGET="almagest" NAME="02.03.01.01.05"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.01.01.05]
		</SPAN>
		   <SPAN>
			 Kay Khusraw numbers the Pahlavan
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860956" TARGET="almagest" NAME="02.03.01.01.06"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.01.01.06]
		</SPAN>
		   <SPAN>
			 Kay Khusraw bestowes Treasures upon the Pahlavan
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860958" TARGET="almagest" NAME="02.03.01.01.07"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.01.01.07]
		</SPAN>
		   <SPAN>
			 Kay Khusraw sends Rustam to the Land of Hind
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860960" TARGET="almagest" NAME="02.03.01.01.08"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.01.01.08]
		</SPAN>
		   <SPAN>
			 Kay Khusraw reviews the Host
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860962" TARGET="almagest" NAME="02.03.01.02.01"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.01.02.01]
		</SPAN>
		   <SPAN>
			 The Prelude
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860964" TARGET="almagest" NAME="02.03.01.02.02"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.01.02.02]
		</SPAN>
		   <SPAN>
			 Tus goes to Turkistan
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860966" TARGET="almagest" NAME="02.03.01.02.03"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.01.02.03]
		</SPAN>
		   <SPAN>
			 Furud hears of the Coming of Tus
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860968" TARGET="almagest" NAME="02.03.01.02.04"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.01.02.04]
		</SPAN>
		   <SPAN>
			 Furud and Tukhar go to view the Host
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860971" TARGET="almagest" NAME="02.03.01.02.05"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.01.02.05]
		</SPAN>
		   <SPAN>
			 Bahram comes to Furud upon the Mountain
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860974" TARGET="almagest" NAME="02.03.01.02.06"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.01.02.06]
		</SPAN>
		   <SPAN>
			 Bahram goes back to Tus
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860977" TARGET="almagest" NAME="02.03.01.02.07"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.01.02.07]
		</SPAN>
		   <SPAN>
			 Rivniz is killed by Furud
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860980" TARGET="almagest" NAME="02.03.01.02.08"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.01.02.08]
		</SPAN>
		   <SPAN>
			 Zarasp is killed by Furud
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860983" TARGET="almagest" NAME="02.03.01.02.09"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.01.02.09]
		</SPAN>
		   <SPAN>
			 Tus fights with Furud
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860985" TARGET="almagest" NAME="02.03.01.02.10"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.01.02.10]
		</SPAN>
		   <SPAN>
			 Giv fights with Furud
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860988" TARGET="almagest" NAME="02.03.01.02.11"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.01.02.11]
		</SPAN>
		   <SPAN>
			 Bizhan fights with Furud
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860993" TARGET="almagest" NAME="02.03.01.02.12"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.01.02.12]
		</SPAN>
		   <SPAN>
			 Furud is killed
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860996" TARGET="almagest" NAME="02.03.01.02.13"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.01.02.13]
		</SPAN>
		   <SPAN>
			 Jarirah kills herself
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792873,'58G-148-1','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792873&t=" ALT="Jarirah commits suicide over her son body [58G-148-1]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=868970" TARGET="almagest">58G-148-1</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=860999" TARGET="almagest" NAME="02.03.01.02.14"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.01.02.14]
		</SPAN>
		   <SPAN>
			 Tus leads the Host to the Kasah Rud, and how Palashan is killed by Bizhan
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861002" TARGET="almagest" NAME="02.03.01.02.15"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.01.02.15]
		</SPAN>
		   <SPAN>
			 the Iranians suffer in a Snowstorm
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861005" TARGET="almagest" NAME="02.03.01.02.16"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.01.02.16]
		</SPAN>
		   <SPAN>
			 Bahram capturs Kabudah
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861008" TARGET="almagest" NAME="02.03.01.02.17"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.01.02.17]
		</SPAN>
		   <SPAN>
			 the Iranians fights with Tazhav
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792780,'56G-186-2','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792780&t=" ALT="Giv strikes crown from Tazhav head [56G-186-2]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=868708" TARGET="almagest">56G-186-2</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861013" TARGET="almagest" NAME="02.03.01.02.18"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.01.02.18]
		</SPAN>
		   <SPAN>
			 Afrasiyab has Tidings of Tus and his Host
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861015" TARGET="almagest" NAME="02.03.01.02.19"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.01.02.19]
		</SPAN>
		   <SPAN>
			 Piran makes a Night-attack on the Iranians
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861019" TARGET="almagest" NAME="02.03.01.02.20"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.01.02.20]
		</SPAN>
		   <SPAN>
			 Kay Khusraw recalls Tus
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861023" TARGET="almagest" NAME="02.03.01.02.21"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.01.02.21]
		</SPAN>
		   <SPAN>
			 Fariburz asks a Truce of Piran
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861026" TARGET="almagest" NAME="02.03.01.02.22"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.01.02.22]
		</SPAN>
		   <SPAN>
			 the Iranians are defeated by the Turkmans
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861031" TARGET="almagest" NAME="02.03.01.02.23"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.01.02.23]
		</SPAN>
		   <SPAN>
			 Bahram returns to look for his Whip on the Battlefield
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861033" TARGET="almagest" NAME="02.03.01.02.24"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.01.02.24]
		</SPAN>
		   <SPAN>
			 Bahram is killed by Tazhav
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861036" TARGET="almagest" NAME="02.03.01.02.25"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.01.02.25]
		</SPAN>
		   <SPAN>
			 Giv kills Tazhav in Revenge for Bahram
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861039" TARGET="almagest" NAME="02.03.01.02.26"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.01.02.26]
		</SPAN>
		   <SPAN>
			 the Iranians go back to Khusraw
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861041" TARGET="almagest" NAME="02.03.02.01.01"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.02.01.01]
		</SPAN>
		   <SPAN>
			 The Prelude
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861043" TARGET="almagest" NAME="02.03.02.01.02"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.02.01.02]
		</SPAN>
		   <SPAN>
			 Khusraw reviles Tus
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861045" TARGET="almagest" NAME="02.03.02.01.03"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.02.01.03]
		</SPAN>
		   <SPAN>
			 Khusraw pardons the Iranians
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861047" TARGET="almagest" NAME="02.03.02.01.04"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.02.01.04]
		</SPAN>
		   <SPAN>
			 Khusraw sends Tus to Turan
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861050" TARGET="almagest" NAME="02.03.02.01.05"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.02.01.05]
		</SPAN>
		   <SPAN>
			 The Message of Piran to the Army of Iran
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861052" TARGET="almagest" NAME="02.03.02.01.06"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.02.01.06]
		</SPAN>
		   <SPAN>
			 Afrasiyab sends an Army to Piran
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861054" TARGET="almagest" NAME="02.03.02.01.07"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.02.01.07]
		</SPAN>
		   <SPAN>
			 Tus kills Arzhang
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861056" TARGET="almagest" NAME="02.03.02.01.08"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.02.01.08]
		</SPAN>
		   <SPAN>
			 Human fights with Tus
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861059" TARGET="almagest" NAME="02.03.02.01.09"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.02.01.09]
		</SPAN>
		   <SPAN>
			 the Iranians and Turanian fight the second Time
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861062" TARGET="almagest" NAME="02.03.02.01.10"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.02.01.10]
		</SPAN>
		   <SPAN>
			 the Turanian use Sorcery against the Host of Iran
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792733,'PECK-132-2','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792733&t=" ALT="Rustam subdues Bazur Turanian wizard [PECK-132-2]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=868349" TARGET="almagest">PECK-132-2</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861066" TARGET="almagest" NAME="02.03.02.01.11"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.02.01.11]
		</SPAN>
		   <SPAN>
			 the Iranians retreat to Mount Hamawan
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861068" TARGET="almagest" NAME="02.03.02.01.12"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.02.01.12]
		</SPAN>
		   <SPAN>
			 the Host of Turan beleaguers Mount Hamawan
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861071" TARGET="almagest" NAME="02.03.02.01.13"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.02.01.13]
		</SPAN>
		   <SPAN>
			 Piran goes in Pursuit of the Iranians to Mount Hamavan
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861073" TARGET="almagest" NAME="02.03.02.01.14"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.02.01.14]
		</SPAN>
		   <SPAN>
			 the Iranians make a Night-attack
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861076" TARGET="almagest" NAME="02.03.02.01.15"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.02.01.15]
		</SPAN>
		   <SPAN>
			 Kay Khusraw has Tidings of his Host
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861079" TARGET="almagest" NAME="02.03.02.01.16"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.02.01.16]
		</SPAN>
		   <SPAN>
			 Fariburz asks to Wife Farangis, the Mother of Kay Khusraw
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861082" TARGET="almagest" NAME="02.03.02.01.17"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.02.01.17]
		</SPAN>
		   <SPAN>
			 Tus sees Siyavash in a Dream
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861084" TARGET="almagest" NAME="02.03.02.01.18"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.02.01.18]
		</SPAN>
		   <SPAN>
			 Afrasiyab sends the Khan and Kamus to help Piran
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861086" TARGET="almagest" NAME="02.03.02.01.19"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.02.01.19]
		</SPAN>
		   <SPAN>
			 Khaqan of China comes to Hamawan
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861088" TARGET="almagest" NAME="02.03.02.01.20"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.02.01.20]
		</SPAN>
		   <SPAN>
			 the Iranians take Counsel how to act
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861091" TARGET="almagest" NAME="02.03.02.01.21"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.02.01.21]
		</SPAN>
		   <SPAN>
			 Gudarz has Tidings of the Coming of Rustam
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861093" TARGET="almagest" NAME="02.03.02.01.22"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.02.01.22]
		</SPAN>
		   <SPAN>
			 Khaqan of China goes to reconnoitre the Army of Iran
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861095" TARGET="almagest" NAME="02.03.02.01.23"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.02.01.23]
		</SPAN>
		   <SPAN>
			 Fariburz reaches Mount Hamawan
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861097" TARGET="almagest" NAME="02.03.02.01.24"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.02.01.24]
		</SPAN>
		   <SPAN>
			 Piran takes Counsel with Khaqan of China
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861100" TARGET="almagest" NAME="02.03.02.01.25"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.02.01.25]
		</SPAN>
		   <SPAN>
			 Giv and Tus fights with Kamus
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861103" TARGET="almagest" NAME="02.03.02.01.26"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.02.01.26]
		</SPAN>
		   <SPAN>
			 The Coming of Rustam
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861106" TARGET="almagest" NAME="02.03.02.01.27"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.02.01.27]
		</SPAN>
		   <SPAN>
			 the Iranians and Turanian array their Hosts
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861109" TARGET="almagest" NAME="02.03.02.01.28"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.02.01.28]
		</SPAN>
		   <SPAN>
			 Rustam fights with Ashkabus
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792781,'56G-209-1','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792781&t=" ALT="Rustam slays Ashkabus, horse [56G-209-1]" HEIGHT="100"></A>
				</TD>
				
				 <TD>
					<A HREF="javascript:viewImage(792908,'58G-171-2','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792908&t=" ALT="Rustam slays Ashkabus, horse [58G-171-2]" HEIGHT="100"></A>
				</TD>
				
				 <TD>
					<A HREF="javascript:viewImage(792808,'57G-163-1','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792808&t=" ALT="Rustam slays Ashkabus, horse [57G-163-1]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=868713" TARGET="almagest">56G-209-1</A>
				</TD>
				
				 <TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=869469" TARGET="almagest">58G-171-2</A>
				</TD>
				
				 <TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=868589" TARGET="almagest">57G-163-1</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861113" TARGET="almagest" NAME="02.03.02.01.29"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.02.01.29]
		</SPAN>
		   <SPAN>
			 Piran holds Converse concerning the Coming of Rustam
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861115" TARGET="almagest" NAME="02.03.02.01.30"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.02.01.30]
		</SPAN>
		   <SPAN>
			 the Iranians and Turanian set the Battle in Array
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861117" TARGET="almagest" NAME="02.03.02.01.31"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.02.01.31]
		</SPAN>
		   <SPAN>
			 Alwa is killed by Kamus
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861119" TARGET="almagest" NAME="02.03.02.01.32"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.02.01.32]
		</SPAN>
		   <SPAN>
			 Kamus is killed by Rustam
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792734,'PECK-142-2','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792734&t=" ALT="Rustam captures Kamus, kills him [PECK-142-2]" HEIGHT="100"></A>
				</TD>
				
				 <TD>
					<A HREF="javascript:viewImage(792839,'59G-173-2','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792839&t=" ALT="Rustam captures Kamus, kills him [59G-173-2]" HEIGHT="100"></A>
				</TD>
				
				 <TD>
					<A HREF="javascript:viewImage(792905,'58G-173-2','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792905&t=" ALT="Rustam captures Kamus, kills him [58G-173-2]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=868354" TARGET="almagest">PECK-142-2</A>
				</TD>
				
				 <TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=868852" TARGET="almagest">59G-173-2</A>
				</TD>
				
				 <TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=869097" TARGET="almagest">58G-173-2</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861122" TARGET="almagest" NAME="02.03.03.01.01"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.03.01.01]
		</SPAN>
		   <SPAN>
			 Khaqan of China has Tidings of the Slaying of Kamus
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861124" TARGET="almagest" NAME="02.03.03.01.02"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.03.01.02]
		</SPAN>
		   <SPAN>
			 How Chingish fares with Rustam
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792809,'57G-172-1','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792809&t=" ALT="Rustam pulls Khaqan of China from elephant by lasso [57G-172-1]" HEIGHT="100"></A>
				</TD>
				
				 <TD>
					<A HREF="javascript:viewImage(792880,'58G-182-2','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792880&t=" ALT="Rustam pulls Khaqan of China from elephant by lasso [58G-182-2]" HEIGHT="100"></A>
				</TD>
				
				 <TD>
					<A HREF="javascript:viewImage(792902,'58G-175-1','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792902&t=" ALT="Rustam overturns Chingish by seizing horse tail [58G-175-1]" HEIGHT="100"></A>
				</TD>
				
				 <TD>
					<A HREF="javascript:viewImage(792735,'PECK-149-2','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792735&t=" ALT="Rustam pulls Khaqan of China from elephant by lasso [PECK-149-2]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=868594" TARGET="almagest">57G-172-1</A>
				</TD>
				
				 <TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=868999" TARGET="almagest">58G-182-2</A>
				</TD>
				
				 <TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=869082" TARGET="almagest">58G-175-1</A>
				</TD>
				
				 <TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=868359" TARGET="almagest">PECK-149-2</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861128" TARGET="almagest" NAME="02.03.03.01.03"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.03.01.03]
		</SPAN>
		   <SPAN>
			 Khaqan of China sends Human to Rustam
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861130" TARGET="almagest" NAME="02.03.03.01.04"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.03.01.04]
		</SPAN>
		   <SPAN>
			 Piran takes Counsel with Human and the Khan
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861132" TARGET="almagest" NAME="02.03.03.01.05"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.03.01.05]
		</SPAN>
		   <SPAN>
			 Piran comes to Rustam
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792870,'58G-176-2','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792870&t=" ALT="Rustam, Piran parley [58G-176-2]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=869459" TARGET="almagest">58G-176-2</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861135" TARGET="almagest" NAME="02.03.03.01.06"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.03.01.06]
		</SPAN>
		   <SPAN>
			 the Turanian take Counsel for Battle with the Iranians
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861137" TARGET="almagest" NAME="02.03.03.01.07"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.03.01.07]
		</SPAN>
		   <SPAN>
			 Rustam harangues his Troops
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861139" TARGET="almagest" NAME="02.03.03.01.08"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.03.01.08]
		</SPAN>
		   <SPAN>
			 the Iranians and Turanian set the Battle in Array
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861141" TARGET="almagest" NAME="02.03.03.01.09"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.03.01.09]
		</SPAN>
		   <SPAN>
			 Rustam reproaches Piran
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861144" TARGET="almagest" NAME="02.03.03.01.10"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.03.01.10]
		</SPAN>
		   <SPAN>
			 the Battle is joined
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861147" TARGET="almagest" NAME="02.03.03.01.11"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.03.01.11]
		</SPAN>
		   <SPAN>
			 Shangul fights with Rustam and fled
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861150" TARGET="almagest" NAME="02.03.03.01.12"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.03.01.12]
		</SPAN>
		   <SPAN>
			 Rustam fights with Savah
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861153" TARGET="almagest" NAME="02.03.03.01.13"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.03.01.13]
		</SPAN>
		   <SPAN>
			 Rustam kills Gahar
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861156" TARGET="almagest" NAME="02.03.03.01.14"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.03.01.14]
		</SPAN>
		   <SPAN>
			 the Khan is taken Prisoner
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861158" TARGET="almagest" NAME="02.03.03.01.15"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.03.01.15]
		</SPAN>
		   <SPAN>
			 the Host of the Turanian is defeated
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861160" TARGET="almagest" NAME="02.03.03.01.16"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.03.01.16]
		</SPAN>
		   <SPAN>
			 Rustam divides the Spoil
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861162" TARGET="almagest" NAME="02.03.03.01.17"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.03.01.17]
		</SPAN>
		   <SPAN>
			 Rustam writes a Letter to Kay Khusraw
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861164" TARGET="almagest" NAME="02.03.03.01.18"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.03.01.18]
		</SPAN>
		   <SPAN>
			 Kay Khusraw makes Answer to Rustam's Letter
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861166" TARGET="almagest" NAME="02.03.03.01.19"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.03.01.19]
		</SPAN>
		   <SPAN>
			 Afrasiyab has Tidings of the Case of his Army
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861168" TARGET="almagest" NAME="02.03.03.01.20"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.03.01.20]
		</SPAN>
		   <SPAN>
			 Rustam fights with Kafur the Man-eater
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861170" TARGET="almagest" NAME="02.03.03.01.21"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.03.01.21]
		</SPAN>
		   <SPAN>
			 Afrasiyab has Tidings of the Coming of Rustam
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861172" TARGET="almagest" NAME="02.03.03.01.22"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.03.01.22]
		</SPAN>
		   <SPAN>
			 Afrasiyab's Letter to Puladvand
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861174" TARGET="almagest" NAME="02.03.03.01.23"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.03.01.23]
		</SPAN>
		   <SPAN>
			 Puladvand fights with Giv and Tus
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792736,'PECK-154-2','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792736&t=" ALT="Puladvand lifts Giv, Tus from their saddles [PECK-154-2]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=868364" TARGET="almagest">PECK-154-2</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861177" TARGET="almagest" NAME="02.03.03.01.24"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.03.01.24]
		</SPAN>
		   <SPAN>
			 Rustam fights with Puladvand
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861180" TARGET="almagest" NAME="02.03.03.01.25"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.03.01.25]
		</SPAN>
		   <SPAN>
			 The Wrestling of Rustam and Puladvand
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792886,'58G-189-2','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792886&t=" ALT="Rustam vanquishes Puladvand [58G-189-2]" HEIGHT="100"></A>
				</TD>
				
				 <TD>
					<A HREF="javascript:viewImage(792782,'56G-224-1','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792782&t=" ALT="Rustam vanquishes Puladvand [56G-224-1]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=869021" TARGET="almagest">58G-189-2</A>
				</TD>
				
				 <TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=868716" TARGET="almagest">56G-224-1</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861184" TARGET="almagest" NAME="02.03.03.01.26"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.03.01.26]
		</SPAN>
		   <SPAN>
			 Afrasiyab flees from Rustam
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861186" TARGET="almagest" NAME="02.03.03.01.27"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.03.01.27]
		</SPAN>
		   <SPAN>
			 Rustam returns to the Court of the Shah
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861188" TARGET="almagest" NAME="02.03.03.01.28"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.03.01.28]
		</SPAN>
		   <SPAN>
			 Rustam goes back to Sistan
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861190" TARGET="almagest" NAME="02.03.04.01.01"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.04.01.01]
		</SPAN>
		   <SPAN>
			 The Prelude
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861192" TARGET="almagest" NAME="02.03.04.01.02"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.04.01.02]
		</SPAN>
		   <SPAN>
			 Khusraw summons Rustam to fight the Akvan Div
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861194" TARGET="almagest" NAME="02.03.04.01.03"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.04.01.03]
		</SPAN>
		   <SPAN>
			 Rustam goes in Quest of the Div
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861196" TARGET="almagest" NAME="02.03.04.01.04"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.04.01.04]
		</SPAN>
		   <SPAN>
			 the Akvan Div flings Rustam into the Sea
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792810,'57G-180-2','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792810&t=" ALT="Akvan Div flings Rustam into sea [57G-180-2]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=868597" TARGET="almagest">57G-180-2</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861200" TARGET="almagest" NAME="02.03.04.01.05"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.04.01.05]
		</SPAN>
		   <SPAN>
			 Afrasiyab comes to inspect his Steeds, and how Rustam kills the Akvan Div
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792877,'58G-192-2','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792877&t=" ALT="Rustam kills Akvan Div [58G-192-2]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=868986" TARGET="almagest">58G-192-2</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861205" TARGET="almagest" NAME="02.03.04.01.06"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.04.01.06]
		</SPAN>
		   <SPAN>
			 Rustam goes back to the Land of Iran
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861207" TARGET="almagest" NAME="02.03.05.01.01"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.05.01.01]
		</SPAN>
		   <SPAN>
			 The Prelude
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861209" TARGET="almagest" NAME="02.03.05.01.02"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.05.01.02]
		</SPAN>
		   <SPAN>
			 the Iranians appeal to Khusraw
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861212" TARGET="almagest" NAME="02.03.05.01.03"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.05.01.03]
		</SPAN>
		   <SPAN>
			 Bizhan goes to fight the wild Boars
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861215" TARGET="almagest" NAME="02.03.05.01.04"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.05.01.04]
		</SPAN>
		   <SPAN>
			 Gurgin beguiles Bizhan
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861217" TARGET="almagest" NAME="02.03.05.01.05"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.05.01.05]
		</SPAN>
		   <SPAN>
			 Bizhan goes to see Manizhah, Daughter of Afrasiyab
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861219" TARGET="almagest" NAME="02.03.05.01.06"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.05.01.06]
		</SPAN>
		   <SPAN>
			 Bizhan goes to the Tent of Manizhah
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861223" TARGET="almagest" NAME="02.03.05.01.07"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.05.01.07]
		</SPAN>
		   <SPAN>
			 Manizhah carries off Bizhan to her Palace
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861225" TARGET="almagest" NAME="02.03.05.01.08"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.05.01.08]
		</SPAN>
		   <SPAN>
			 Garsivaz brings Bizhan before Afrasiyab
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861228" TARGET="almagest" NAME="02.03.05.01.09"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.05.01.09]
		</SPAN>
		   <SPAN>
			 Piran begs Bizhan's Life from Afrasiyab
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861231" TARGET="almagest" NAME="02.03.05.01.10"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.05.01.10]
		</SPAN>
		   <SPAN>
			 Afrasiyab puts Bizhan in Ward
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861235" TARGET="almagest" NAME="02.03.05.01.11"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.05.01.11]
		</SPAN>
		   <SPAN>
			 Gurgin returns to Iran and lies about Bizhan
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861237" TARGET="almagest" NAME="02.03.05.01.12"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.05.01.12]
		</SPAN>
		   <SPAN>
			 Giv brings Gurgin before Khusraw
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861239" TARGET="almagest" NAME="02.03.05.01.13"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.05.01.13]
		</SPAN>
		   <SPAN>
			 Kay Khusraw sees Bizhan in the Cup that shows the World
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861241" TARGET="almagest" NAME="02.03.05.01.14"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.05.01.14]
		</SPAN>
		   <SPAN>
			 Khusraw writes a Letter to Rustam
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861243" TARGET="almagest" NAME="02.03.05.01.15"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.05.01.15]
		</SPAN>
		   <SPAN>
			 Giv bears the Letter of Kay Khusraw to Rustam
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861245" TARGET="almagest" NAME="02.03.05.01.16"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.05.01.16]
		</SPAN>
		   <SPAN>
			 Rustam makes a Feast for Giv
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861248" TARGET="almagest" NAME="02.03.05.01.17"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.05.01.17]
		</SPAN>
		   <SPAN>
			 Rustam comes to Khusraw
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861251" TARGET="almagest" NAME="02.03.05.01.18"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.05.01.18]
		</SPAN>
		   <SPAN>
			 Kay Khusraw holds Feast with the Pahlavan
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861253" TARGET="almagest" NAME="02.03.05.01.19"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.05.01.19]
		</SPAN>
		   <SPAN>
			 Rustam makes Petition for Gurgin to the Shah
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861255" TARGET="almagest" NAME="02.03.05.01.20"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.05.01.20]
		</SPAN>
		   <SPAN>
			 Rustam equips his Escort
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861257" TARGET="almagest" NAME="02.03.05.01.21"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.05.01.21]
		</SPAN>
		   <SPAN>
			 Rustam goes to the City of Khutan to Piran
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792737,'PECK-167-1','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792737&t=" ALT="Rustam dressed as merchant before Piran [PECK-167-1]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=868369" TARGET="almagest">PECK-167-1</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861260" TARGET="almagest" NAME="02.03.05.01.22"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.05.01.22]
		</SPAN>
		   <SPAN>
			 Manizhah comes before Rustam
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861263" TARGET="almagest" NAME="02.03.05.01.23"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.05.01.23]
		</SPAN>
		   <SPAN>
			 Bizhan hears of the Coming of Rustam
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861265" TARGET="almagest" NAME="02.03.05.01.24"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.05.01.24]
		</SPAN>
		   <SPAN>
			 Rustam takes Bizhan out of the Pit
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792783,'56G-245-1','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792783&t=" ALT="Rustam rescues Bizhan from pit [56G-245-1]" HEIGHT="100"></A>
				</TD>
				
				 <TD>
					<A HREF="javascript:viewImage(792841,'59G-200-2','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792841&t=" ALT="Rustam rescues Bizhan from pit [59G-200-2]" HEIGHT="100"></A>
				</TD>
				
				 <TD>
					<A HREF="javascript:viewImage(792811,'57G-193-2','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792811&t=" ALT="Rustam rescues Bizhan from pit [57G-193-2]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=868721" TARGET="almagest">56G-245-1</A>
				</TD>
				
				 <TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=868858" TARGET="almagest">59G-200-2</A>
				</TD>
				
				 <TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=868602" TARGET="almagest">57G-193-2</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861268" TARGET="almagest" NAME="02.03.05.01.25"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.05.01.25]
		</SPAN>
		   <SPAN>
			 Rustam attacks the Palace of Afrasiyab by Night
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861271" TARGET="almagest" NAME="02.03.05.01.26"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.05.01.26]
		</SPAN>
		   <SPAN>
			 Afrasiyab goes to fight with Rustam
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792812,'57G-195-2','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792812&t=" ALT="Rustam fights Afrasiyab [57G-195-2]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=868607" TARGET="almagest">57G-195-2</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861274" TARGET="almagest" NAME="02.03.05.01.27"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.05.01.27]
		</SPAN>
		   <SPAN>
			 Afrasiyab is defeated by the Iranians
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861277" TARGET="almagest" NAME="02.03.05.01.28"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.05.01.28]
		</SPAN>
		   <SPAN>
			 Rustam returns to Kay Khusraw
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861279" TARGET="almagest" NAME="02.03.05.01.29"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.05.01.29]
		</SPAN>
		   <SPAN>
			 Kay Khusraw makes a Feast
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861282" TARGET="almagest" NAME="02.03.06.01.01"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.06.01.01]
		</SPAN>
		   <SPAN>
			 The Prelude
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861284" TARGET="almagest" NAME="02.03.06.01.02"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.06.01.02]
		</SPAN>
		   <SPAN>
			 Afrasiyab calls together his Host
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861286" TARGET="almagest" NAME="02.03.06.01.03"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.06.01.03]
		</SPAN>
		   <SPAN>
			 Kay Khusraw sends Gudarz to fight the Turanian
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861288" TARGET="almagest" NAME="02.03.06.01.04"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.06.01.04]
		</SPAN>
		   <SPAN>
			 Giv is makes the Bearer of Overtures from Gudarz to Piran
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792732,'PECK-127-2','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792732&t=" ALT="Piran meets Bahram [PECK-127-2]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=868344" TARGET="almagest">PECK-127-2</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861291" TARGET="almagest" NAME="02.03.06.01.05"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.06.01.05]
		</SPAN>
		   <SPAN>
			 Giv visits Piran at Visahgird
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861293" TARGET="almagest" NAME="02.03.06.01.06"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.06.01.06]
		</SPAN>
		   <SPAN>
			 The Arraying of the Hosts
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861295" TARGET="almagest" NAME="02.03.06.01.07"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.06.01.07]
		</SPAN>
		   <SPAN>
			 Bizhan goes to Giv to urge him to fight
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861297" TARGET="almagest" NAME="02.03.06.01.08"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.06.01.08]
		</SPAN>
		   <SPAN>
			 Human asks Piran for leave to fight
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792738,'PECK-175-1','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792738&t=" ALT="Human challenges Iranian Champions [PECK-175-1]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=868374" TARGET="almagest">PECK-175-1</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861300" TARGET="almagest" NAME="02.03.06.01.09"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.06.01.09]
		</SPAN>
		   <SPAN>
			 Human challenges Ruhham
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861303" TARGET="almagest" NAME="02.03.06.01.10"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.06.01.10]
		</SPAN>
		   <SPAN>
			 Human challenges Fariburz
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861305" TARGET="almagest" NAME="02.03.06.01.11"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.06.01.11]
		</SPAN>
		   <SPAN>
			 Human challenges Gudarz
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861308" TARGET="almagest" NAME="02.03.06.01.12"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.06.01.12]
		</SPAN>
		   <SPAN>
			 Bizhan hears of the Doings of Human
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861310" TARGET="almagest" NAME="02.03.06.01.13"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.06.01.13]
		</SPAN>
		   <SPAN>
			 Giv gives the Mail of Siyavash to Bizhan
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861312" TARGET="almagest" NAME="02.03.06.01.14"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.06.01.14]
		</SPAN>
		   <SPAN>
			 Human comes to Battle with Bizhan
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861315" TARGET="almagest" NAME="02.03.06.01.15"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.06.01.15]
		</SPAN>
		   <SPAN>
			 Human is killed by Bizhan
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861318" TARGET="almagest" NAME="02.03.06.01.16"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.06.01.16]
		</SPAN>
		   <SPAN>
			 Nastihan makes a Night-attack and is killed
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792739,'PECK-179-1','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792739&t=" ALT="Bizhan slays Nastihan [PECK-179-1]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=868379" TARGET="almagest">PECK-179-1</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861321" TARGET="almagest" NAME="02.03.06.01.17"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.06.01.17]
		</SPAN>
		   <SPAN>
			 Gudarz asks Aid of Khusraw
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861324" TARGET="almagest" NAME="02.03.06.01.18"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.06.01.18]
		</SPAN>
		   <SPAN>
			 The Answer of Khusraw to the Letter of Gudarz
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861326" TARGET="almagest" NAME="02.03.06.01.19"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.06.01.19]
		</SPAN>
		   <SPAN>
			 Khusraw arrays the Host
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861328" TARGET="almagest" NAME="02.03.06.01.20"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.06.01.20]
		</SPAN>
		   <SPAN>
			 Piran writes to Gudarz, Son of Kishvad
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861330" TARGET="almagest" NAME="02.03.06.01.21"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.06.01.21]
		</SPAN>
		   <SPAN>
			 The Answer of Gudarz to the Letter of Piran
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861333" TARGET="almagest" NAME="02.03.06.01.22"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.06.01.22]
		</SPAN>
		   <SPAN>
			 Piran asks Succour from Afrasiyab
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861335" TARGET="almagest" NAME="02.03.06.01.23"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.06.01.23]
		</SPAN>
		   <SPAN>
			 The Answer of Afrasiyab to the Letter of Piran
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861337" TARGET="almagest" NAME="02.03.06.01.24"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.06.01.24]
		</SPAN>
		   <SPAN>
			 the Iranians and Turanian fight a pitched Battle
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861340" TARGET="almagest" NAME="02.03.06.01.25"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.06.01.25]
		</SPAN>
		   <SPAN>
			 Giv fights with Piran and Giv's Horse jibbed
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792740,'PECK-185-1','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792740&t=" ALT="Giv battles Piran [PECK-185-1]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=869445" TARGET="almagest">PECK-185-1</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861343" TARGET="almagest" NAME="02.03.06.01.26"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.06.01.26]
		</SPAN>
		   <SPAN>
			 Gudarz and Piran arrange a Battle of Eleven Rukhs
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861345" TARGET="almagest" NAME="02.03.06.01.27"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.06.01.27]
		</SPAN>
		   <SPAN>
			 Piran haranques his Men of Name
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861347" TARGET="almagest" NAME="02.03.06.01.28"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.06.01.28]
		</SPAN>
		   <SPAN>
			 Gudarz and Piran chose the Warriors for the Battle of the Eleven Rukhs
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861350" TARGET="almagest" NAME="02.03.06.01.29"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.06.01.29]
		</SPAN>
		   <SPAN>
			 Fariburz fights with Kulbad
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861353" TARGET="almagest" NAME="02.03.06.01.30"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.06.01.30]
		</SPAN>
		   <SPAN>
			 Giv fights with Guruy
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792741,'PECK-193-2','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792741&t=" ALT="Kay Khusraw has Guruy Killed [PECK-193-2]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=868384" TARGET="almagest">PECK-193-2</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861359" TARGET="almagest" NAME="02.03.06.01.31"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.06.01.31]
		</SPAN>
		   <SPAN>
			 Gurazah fights with Siyamak
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861362" TARGET="almagest" NAME="02.03.06.01.32"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.06.01.32]
		</SPAN>
		   <SPAN>
			 Furuhil fights with Zangulah
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861365" TARGET="almagest" NAME="02.03.06.01.33"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.06.01.33]
		</SPAN>
		   <SPAN>
			 Ruhham fights with Barman
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861368" TARGET="almagest" NAME="02.03.06.01.34"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.06.01.34]
		</SPAN>
		   <SPAN>
			 Bizhan fights with Ruin
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861371" TARGET="almagest" NAME="02.03.06.01.35"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.06.01.35]
		</SPAN>
		   <SPAN>
			 Hajir fights with Sipahram
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861374" TARGET="almagest" NAME="02.03.06.01.36"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.06.01.36]
		</SPAN>
		   <SPAN>
			 Gurgin fights with Andariman
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861378" TARGET="almagest" NAME="02.03.06.01.37"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.06.01.37]
		</SPAN>
		   <SPAN>
			 Bartah fights with Kuhram
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861380" TARGET="almagest" NAME="02.03.06.01.38"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.06.01.38]
		</SPAN>
		   <SPAN>
			 Zangah, Son of Shavaran, fights with Akhast
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861384" TARGET="almagest" NAME="02.03.06.01.39"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.06.01.39]
		</SPAN>
		   <SPAN>
			 Gudarz fights with Piran
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792784,'56G-270-2','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792784&t=" ALT="Piran escapes from Gudarz but later killed [56G-270-2]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=868733" TARGET="almagest">56G-270-2</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861388" TARGET="almagest" NAME="02.03.06.01.40"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.06.01.40]
		</SPAN>
		   <SPAN>
			 Gudarz returns to the Warriors of Iran
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861390" TARGET="almagest" NAME="02.03.06.01.41"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.06.01.41]
		</SPAN>
		   <SPAN>
			 Lahhak and Farshidvard bewails Piran
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861392" TARGET="almagest" NAME="02.03.06.01.42"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.06.01.42]
		</SPAN>
		   <SPAN>
			 Lahhak and Farshidvard take the Road to Turan
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861394" TARGET="almagest" NAME="02.03.06.01.43"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.06.01.43]
		</SPAN>
		   <SPAN>
			 Gustaham pursues Lahhak and Farshidvard
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861396" TARGET="almagest" NAME="02.03.06.01.44"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.06.01.44]
		</SPAN>
		   <SPAN>
			 Bizhan follows after Gustaham
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861398" TARGET="almagest" NAME="02.03.06.01.45"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.06.01.45]
		</SPAN>
		   <SPAN>
			 Lahhak and Farshid ward are killed by Gustaham
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861401" TARGET="almagest" NAME="02.03.06.01.46"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.06.01.46]
		</SPAN>
		   <SPAN>
			 Bizhan sees Gustaham in the Mead
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861405" TARGET="almagest" NAME="02.03.06.01.47"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.06.01.47]
		</SPAN>
		   <SPAN>
			 Kay Khusraw buids a Charnel-house for Piran and for the other Chiefs of Turan, and how he kills Guruy the Son of Zira
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792842,'59G-245-2','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792842&t=" ALT="Kay Khusraw kills Guruy [59G-245-2]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=868861" TARGET="almagest">59G-245-2</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861410" TARGET="almagest" NAME="02.03.06.01.48"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.06.01.48]
		</SPAN>
		   <SPAN>
			 the Turanian ask Quarter of Kay Khusraw
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861412" TARGET="almagest" NAME="02.03.06.01.49"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.06.01.49]
		</SPAN>
		   <SPAN>
			 Bizhan returns with Gustaham
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861415" TARGET="almagest" NAME="02.03.07.01.01"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.07.01.01]
		</SPAN>
		   <SPAN>
			 In Praise of Sultan Mahmud
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861417" TARGET="almagest" NAME="02.03.07.01.02"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.07.01.02]
		</SPAN>
		   <SPAN>
			 Kay Khusraw arrays his Host against Afrasiyab
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861420" TARGET="almagest" NAME="02.03.07.01.03"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.07.01.03]
		</SPAN>
		   <SPAN>
			 Afrasiyab hears that Piran is killed and that Kay Khusraw has arrayed his Host
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861422" TARGET="almagest" NAME="02.03.07.01.04"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.07.01.04]
		</SPAN>
		   <SPAN>
			 Kay Khusraw has Tidings that Afrasiyab advances to fight with him
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861424" TARGET="almagest" NAME="02.03.07.01.05"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.07.01.05]
		</SPAN>
		   <SPAN>
			 Shidah comes before his Father Afrasiyab
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861426" TARGET="almagest" NAME="02.03.07.01.06"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.07.01.06]
		</SPAN>
		   <SPAN>
			 Afrasiyab sends an Embassage to Kay Khusraw
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861429" TARGET="almagest" NAME="02.03.07.01.07"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.07.01.07]
		</SPAN>
		   <SPAN>
			 Kay Khusraw sends an Answer to Afrasiyab
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861432" TARGET="almagest" NAME="02.03.07.01.08"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.07.01.08]
		</SPAN>
		   <SPAN>
			 Kay Khusraw fights with Shidah the Son of Afrasiyab
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861435" TARGET="almagest" NAME="02.03.07.01.09"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.07.01.09]
		</SPAN>
		   <SPAN>
			 Shada is killed by Khusraw
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861438" TARGET="almagest" NAME="02.03.07.01.10"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.07.01.10]
		</SPAN>
		   <SPAN>
			 the Battle is joined between the Hosts
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792785,'56G-287-1','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792785&t=" ALT="Kay Khusraw, Iranians triumph [56G-287-1]" HEIGHT="100"></A>
				</TD>
				
				 <TD>
					<A HREF="javascript:viewImage(792742,'PECK-202-1','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792742&t=" ALT="Kay Khusraw encounters Ustugila, Ila, Burzu'ila [PECK-202-1]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=868738" TARGET="almagest">56G-287-1</A>
				</TD>
				
				 <TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=868389" TARGET="almagest">PECK-202-1</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861443" TARGET="almagest" NAME="02.03.07.01.11"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.07.01.11]
		</SPAN>
		   <SPAN>
			 Afrasiyab flees
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861447" TARGET="almagest" NAME="02.03.07.01.12"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.07.01.12]
		</SPAN>
		   <SPAN>
			 Kay Khusraw announces his Victory to Kaus
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861450" TARGET="almagest" NAME="02.03.07.01.13"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.07.01.13]
		</SPAN>
		   <SPAN>
			 Afrasiyab goes to Gang Bihish
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861452" TARGET="almagest" NAME="02.03.07.01.14"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.07.01.14]
		</SPAN>
		   <SPAN>
			 Khusraw crossed the Jayhun
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861454" TARGET="almagest" NAME="02.03.07.01.15"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.07.01.15]
		</SPAN>
		   <SPAN>
			 Kay Khusraw fights with Afrasiyab the second Time
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861456" TARGET="almagest" NAME="02.03.07.01.16"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.07.01.16]
		</SPAN>
		   <SPAN>
			 Afrasiyab takes Refuge in Gang Bihish
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861458" TARGET="almagest" NAME="02.03.07.01.17"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.07.01.17]
		</SPAN>
		   <SPAN>
			 The Letter of Afrasiyab to the Gang Dizh
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861460" TARGET="almagest" NAME="02.03.07.01.18"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.07.01.18]
		</SPAN>
		   <SPAN>
			 Kay Khusraw arrives before Gang Bihish
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861462" TARGET="almagest" NAME="02.03.07.01.19"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.07.01.19]
		</SPAN>
		   <SPAN>
			 Jahn comes to Kay Khusraw with an Embassage from Afrasiyab
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861464" TARGET="almagest" NAME="02.03.07.01.20"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.07.01.20]
		</SPAN>
		   <SPAN>
			 Kay Khusraw makes Answer to Jahn
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861466" TARGET="almagest" NAME="02.03.07.01.21"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.07.01.21]
		</SPAN>
		   <SPAN>
			 Kay Khusraw fights with Afrasiyab and takes Gang Bihish
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861468" TARGET="almagest" NAME="02.03.07.01.22"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.07.01.22]
		</SPAN>
		   <SPAN>
			 Afrasiyab flees from Gang Bihish
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861470" TARGET="almagest" NAME="02.03.07.01.23"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.07.01.23]
		</SPAN>
		   <SPAN>
			 Kay Khusraw gives Quarter to the Family of Afrasiyab
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861472" TARGET="almagest" NAME="02.03.07.01.24"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.07.01.24]
		</SPAN>
		   <SPAN>
			 Kay Khusraw exhorts the Iranians
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861474" TARGET="almagest" NAME="02.03.07.01.25"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.07.01.25]
		</SPAN>
		   <SPAN>
			 Kay Khusraw writes a Letter with the News of his Victory to Kai Ka'us
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861476" TARGET="almagest" NAME="02.03.07.01.26"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.07.01.26]
		</SPAN>
		   <SPAN>
			 Kay Khusraw has Tidings of the Coming of Afrasiyab with the Host of the Faghfur
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861478" TARGET="almagest" NAME="02.03.07.01.27"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.07.01.27]
		</SPAN>
		   <SPAN>
			 The Message of Afrasiyab to Kay Khusraw
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861480" TARGET="almagest" NAME="02.03.07.01.28"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.07.01.28]
		</SPAN>
		   <SPAN>
			 the Iranians and Turanian fights
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861482" TARGET="almagest" NAME="02.03.07.01.29"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.07.01.29]
		</SPAN>
		   <SPAN>
			 Afrasiyab makes a Night-attack upon Kay Khusraw and is defeated
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861485" TARGET="almagest" NAME="02.03.07.01.30"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.07.01.30]
		</SPAN>
		   <SPAN>
			 the Gang Dizh sends an Envoy to Kay Khusraw
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861487" TARGET="almagest" NAME="02.03.07.01.31"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.07.01.31]
		</SPAN>
		   <SPAN>
			 Afrasiyab crosses the Sea
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861489" TARGET="almagest" NAME="02.03.07.01.32"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.07.01.32]
		</SPAN>
		   <SPAN>
			 Kay Khusraw sends the Prisoners and Treasure to Kaus with a Letter
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861492" TARGET="almagest" NAME="02.03.07.01.33"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.07.01.33]
		</SPAN>
		   <SPAN>
			 The Answer of Shah Kaus to the Letter of Khusraw
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861494" TARGET="almagest" NAME="02.03.07.01.34"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.07.01.34]
		</SPAN>
		   <SPAN>
			 The Embassage of Kay Khusraw to the Gang Dizh and the King of Makran
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861496" TARGET="almagest" NAME="02.03.07.01.35"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.07.01.35]
		</SPAN>
		   <SPAN>
			 Kay Khusraw fights with the King of Makran and how the King of Makran is killed
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861499" TARGET="almagest" NAME="02.03.07.01.36"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.07.01.36]
		</SPAN>
		   <SPAN>
			 Kay Khusraw crosses the Sea
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792743,'PECK-212-2','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792743&t=" ALT="Kay Khusraw sees marvels of sea [PECK-212-2]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=868394" TARGET="almagest">PECK-212-2</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861502" TARGET="almagest" NAME="02.03.07.01.37"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.07.01.37]
		</SPAN>
		   <SPAN>
			 Kay Khusraw reaches Gang Dizh
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861506" TARGET="almagest" NAME="02.03.07.01.38"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.07.01.38]
		</SPAN>
		   <SPAN>
			 Kay Khusraw returns from Gang Dizh to Siyavashgird
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861508" TARGET="almagest" NAME="02.03.07.01.39"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.07.01.39]
		</SPAN>
		   <SPAN>
			 Kay Khusraw returns from Turan to the Land of Iran
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861510" TARGET="almagest" NAME="02.03.07.01.40"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.07.01.40]
		</SPAN>
		   <SPAN>
			 Kay Khusraw returns to his Grandsire
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861512" TARGET="almagest" NAME="02.03.07.01.41"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.07.01.41]
		</SPAN>
		   <SPAN>
			 Afrasiyab is captured by Hum of the Race of Faridun
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861515" TARGET="almagest" NAME="02.03.07.01.42"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.07.01.42]
		</SPAN>
		   <SPAN>
			 Afrasiyab escapes from Hum
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861517" TARGET="almagest" NAME="02.03.07.01.43"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.07.01.43]
		</SPAN>
		   <SPAN>
			 Kaus and Khusraw come to Hum
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861519" TARGET="almagest" NAME="02.03.07.01.44"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.07.01.44]
		</SPAN>
		   <SPAN>
			 Afrasiyab is taken the second Time and how he and Garsivaz are killed
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792786,'56G-307-2','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792786&t=" ALT="Kay Khusraw slays Afrasiyab while Garsivaz [56G-307-2]" HEIGHT="100"></A>
				</TD>
				
				 <TD>
					<A HREF="javascript:viewImage(792814,'57G-272-2','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792814&t=" ALT="Kay Khusraw slays Afrasiyab while Garsivaz [57G-272-2]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=868743" TARGET="almagest">56G-307-2</A>
				</TD>
				
				 <TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=868612" TARGET="almagest">57G-272-2</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861525" TARGET="almagest" NAME="02.03.07.01.45"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.07.01.45]
		</SPAN>
		   <SPAN>
			 Kaus and Khusraw returns to Pars
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861527" TARGET="almagest" NAME="02.03.07.01.46"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.07.01.46]
		</SPAN>
		   <SPAN>
			 The Death of Kai Ka'us
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861529" TARGET="almagest" NAME="02.03.07.01.47"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.07.01.47]
		</SPAN>
		   <SPAN>
			 Kay Khusraw falls into Melancholy
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861531" TARGET="almagest" NAME="02.03.07.01.48"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.07.01.48]
		</SPAN>
		   <SPAN>
			 the Nobles inquire why Khusraw has closed his Court
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861533" TARGET="almagest" NAME="02.03.07.01.49"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.07.01.49]
		</SPAN>
		   <SPAN>
			 the Iranians summon Zal and Rustam
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861535" TARGET="almagest" NAME="02.03.07.01.50"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.07.01.50]
		</SPAN>
		   <SPAN>
			 Kay Khusraw sees Surush in a Dream
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861537" TARGET="almagest" NAME="02.03.07.01.51"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.07.01.51]
		</SPAN>
		   <SPAN>
			 Zal admonishes Kay Khusraw
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861540" TARGET="almagest" NAME="02.03.07.01.52"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.07.01.52]
		</SPAN>
		   <SPAN>
			 Kay Khusraw answers Zal
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861542" TARGET="almagest" NAME="02.03.07.01.53"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.07.01.53]
		</SPAN>
		   <SPAN>
			 Zal rebukes Kay Khusraw
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861544" TARGET="almagest" NAME="02.03.07.01.54"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.07.01.54]
		</SPAN>
		   <SPAN>
			 Kay Khusraw answers and how Zal excuses himself
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861546" TARGET="almagest" NAME="02.03.07.01.55"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.07.01.55]
		</SPAN>
		   <SPAN>
			 Kay Khusraw gives his last Charge to the Iranians
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861548" TARGET="almagest" NAME="02.03.07.01.56"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.07.01.56]
		</SPAN>
		   <SPAN>
			 Kay Khusraw appoints Gudarz to be his Mandatary
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792744,'PECK-222-2','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792744&t=" ALT="Kay Khusraw makes appointments, gives up throne [PECK-222-2]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=868399" TARGET="almagest">PECK-222-2</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861551" TARGET="almagest" NAME="02.03.07.01.57"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.07.01.57]
		</SPAN>
		   <SPAN>
			 Zal asks of Kay Khusraw a Patent for Rustam
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861553" TARGET="almagest" NAME="02.03.07.01.58"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.07.01.58]
		</SPAN>
		   <SPAN>
			 Kay Khusraw gives a Patent to Giv
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861555" TARGET="almagest" NAME="02.03.07.01.59"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.07.01.59]
		</SPAN>
		   <SPAN>
			 Kai Khusrai gives a Patent to Tus
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861557" TARGET="almagest" NAME="02.03.07.01.60"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.07.01.60]
		</SPAN>
		   <SPAN>
			 Kay Khusraw gives the Kingship to Luhrasp
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861559" TARGET="almagest" NAME="02.03.07.01.61"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.07.01.61]
		</SPAN>
		   <SPAN>
			 Kay Khusraw farewells his Women
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861562" TARGET="almagest" NAME="02.03.07.01.62"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.07.01.62]
		</SPAN>
		   <SPAN>
			 Kay Khusraw goes to the Mountains and vanishes in the Snow
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861565" TARGET="almagest" NAME="02.03.07.01.63"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.07.01.63]
		</SPAN>
		   <SPAN>
			 the Pahlavan are lost in the Snow
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861569" TARGET="almagest" NAME="02.03.07.01.64"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.03.07.01.64]
		</SPAN>
		   <SPAN>
			 Luhrasp has Tidings of the Disappearance of Kay Khusraw
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR>
	<TD COLSPAN="2" STYLE="font-weight:bold;color:white;background-color:gray;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861572" STYLE="color:white;" TARGET="almagest">2.4 LUHRASP</A>
	</TD>
</TR>

<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861574" TARGET="almagest" NAME="02.04.01.01.01"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.04.01.01.01]
		</SPAN>
		   <SPAN>
			 Luhrasp builds a Fire-temple at Balkh
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792745,'PECK-223-1','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792745&t=" ALT="Luhrasp enthroned [PECK-223-1]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=868404" TARGET="almagest">PECK-223-1</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861577" TARGET="almagest" NAME="02.04.01.01.02"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.04.01.01.02]
		</SPAN>
		   <SPAN>
			 Gushtasp quits Luhrasp in wrath
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861580" TARGET="almagest" NAME="02.04.01.01.03"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.04.01.01.03]
		</SPAN>
		   <SPAN>
			 Gushtasp returns with Zarir
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861583" TARGET="almagest" NAME="02.04.01.01.04"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.04.01.01.04]
		</SPAN>
		   <SPAN>
			 Gushtasp sets off for Rum
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861585" TARGET="almagest" NAME="02.04.01.01.05"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.04.01.01.05]
		</SPAN>
		   <SPAN>
			 Gushtasp arrives in Rum
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861588" TARGET="almagest" NAME="02.04.01.01.06"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.04.01.01.06]
		</SPAN>
		   <SPAN>
			 a Dihqan entertains Gushtasp
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861590" TARGET="almagest" NAME="02.04.01.01.07"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.04.01.01.07]
		</SPAN>
		   <SPAN>
			 The Story of Katayun the Daughter of C?sar
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861592" TARGET="almagest" NAME="02.04.01.01.08"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.04.01.01.08]
		</SPAN>
		   <SPAN>
			 C?sar gsves Katayun to Gushtasp
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861595" TARGET="almagest" NAME="02.04.01.01.09"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.04.01.01.09]
		</SPAN>
		   <SPAN>
			 Mirin asks in Marriage C?sar's second Daughter
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861597" TARGET="almagest" NAME="02.04.01.01.10"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.04.01.01.10]
		</SPAN>
		   <SPAN>
			 Gushtasp kills the Wolf
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792787,'56G-323-1','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792787&t=" ALT="Gushtasp slays wolf [56G-323-1]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=868746" TARGET="almagest">56G-323-1</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861600" TARGET="almagest" NAME="02.04.01.01.11"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.04.01.01.11]
		</SPAN>
		   <SPAN>
			 Ahran asks C?sar's third Daughter in Marriage
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861602" TARGET="almagest" NAME="02.04.01.01.12"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.04.01.01.12]
		</SPAN>
		   <SPAN>
			 Gushtasp kills the Dragon and how C?sar gsves his Daughter to Ahran
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792843,'59G-261-2','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792843&t=" ALT="Gushtasp slays dragon [59G-261-2]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=868866" TARGET="almagest">59G-261-2</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861605" TARGET="almagest" NAME="02.04.01.01.13"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.04.01.01.13]
		</SPAN>
		   <SPAN>
			 Gushtasp displays his Prowess on the Riding-ground
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792746,'PECK-229-1','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792746&t=" ALT="Gushtasp plays polo before Caesar [PECK-229-1]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=868409" TARGET="almagest">PECK-229-1</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861608" TARGET="almagest" NAME="02.04.01.01.14"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.04.01.01.14]
		</SPAN>
		   <SPAN>
			 C?sar writes to Ilyas and demands Tribute
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861611" TARGET="almagest" NAME="02.04.01.01.15"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.04.01.01.15]
		</SPAN>
		   <SPAN>
			 Gushtasp fights with Ilyas and kills him
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861614" TARGET="almagest" NAME="02.04.01.01.16"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.04.01.01.16]
		</SPAN>
		   <SPAN>
			 C?sar demands from Luhrasp Tribute for Iran
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861616" TARGET="almagest" NAME="02.04.01.01.17"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.04.01.01.17]
		</SPAN>
		   <SPAN>
			 Zarir carries a Message from Luhrasp to C?sar
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861620" TARGET="almagest" NAME="02.04.01.01.18"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.04.01.01.18]
		</SPAN>
		   <SPAN>
			 Gushtasp returns with Zarir to the Land of Iran and receives the Throne from Luhrasp
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR>
	<TD COLSPAN="2" STYLE="font-weight:bold;color:white;background-color:gray;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861625" STYLE="color:white;" TARGET="almagest">2.5 GUSHTASP</A>
	</TD>
</TR>

<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861627" TARGET="almagest" NAME="02.05.01.01.01"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.05.01.01.01]
		</SPAN>
		   <SPAN>
			 Firdawsi sees Daqiqi in a Dream
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861629" TARGET="almagest" NAME="02.05.01.01.02"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.05.01.01.02]
		</SPAN>
		   <SPAN>
			 Luhrasp goes to Balkh and how Gushtasp sits upon the Throne
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861631" TARGET="almagest" NAME="02.05.01.01.03"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.05.01.01.03]
		</SPAN>
		   <SPAN>
			 Zardusht  appears and how Gushtasp accepts his Evangel
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861633" TARGET="almagest" NAME="02.05.01.01.04"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.05.01.01.04]
		</SPAN>
		   <SPAN>
			 Gushtasp refuses to Arjasp the Tribute for Iran
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861635" TARGET="almagest" NAME="02.05.01.01.05"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.05.01.01.05]
		</SPAN>
		   <SPAN>
			 Arjasp writes a Letter to Gushtasp
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861637" TARGET="almagest" NAME="02.05.01.01.06"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.05.01.01.06]
		</SPAN>
		   <SPAN>
			 Arjasp sends Envoys to Gushtasp
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861639" TARGET="almagest" NAME="02.05.01.01.07"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.05.01.01.07]
		</SPAN>
		   <SPAN>
			 Zarir makes Answer to Arjasp
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861641" TARGET="almagest" NAME="02.05.01.01.08"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.05.01.01.08]
		</SPAN>
		   <SPAN>
			 the Envoys return to Arjasp with the Letter of Gushtasp
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861643" TARGET="almagest" NAME="02.05.01.01.09"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.05.01.01.09]
		</SPAN>
		   <SPAN>
			 Gushtasp assembles his Troops
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861645" TARGET="almagest" NAME="02.05.01.01.10"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.05.01.01.10]
		</SPAN>
		   <SPAN>
			 Jamasp foretells the Issue of the Battle to Gushtasp
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861647" TARGET="almagest" NAME="02.05.01.01.11"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.05.01.01.11]
		</SPAN>
		   <SPAN>
			 Gushtasp and Arjasp array their Hosts
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861650" TARGET="almagest" NAME="02.05.01.01.12"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.05.01.01.12]
		</SPAN>
		   <SPAN>
			 The Beginning of the Battle between the Iranians and Turanian, and how Ardshir, Shiru, and Shidasp are killed
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861652" TARGET="almagest" NAME="02.05.01.01.13"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.05.01.01.13]
		</SPAN>
		   <SPAN>
			 Girami, Jamasp's Son, and Nawzar are killed
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861655" TARGET="almagest" NAME="02.05.01.01.14"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.05.01.01.14]
		</SPAN>
		   <SPAN>
			 Zarir, the Brother of Gushtasp, is killed by Bidirafsh
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861659" TARGET="almagest" NAME="02.05.01.01.15"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.05.01.01.15]
		</SPAN>
		   <SPAN>
			 Isfandiyar hears of the Slaying of Zarir
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861661" TARGET="almagest" NAME="02.05.01.01.16"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.05.01.01.16]
		</SPAN>
		   <SPAN>
			 Isfandiyar goes to battle with Arjasp
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861663" TARGET="almagest" NAME="02.05.01.01.17"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.05.01.01.17]
		</SPAN>
		   <SPAN>
			 Nastur and Isfandiyar kill Bidirafsh
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792788,'56G-338-2','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792788&t=" ALT="Isfandiyar kills Bidirafsh [56G-338-2]" HEIGHT="100"></A>
				</TD>
				
				 <TD>
					<A HREF="javascript:viewImage(792747,'PECK-238-2','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792747&t=" ALT="Isfandiyar kills Bidirafsh [PECK-238-2]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=868751" TARGET="almagest">56G-338-2</A>
				</TD>
				
				 <TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=868419" TARGET="almagest">PECK-238-2</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861666" TARGET="almagest" NAME="02.05.01.01.18"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.05.01.01.18]
		</SPAN>
		   <SPAN>
			 Arjasp flees from the Battle
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861668" TARGET="almagest" NAME="02.05.01.01.19"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.05.01.01.19]
		</SPAN>
		   <SPAN>
			 The Turkmans receive Quarter from Isfandiyar
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861670" TARGET="almagest" NAME="02.05.01.01.20"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.05.01.01.20]
		</SPAN>
		   <SPAN>
			 Gushtasp returns to Balkh
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861672" TARGET="almagest" NAME="02.05.01.01.21"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.05.01.01.21]
		</SPAN>
		   <SPAN>
			 Gushtasp sends Isfandiyar to all the Provinces, and how the Folk receive from him the good Religion
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861674" TARGET="almagest" NAME="02.05.01.01.22"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.05.01.01.22]
		</SPAN>
		   <SPAN>
			 Gurazm speaks Evil of Isfandiyar
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861676" TARGET="almagest" NAME="02.05.01.01.23"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.05.01.01.23]
		</SPAN>
		   <SPAN>
			 Jamasp comes to Isfandiyar
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861678" TARGET="almagest" NAME="02.05.01.01.24"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.05.01.01.24]
		</SPAN>
		   <SPAN>
			 Gushtasp imprisons Isfandiyar
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861681" TARGET="almagest" NAME="02.05.01.01.25"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.05.01.01.25]
		</SPAN>
		   <SPAN>
			 Gushtasp goes to Sistan and how Arjasp arrays his Host the second Time
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861683" TARGET="almagest" NAME="02.05.01.01.26"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.05.01.01.26]
		</SPAN>
		   <SPAN>
			 The Words of Daqiqi being ended, Firdawsi resumes, praising Shah Mahmud and criticising Daqiqi
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861686" TARGET="almagest" NAME="02.05.01.01.27"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.05.01.01.27]
		</SPAN>
		   <SPAN>
			 the Host of Arjasp march to Balkh and how Luhrasp is killed
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861690" TARGET="almagest" NAME="02.05.01.01.28"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.05.01.01.28]
		</SPAN>
		   <SPAN>
			 Gushtasp hears of the Slaying of Luhrasp and leads his Army toward Balkh
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861692" TARGET="almagest" NAME="02.05.01.01.29"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.05.01.01.29]
		</SPAN>
		   <SPAN>
			 Gushtasp is put to Flight by Arjasp
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861695" TARGET="almagest" NAME="02.05.01.01.30"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.05.01.01.30]
		</SPAN>
		   <SPAN>
			 Jamasp visits Isfandiyar
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861699" TARGET="almagest" NAME="02.05.01.01.31"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.05.01.01.31]
		</SPAN>
		   <SPAN>
			 Isfandiyar sees his Brother Farshidvard
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861701" TARGET="almagest" NAME="02.05.01.01.32"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.05.01.01.32]
		</SPAN>
		   <SPAN>
			 Isfandiyar comes to the Mountain to Gushtasp
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861703" TARGET="almagest" NAME="02.05.01.01.33"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.05.01.01.33]
		</SPAN>
		   <SPAN>
			 Gushtasp sends Isfandiyar the second Time to fight Arjasp
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792748,'PECK-246-1','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792748&t=" ALT="Isfandiyar lassoes Turanian Gurgsar [PECK-246-1]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=868424" TARGET="almagest">PECK-246-1</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861708" TARGET="almagest" NAME="02.05.02.01.01"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.05.02.01.01]
		</SPAN>
		   <SPAN>
			 The First Stage Isfandiyar kills two Wolves
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792789,'56G-350-2','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792789&t=" ALT="Isfandiyar first exploit: Fights wolves [56G-350-2]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=868754" TARGET="almagest">56G-350-2</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861711" TARGET="almagest" NAME="02.05.02.01.02"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.05.02.01.02]
		</SPAN>
		   <SPAN>
			 The Second Stage Isfandiyar kills two Lions
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861714" TARGET="almagest" NAME="02.05.02.01.03"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.05.02.01.03]
		</SPAN>
		   <SPAN>
			 The Third Stage Isfandiyar kills a Dragon
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792816,'57G-307-2','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792816&t=" ALT="Isfandiyar third exploit: Battles dragon [57G-307-2]" HEIGHT="100"></A>
				</TD>
				
				 <TD>
					<A HREF="javascript:viewImage(792845,'59G-286-1','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792845&t=" ALT="Isfandiyar third exploit: Battles dragon [59G-286-1]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=868622" TARGET="almagest">57G-307-2</A>
				</TD>
				
				 <TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=869358" TARGET="almagest">59G-286-1</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861717" TARGET="almagest" NAME="02.05.02.01.04"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.05.02.01.04]
		</SPAN>
		   <SPAN>
			 The Fourth Stage Isfandiyar kills a Witch
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861720" TARGET="almagest" NAME="02.05.02.01.05"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.05.02.01.05]
		</SPAN>
		   <SPAN>
			 The Fifth Stage Isfandiyar kills the Simah Barzin
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792790,'56G-353-1','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792790&t=" ALT="Isfandiyar fifth exploit: Kills Simah Barzin [56G-353-1]" HEIGHT="100"></A>
				</TD>
				
				 <TD>
					<A HREF="javascript:viewImage(792815,'57G-306-2','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792815&t=" ALT="Isfandiyar fifth exploit: Kills Simah Barzin [57G-306-2]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=868759" TARGET="almagest">56G-353-1</A>
				</TD>
				
				 <TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=868617" TARGET="almagest">57G-306-2</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861723" TARGET="almagest" NAME="02.05.02.01.06"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.05.02.01.06]
		</SPAN>
		   <SPAN>
			 The Sixth Stage Isfandiyar passes through the Snow
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861726" TARGET="almagest" NAME="02.05.02.01.07"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.05.02.01.07]
		</SPAN>
		   <SPAN>
			 The Seventh Stage Isfandiyar crosses the River and kills Gurgsar
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861730" TARGET="almagest" NAME="02.05.02.02.01"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.05.02.02.01]
		</SPAN>
		   <SPAN>
			 Isfandiyar go to the Brazen Hold in the Guise of a Merchant
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792749,'PECK-252-1','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792749&t=" ALT="Isfandiyar disguised as merchant before Arjasp [PECK-252-1]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=868429" TARGET="almagest">PECK-252-1</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861733" TARGET="almagest" NAME="02.05.02.02.02"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.05.02.02.02]
		</SPAN>
		   <SPAN>
			 the Sisters of Isfandiyar recognises him
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861736" TARGET="almagest" NAME="02.05.02.02.03"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.05.02.02.03]
		</SPAN>
		   <SPAN>
			 Pashutan assaults the Brazen Hold
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861739" TARGET="almagest" NAME="02.05.02.02.04"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.05.02.02.04]
		</SPAN>
		   <SPAN>
			 Isfandiyar kills Arjasp
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861743" TARGET="almagest" NAME="02.05.02.02.05"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.05.02.02.05]
		</SPAN>
		   <SPAN>
			 Isfandiyar kills Kuhram
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861747" TARGET="almagest" NAME="02.05.02.02.06"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.05.02.02.06]
		</SPAN>
		   <SPAN>
			 Isfandiyar writes a Letter to Gushtasp and his Answer
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861749" TARGET="almagest" NAME="02.05.02.02.07"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.05.02.02.07]
		</SPAN>
		   <SPAN>
			 Isfandiyar returns to Gushtasp
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792791,'56G-361-1','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792791&t=" ALT="Gushtasp receives Isfandiyar [56G-361-1]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=868762" TARGET="almagest">56G-361-1</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861752" TARGET="almagest" NAME="02.05.03.01.01"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.05.03.01.01]
		</SPAN>
		   <SPAN>
			 Isfandiyar ambitions the Throne and how Gushtasp takes Counsel with the Astrologers
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861754" TARGET="almagest" NAME="02.05.03.01.02"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.05.03.01.02]
		</SPAN>
		   <SPAN>
			 Isfandiyar demands the Kingdom from his Father
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792928,'ILKH-143-1','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792928&t=" ALT="Isfandiyar demands Kingdom from father Gushtasp [ILKH-143-1]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=869382" TARGET="almagest">ILKH-143-1</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861757" TARGET="almagest" NAME="02.05.03.01.03"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.05.03.01.03]
		</SPAN>
		   <SPAN>
			 Gushtasp answers his Son
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861759" TARGET="almagest" NAME="02.05.03.01.04"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.05.03.01.04]
		</SPAN>
		   <SPAN>
			 Katayun counsels Isfandiyar
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861761" TARGET="almagest" NAME="02.05.03.01.05"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.05.03.01.05]
		</SPAN>
		   <SPAN>
			 Isfandiyar leads a Host to Zabul
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861763" TARGET="almagest" NAME="02.05.03.01.06"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.05.03.01.06]
		</SPAN>
		   <SPAN>
			 Isfandiyar sends Bahman to Rustam
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861765" TARGET="almagest" NAME="02.05.03.01.07"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.05.03.01.07]
		</SPAN>
		   <SPAN>
			 Bahman comes to Zal
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792929,'ILKH-144-2','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792929&t=" ALT="Zal greets Bahman [ILKH-144-2]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=869185" TARGET="almagest">ILKH-144-2</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861768" TARGET="almagest" NAME="02.05.03.01.08"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.05.03.01.08]
		</SPAN>
		   <SPAN>
			 Bahman gives the Message to Rustam
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792792,'56G-367-2','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792792&t=" ALT="Rustam kicks aside rocked pushed by Bahman [56G-367-2]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=868767" TARGET="almagest">56G-367-2</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861772" TARGET="almagest" NAME="02.05.03.01.09"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.05.03.01.09]
		</SPAN>
		   <SPAN>
			 Rustam makes Answer to Isfandiyar
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861774" TARGET="almagest" NAME="02.05.03.01.10"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.05.03.01.10]
		</SPAN>
		   <SPAN>
			 Bahman returns
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861776" TARGET="almagest" NAME="02.05.03.01.11"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.05.03.01.11]
		</SPAN>
		   <SPAN>
			 The Meeting of Rustam and Isfandiyar
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792750,'PECK-263-1','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792750&t=" ALT="Rustam debates Isfandiyar [PECK-263-1]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=868434" TARGET="almagest">PECK-263-1</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861780" TARGET="almagest" NAME="02.05.03.01.12"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.05.03.01.12]
		</SPAN>
		   <SPAN>
			 Isfandiyar summons not Rustam to the Feast
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861782" TARGET="almagest" NAME="02.05.03.01.13"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.05.03.01.13]
		</SPAN>
		   <SPAN>
			 Isfandiyar excuses himself for not summoning Rustam to the Feast
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861785" TARGET="almagest" NAME="02.05.03.01.14"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.05.03.01.14]
		</SPAN>
		   <SPAN>
			 Isfandiyar speakes Shame of the Race of Rustam
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861787" TARGET="almagest" NAME="02.05.03.01.15"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.05.03.01.15]
		</SPAN>
		   <SPAN>
			 Rustam answers Isfandiyar, praising his own Race and his Deeds
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792793,'56G-375-1','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792793&t=" ALT="Rustam boasts during trial of strength with Isfandiyar [56G-375-1]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=868772" TARGET="almagest">56G-375-1</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861790" TARGET="almagest" NAME="02.05.03.01.16"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.05.03.01.16]
		</SPAN>
		   <SPAN>
			 Isfandiyar boasts of his Ancestry
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861792" TARGET="almagest" NAME="02.05.03.01.17"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.05.03.01.17]
		</SPAN>
		   <SPAN>
			 Rustam vaunts his Valour
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861794" TARGET="almagest" NAME="02.05.03.01.18"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.05.03.01.18]
		</SPAN>
		   <SPAN>
			 Rustam drinks Wine with Isfandiyar
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861796" TARGET="almagest" NAME="02.05.03.01.19"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.05.03.01.19]
		</SPAN>
		   <SPAN>
			 Rustam returns to his Palace
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861798" TARGET="almagest" NAME="02.05.03.01.20"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.05.03.01.20]
		</SPAN>
		   <SPAN>
			 Zal counsells Rustam
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861800" TARGET="almagest" NAME="02.05.03.01.21"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.05.03.01.21]
		</SPAN>
		   <SPAN>
			 Rustam fights with Isfandiyar
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792794,'56G-380-2','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792794&t=" ALT="Combat Rustam, Isfandiyar [56G-380-2]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=868777" TARGET="almagest">56G-380-2</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861803" TARGET="almagest" NAME="02.05.03.01.22"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.05.03.01.22]
		</SPAN>
		   <SPAN>
			 the Sons of Isfandiyar are killed by Zavarah and Faramarz
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792931,'ILKH-149-2','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792931&t=" ALT="Zavarah, Faramarz slay Nush Azar, Mihr Nush [ILKH-149-2]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=869190" TARGET="almagest">ILKH-149-2</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861806" TARGET="almagest" NAME="02.05.03.01.23"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.05.03.01.23]
		</SPAN>
		   <SPAN>
			 Rustam flees to the Heights
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861809" TARGET="almagest" NAME="02.05.03.01.24"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.05.03.01.24]
		</SPAN>
		   <SPAN>
			 Rustam takes Counsel with his Kin
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861811" TARGET="almagest" NAME="02.05.03.01.25"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.05.03.01.25]
		</SPAN>
		   <SPAN>
			 the Simah Barzin succours Rustam
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861814" TARGET="almagest" NAME="02.05.03.01.26"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.05.03.01.26]
		</SPAN>
		   <SPAN>
			 Rustam goes back to fight Isfandiyar
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792930,'ILKH-149-1','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792930&t=" ALT="Rustam, Isfandiyar fight again [ILKH-149-1]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=869195" TARGET="almagest">ILKH-149-1</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861818" TARGET="almagest" NAME="02.05.03.01.27"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.05.03.01.27]
		</SPAN>
		   <SPAN>
			 Rustam shoots Isfandiyar in the Eyes with an Arrow
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792932,'ILKH-152-1','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792932&t=" ALT="Rustam shoots double-pointed arrow in Isfandiyar eyes [ILKH-152-1]" HEIGHT="100"></A>
				</TD>
				
				 <TD>
					<A HREF="javascript:viewImage(792883,'58G-324-1','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792883&t=" ALT="Rustam shoots double-pointed arrow in Isfandiyar eyes [58G-324-1]" HEIGHT="100"></A>
				</TD>
				
				 <TD>
					<A HREF="javascript:viewImage(792821,'57G-386-1','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792821&t=" ALT="Rustam shoots double-pointed arrow in Isfandiyar eyes [57G-386-1]" HEIGHT="100"></A>
				</TD>
				
				 <TD>
					<A HREF="javascript:viewImage(792751,'PECK-268-1','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792751&t=" ALT="Rustam shoots double-pointed arrow in Isfandiyar eyes [PECK-268-1]" HEIGHT="100"></A>
				</TD>
				
				 <TD>
					<A HREF="javascript:viewImage(792795,'56G-386-2','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792795&t=" ALT="Rustam Slays Isfandiyar [56G-386-2]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=869200" TARGET="almagest">ILKH-152-1</A>
				</TD>
				
				 <TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=869012" TARGET="almagest">58G-324-1</A>
				</TD>
				
				 <TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=868645" TARGET="almagest">57G-386-1</A>
				</TD>
				
				 <TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=868439" TARGET="almagest">PECK-268-1</A>
				</TD>
				
				 <TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=868782" TARGET="almagest">56G-386-2</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861822" TARGET="almagest" NAME="02.05.03.01.28"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.05.03.01.28]
		</SPAN>
		   <SPAN>
			 Isfandiyar tells his last Wishes to Rustam
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861825" TARGET="almagest" NAME="02.05.03.01.29"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.05.03.01.29]
		</SPAN>
		   <SPAN>
			 Pashutan bears the Coffin of Isfandiyar to Gushtasp
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792933,'ILKH-153-1','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792933&t=" ALT="Isfandiyar mourned [ILKH-153-1]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=869203" TARGET="almagest">ILKH-153-1</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861828" TARGET="almagest" NAME="02.05.03.01.30"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.05.03.01.30]
		</SPAN>
		   <SPAN>
			 Rustam sends Bahman back to Iran
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861830" TARGET="almagest" NAME="02.05.04.01.01"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.05.04.01.01]
		</SPAN>
		   <SPAN>
			 The Prelude
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861832" TARGET="almagest" NAME="02.05.04.01.02"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.05.04.01.02]
		</SPAN>
		   <SPAN>
			 Rustam goes to Kabul on behalf of his Brother Shaghad
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861834" TARGET="almagest" NAME="02.05.04.01.03"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.05.04.01.03]
		</SPAN>
		   <SPAN>
			 the King of Kabul digs Pits in the Hunting-ground and how Rustam and Zavarah fall therein
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861836" TARGET="almagest" NAME="02.05.04.01.04"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.05.04.01.04]
		</SPAN>
		   <SPAN>
			 Rustam kills Shaghad and dies
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792822,'57G-391-1','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792822&t=" ALT="Rustam slays Shaghad then dies [57G-391-1]" HEIGHT="100"></A>
				</TD>
				
				 <TD>
					<A HREF="javascript:viewImage(792934,'ILKH-155-1','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792934&t=" ALT="Rustam slays Shaghad then dies [ILKH-155-1]" HEIGHT="100"></A>
				</TD>
				
				 <TD>
					<A HREF="javascript:viewImage(792752,'PECK-273-1','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792752&t=" ALT="Rustam slays Shaghad then dies [PECK-273-1]" HEIGHT="100"></A>
				</TD>
				
				 <TD>
					<A HREF="javascript:viewImage(792896,'58G-329-2','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792896&t=" ALT="Rustam slays Shaghad then dies [58G-329-2]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=868648" TARGET="almagest">57G-391-1</A>
				</TD>
				
				 <TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=869387" TARGET="almagest">ILKH-155-1</A>
				</TD>
				
				 <TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=869403" TARGET="almagest">PECK-273-1</A>
				</TD>
				
				 <TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=869367" TARGET="almagest">58G-329-2</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861839" TARGET="almagest" NAME="02.05.04.01.05"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.05.04.01.05]
		</SPAN>
		   <SPAN>
			 Zal receives News of the Slaying of Rustam and Zavarah, and how Faramarz brings their Coffins and sets them in the Charnel-house
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792935,'ILKH-155-2','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792935&t=" ALT="Rustam funeral [ILKH-155-2]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=869208" TARGET="almagest">ILKH-155-2</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861843" TARGET="almagest" NAME="02.05.04.01.06"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.05.04.01.06]
		</SPAN>
		   <SPAN>
			 Faramarz leads an Army to avenge Rustam and kills the King of Kabul
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792936,'ILKH-156-1','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792936&t=" ALT="Faramarz leads army against Kabol [ILKH-156-1]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=869213" TARGET="almagest">ILKH-156-1</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861847" TARGET="almagest" NAME="02.05.04.01.07"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.05.04.01.07]
		</SPAN>
		   <SPAN>
			 Rudabah loses her Wits through mourning for Rustam
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861850" TARGET="almagest" NAME="02.05.04.01.08"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.05.04.01.08]
		</SPAN>
		   <SPAN>
			 Gushtasp gives the Kingdom to Bahman and dies
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR>
	<TD COLSPAN="2" STYLE="font-weight:bold;color:white;background-color:gray;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861853" STYLE="color:white;" TARGET="almagest">2.6 BAHMAN, SON OF ISFANDIYAR</A>
	</TD>
</TR>

<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861855" TARGET="almagest" NAME="02.06.01.01.01"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.06.01.01.01]
		</SPAN>
		   <SPAN>
			 Bahman seeks Revenge for the Death of Isfandiyar
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861857" TARGET="almagest" NAME="02.06.01.01.02"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.06.01.01.02]
		</SPAN>
		   <SPAN>
			 Bahman puts Zal in Bonds
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861859" TARGET="almagest" NAME="02.06.01.01.03"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.06.01.01.03]
		</SPAN>
		   <SPAN>
			 Faramarz fights with Bahman and is put to Death
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792797,'56G-408-2','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792797&t=" ALT="Bahman Has Faramarz Shot Full of Arrows in Kabul [56G-408-2]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=868790" TARGET="almagest">56G-408-2</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861863" TARGET="almagest" NAME="02.06.01.01.04"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.06.01.01.04]
		</SPAN>
		   <SPAN>
			 Bahman releases Zal and returns to Iran
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861867" TARGET="almagest" NAME="02.06.01.01.05"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.06.01.01.05]
		</SPAN>
		   <SPAN>
			 Bahman marries his own daughter Humay and appoints his Successor
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR>
	<TD COLSPAN="2" STYLE="font-weight:bold;color:white;background-color:gray;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861869" STYLE="color:white;" TARGET="almagest">2.7 HUMAY</A>
	</TD>
</TR>

<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861871" TARGET="almagest" NAME="02.07.01.01.01"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.07.01.01.01]
		</SPAN>
		   <SPAN>
			 Humay casts away her son Darab on the Furat in an Ark
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861875" TARGET="almagest" NAME="02.07.01.01.02"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.07.01.01.02]
		</SPAN>
		   <SPAN>
			 the Launderer brings up Darab
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861877" TARGET="almagest" NAME="02.07.01.01.03"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.07.01.01.03]
		</SPAN>
		   <SPAN>
			 Darab questions the Launderer's Wife about his Parentage, and fights against the Rumans
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792937,'ILKH-158-2','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792937&t=" ALT="Rashnavad finds Darab asleep by castle ruins [ILKH-158-2]" HEIGHT="100"></A>
				</TD>
				
				 <TD>
					<A HREF="javascript:viewImage(792938,'ILKH-159-1','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792938&t=" ALT="Darab, Rashnavad battle Rumis [ILKH-159-1]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=869218" TARGET="almagest">ILKH-158-2</A>
				</TD>
				
				 <TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=869223" TARGET="almagest">ILKH-159-1</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861883" TARGET="almagest" NAME="02.07.01.01.04"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.07.01.01.04]
		</SPAN>
		   <SPAN>
			 Rashnavad learns the Case of Darab
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861885" TARGET="almagest" NAME="02.07.01.01.05"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.07.01.01.05]
		</SPAN>
		   <SPAN>
			 Darab fights against the Host of Rum
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861887" TARGET="almagest" NAME="02.07.01.01.06"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.07.01.01.06]
		</SPAN>
		   <SPAN>
			 Humay recognises her Son
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861889" TARGET="almagest" NAME="02.07.01.01.07"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.07.01.01.07]
		</SPAN>
		   <SPAN>
			 Humay seats Darab upon the Throne
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792753,'PECK-278-2','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792753&t=" ALT="Darab receives crown [PECK-278-2]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=868444" TARGET="almagest">PECK-278-2</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR>
	<TD COLSPAN="2" STYLE="font-weight:bold;color:white;background-color:gray;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861892" STYLE="color:white;" TARGET="almagest">2.8 DARAB</A>
	</TD>
</TR>

<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861894" TARGET="almagest" NAME="02.08.01.01.01"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.08.01.01.01]
		</SPAN>
		   <SPAN>
			 Darab builds the City of Darabgird
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861896" TARGET="almagest" NAME="02.08.01.01.02"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.08.01.01.02]
		</SPAN>
		   <SPAN>
			 Darab defeats the Host of Shu'ayb
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861898" TARGET="almagest" NAME="02.08.01.01.03"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.08.01.01.03]
		</SPAN>
		   <SPAN>
			 Darab fights with Filqus and takes to Wife his Daughter
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861900" TARGET="almagest" NAME="02.08.01.01.04"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.08.01.01.04]
		</SPAN>
		   <SPAN>
			 Darab sends back the Daughter of Filqus to Rum, and how Sikandar is born
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR>
	<TD COLSPAN="2" STYLE="font-weight:bold;color:white;background-color:gray;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861902" STYLE="color:white;" TARGET="almagest">2.9 DARA</A>
	</TD>
</TR>

<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861904" TARGET="almagest" NAME="02.09.01.01.01"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.09.01.01.01]
		</SPAN>
		   <SPAN>
			 Dara harangs the Chiefs and takes Order for the Realm
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861906" TARGET="almagest" NAME="02.09.01.01.02"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.09.01.01.02]
		</SPAN>
		   <SPAN>
			 The Death of Filqus and Sikandar's Accession to the Throne
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861909" TARGET="almagest" NAME="02.09.01.01.03"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.09.01.01.03]
		</SPAN>
		   <SPAN>
			 Sikandar goes as his own Ambassador to Dara
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861912" TARGET="almagest" NAME="02.09.01.01.04"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.09.01.01.04]
		</SPAN>
		   <SPAN>
			 Dara fights with Sikandar and is worsted
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861914" TARGET="almagest" NAME="02.09.01.01.05"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.09.01.01.05]
		</SPAN>
		   <SPAN>
			 Dara fights with Sikandar the second Time
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861916" TARGET="almagest" NAME="02.09.01.01.06"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.09.01.01.06]
		</SPAN>
		   <SPAN>
			 Sikandar fights with Dara the third Time, and how Dara flees to Kirman
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861919" TARGET="almagest" NAME="02.09.01.01.07"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.09.01.01.07]
		</SPAN>
		   <SPAN>
			 Dara writes to Sikandar to propose Peace
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861921" TARGET="almagest" NAME="02.09.01.01.08"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.09.01.01.08]
		</SPAN>
		   <SPAN>
			 Dara is killed by his Ministers
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861923" TARGET="almagest" NAME="02.09.01.01.09"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.09.01.01.09]
		</SPAN>
		   <SPAN>
			 Dara tells his last Wishes to Sikandar and dies
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(795106,'58G-341-2','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=795106&t=" ALT="Sikandar attends dying Dara [58G-341-2]" HEIGHT="100"></A>
				</TD>
				
				 <TD>
					<A HREF="javascript:viewImage(792823,'57G-403-2','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792823&t=" ALT="Sikandar attends dying Dara [57G-403-2]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=869111" TARGET="almagest">58G-341-2</A>
				</TD>
				
				 <TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=868653" TARGET="almagest">57G-403-2</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861926" TARGET="almagest" NAME="02.09.01.01.10"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.09.01.01.10]
		</SPAN>
		   <SPAN>
			 Sikandar writes to the Nobles of Iran
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR>
	<TD COLSPAN="2" STYLE="font-weight:bold;color:white;background-color:gray;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861929" STYLE="color:white;" TARGET="almagest">2.10 SIKANDAR</A>
	</TD>
</TR>

<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861931" TARGET="almagest" NAME="02.10.01.01.01"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.10.01.01.01]
		</SPAN>
		   <SPAN>
			 Sikandar sits upon the Throne of Iran
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792939,'ILKH-164-1','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792939&t=" ALT="Sikandar enthroned [ILKH-164-1]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=869390" TARGET="almagest">ILKH-164-1</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861934" TARGET="almagest" NAME="02.10.01.01.02"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.10.01.01.02]
		</SPAN>
		   <SPAN>
			 Sikandar writes to Dilaray and Rawshanak, the Wife and Daughter of Dara, touching the Nuptials of Rawshanak
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861937" TARGET="almagest" NAME="02.10.01.01.03"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.10.01.01.03]
		</SPAN>
		   <SPAN>
			 Dilaray answers the Letter of Sikandar
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861939" TARGET="almagest" NAME="02.10.01.01.04"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.10.01.01.04]
		</SPAN>
		   <SPAN>
			 Sikandar sends his Mother, Nahid, to fetch Rawshanak, and how he marries her
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861942" TARGET="almagest" NAME="02.10.01.01.05"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.10.01.01.05]
		</SPAN>
		   <SPAN>
			 Kayd has a Dream, and how Mihran interprets it
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792940,'ILKH-165-1','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792940&t=" ALT="Kayd tells Mihran of dream [ILKH-165-1]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=869228" TARGET="almagest">ILKH-165-1</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861946" TARGET="almagest" NAME="02.10.01.01.06"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.10.01.01.06]
		</SPAN>
		   <SPAN>
			 Sikandar marches against Kayd and writes a Letter to him
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861949" TARGET="almagest" NAME="02.10.01.01.07"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.10.01.01.07]
		</SPAN>
		   <SPAN>
			 Kayd answers Sikandar's Letter and announces the Sending of the Fur Wonders
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861951" TARGET="almagest" NAME="02.10.01.01.08"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.10.01.01.08]
		</SPAN>
		   <SPAN>
			 Sikandar sends back the Messenger to receive the Fur Wonders
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861953" TARGET="almagest" NAME="02.10.01.01.09"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.10.01.01.09]
		</SPAN>
		   <SPAN>
			 Sikandar sends Ten Sages with a Letter to inspect the Fur Wonders of Kayd
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861955" TARGET="almagest" NAME="02.10.01.01.10"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.10.01.01.10]
		</SPAN>
		   <SPAN>
			 the Ten Sages bring the Daughter, the Cup, the Leech, and the Sage, from Kayd to Sikandar
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861958" TARGET="almagest" NAME="02.10.01.01.11"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.10.01.01.11]
		</SPAN>
		   <SPAN>
			 Sikandar tests the Sage, the Leech, and the Cup sent by Kayd
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861960" TARGET="almagest" NAME="02.10.01.01.12"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.10.01.01.12]
		</SPAN>
		   <SPAN>
			 Sikandar leads a Host against Fur of Hind and writes a Letter to him
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861962" TARGET="almagest" NAME="02.10.01.01.13"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.10.01.01.13]
		</SPAN>
		   <SPAN>
			 Fur answers the Letter of Sikandar
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861964" TARGET="almagest" NAME="02.10.01.01.14"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.10.01.01.14]
		</SPAN>
		   <SPAN>
			 Sikandar arrays his Host to fight with Fur of Hind and makes iron Steeds and Riders filled with Naphtha
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861967" TARGET="almagest" NAME="02.10.01.01.15"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.10.01.01.15]
		</SPAN>
		   <SPAN>
			 the Host of Sikandar fights with the Host of Fur, how Fur is killed by Sikandar, and how Sikandar seats Savurg upon the Throne of Fur
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792942,'ILKH-168-1','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792942&t=" ALT="Fur dies in battle against Sikandar [ILKH-168-1]" HEIGHT="100"></A>
				</TD>
				
				 <TD>
					<A HREF="javascript:viewImage(792941,'ILKH-167-2','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792941&t=" ALT="Sikandar army versus that of Fur [ILKH-167-2]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=869238" TARGET="almagest">ILKH-168-1</A>
				</TD>
				
				 <TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=869233" TARGET="almagest">ILKH-167-2</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861971" TARGET="almagest" NAME="02.10.01.01.16"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.10.01.01.16]
		</SPAN>
		   <SPAN>
			 Sikandar goes on a Pilgrimage to the House of the Ka'bah
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(795104,'58G-350-2','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=795104&t=" ALT="Sikandar goes on pilgrimage to Ka'bah [58G-350-2]" HEIGHT="100"></A>
				</TD>
				
				 <TD>
					<A HREF="javascript:viewImage(792754,'PECK-291-1','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792754&t=" ALT="Sikandar goes on pilgrimage to Ka'bah [PECK-291-1]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=869015" TARGET="almagest">58G-350-2</A>
				</TD>
				
				 <TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=868449" TARGET="almagest">PECK-291-1</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861974" TARGET="almagest" NAME="02.10.01.01.17"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.10.01.01.17]
		</SPAN>
		   <SPAN>
			 Sikandar leads his troops from Juddah  toward Misr
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861976" TARGET="almagest" NAME="02.10.01.01.18"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.10.01.01.18]
		</SPAN>
		   <SPAN>
			 Sikandar's Letter to Qaydafah, Queen of Andalus, and her Answer
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861979" TARGET="almagest" NAME="02.10.01.01.19"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.10.01.01.19]
		</SPAN>
		   <SPAN>
			 Sikandar leads his Troops to Andalus is and takes the Hold of King Faryan
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861983" TARGET="almagest" NAME="02.10.01.01.20"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.10.01.01.20]
		</SPAN>
		   <SPAN>
			 Sikandar goes as an Ambassador to Qaydafah and is recognised by her
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792824,'57G-414-1','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792824&t=" ALT="Sikandar before Qaydafah who holds portrait [57G-414-1]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=868658" TARGET="almagest">57G-414-1</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861987" TARGET="almagest" NAME="02.10.01.01.21"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.10.01.01.21]
		</SPAN>
		   <SPAN>
			 Qaydafah counsels Sikandar
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861989" TARGET="almagest" NAME="02.10.01.01.22"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.10.01.01.22]
		</SPAN>
		   <SPAN>
			 Tinush, the Son of Qaydafah, is wroth with Sikandar, and how Sikandar takes Precaution against him
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861992" TARGET="almagest" NAME="02.10.01.01.23"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.10.01.01.23]
		</SPAN>
		   <SPAN>
			 Sikandar makes a Compact with Qaydafah and returns to his Troops
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861994" TARGET="almagest" NAME="02.10.01.01.24"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.10.01.01.24]
		</SPAN>
		   <SPAN>
			 Sikandar goes to the Country of the Brahmans, inquires into their Mysteries, and receives an Answer
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792943,'ILKH-171-1','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792943&t=" ALT="Sikandar goes to land of Brahmans [ILKH-171-1]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=869243" TARGET="almagest">ILKH-171-1</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=861997" TARGET="almagest" NAME="02.10.01.01.25"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.10.01.01.25]
		</SPAN>
		   <SPAN>
			 Sikandar comes to the Western Sea and sees Wonders
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862000" TARGET="almagest" NAME="02.10.01.01.26"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.10.01.01.26]
		</SPAN>
		   <SPAN>
			 Sikandar reaches the Land of Habash, fights, and is Victorious
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792944,'ILKH-172-1','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792944&t=" ALT="Sikandar slays Habash monster [ILKH-172-1]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=869248" TARGET="almagest">ILKH-172-1</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862003" TARGET="almagest" NAME="02.10.01.01.27"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.10.01.01.27]
		</SPAN>
		   <SPAN>
			 Sikandar reaches the Land of the Narmpay, how he fights and is victorious, how he kills a Dragon, ascends a Mountain, and is forewarned of his own Death
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792945,'ILKH-172-2','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792945&t=" ALT="Sikandar kills dragon [ILKH-172-2]" HEIGHT="100"></A>
				</TD>
				
				 <TD>
					<A HREF="javascript:viewImage(792755,'PECK-296-2','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792755&t=" ALT="Sikandar kills dragon [PECK-296-2]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=869395" TARGET="almagest">ILKH-172-2</A>
				</TD>
				
				 <TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=868454" TARGET="almagest">PECK-296-2</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862006" TARGET="almagest" NAME="02.10.01.01.28"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.10.01.01.28]
		</SPAN>
		   <SPAN>
			 Sikandar reaches the City of Women, named Harum, and sees Wonders there
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862008" TARGET="almagest" NAME="02.10.01.01.29"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.10.01.01.29]
		</SPAN>
		   <SPAN>
			 Sikandar goes into the Gloom to seek the Water of Life and speaks with Birds and Israfil
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792946,'ILKH-174-1','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792946&t=" ALT="Sikandar encounters Israfil [ILKH-174-1]" HEIGHT="100"></A>
				</TD>
				
				 <TD>
					<A HREF="javascript:viewImage(795101,'57G-420-2','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=795101&t=" ALT="Sikandar encounters Israfil [57G-420-2]" HEIGHT="100"></A>
				</TD>
				
				 <TD>
					<A HREF="javascript:viewImage(792947,'ILKH-174-2','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792947&t=" ALT="Sikandar, Khidhr seek fountain of life [ILKH-174-2]" HEIGHT="100"></A>
				</TD>
				
				 <TD>
					<A HREF="javascript:viewImage(792825,'57G-419-2','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792825&t=" ALT="Sikandar, Khidhr seek fountain of life [57G-419-2]" HEIGHT="100"></A>
				</TD>
				
				 <TD>
					<A HREF="javascript:viewImage(795103,'58G-358-2','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=795103&t=" ALT="Sikandar encounters Israfil [58G-358-2]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=869253" TARGET="almagest">ILKH-174-1</A>
				</TD>
				
				 <TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=868937" TARGET="almagest">57G-420-2</A>
				</TD>
				
				 <TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=869256" TARGET="almagest">ILKH-174-2</A>
				</TD>
				
				 <TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=868663" TARGET="almagest">57G-419-2</A>
				</TD>
				
				 <TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=868952" TARGET="almagest">58G-358-2</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862013" TARGET="almagest" NAME="02.10.01.01.30"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.10.01.01.30]
		</SPAN>
		   <SPAN>
			 Sikandar goes to the East, sees Wonders, and builds a Barrier against Yajuj and Majuj
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792948,'ILKH-175-1','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792948&t=" ALT="Sikandar builds wall against Majuj , Majuj [ILKH-175-1]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=869259" TARGET="almagest">ILKH-175-1</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862016" TARGET="almagest" NAME="02.10.01.01.31"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.10.01.01.31]
		</SPAN>
		   <SPAN>
			 Sikandar sees a Corpse in a Palace of Jewels on the Top of a Mountain, and the Speaking Tree, and how he is warned of his Death
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792949,'ILKH-175-2','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792949&t=" ALT="Sikandar in land of talking trees [ILKH-175-2]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=869264" TARGET="almagest">ILKH-175-2</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862019" TARGET="almagest" NAME="02.10.01.01.32"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.10.01.01.32]
		</SPAN>
		   <SPAN>
			 Sikandar marches his Army to Chin, carries his own Letter to Faghfur, and returns to his Army with the Answer
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862021" TARGET="almagest" NAME="02.10.01.01.33"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.10.01.01.33]
		</SPAN>
		   <SPAN>
			 Sikandar returns from Chin, makes war against the Sindians, and goes to Yaman
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862024" TARGET="almagest" NAME="02.10.01.01.34"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.10.01.01.34]
		</SPAN>
		   <SPAN>
			 Sikandar marches toward Babil and finds the Treasure of Kay Khusraw in a City
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862027" TARGET="almagest" NAME="02.10.01.01.35"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.10.01.01.35]
		</SPAN>
		   <SPAN>
			 Sikandar goes to the City of Babil, writes a Letter to Arastalis, and receives his Answer
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862030" TARGET="almagest" NAME="02.10.01.01.36"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.10.01.01.36]
		</SPAN>
		   <SPAN>
			 Sikandar's Letter to his Mother
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862032" TARGET="almagest" NAME="02.10.01.01.37"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.10.01.01.37]
		</SPAN>
		   <SPAN>
			 Sikandar's Life ends and how they carry his Coffin to Iskandariya
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862035" TARGET="almagest" NAME="02.10.01.01.38"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.10.01.01.38]
		</SPAN>
		   <SPAN>
			 the Sages and other Folk lament Sikandar
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862037" TARGET="almagest" NAME="02.10.01.01.39"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.10.01.01.39]
		</SPAN>
		   <SPAN>
			 the Mother and Wife of Sikandar lament him
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792950,'ILKH-178-1','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792950&t=" ALT="Sikandar mourned [ILKH-178-1]" HEIGHT="100"></A>
				</TD>
				
				 <TD>
					<A HREF="javascript:viewImage(795105,'58G-363-2','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=795105&t=" ALT="Sikandar mourned [58G-363-2]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=869269" TARGET="almagest">ILKH-178-1</A>
				</TD>
				
				 <TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=869092" TARGET="almagest">58G-363-2</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862040" TARGET="almagest" NAME="02.10.01.01.40"> <SPAN STYLE="color:green;font-size:9pt;">
			[02.10.01.01.40]
		</SPAN>
		   <SPAN>
			 Firdawsi's Complaint of the Sky and Appeal to God
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR>
	<TD COLSPAN="2" STYLE="font-weight:bold;color:white;background-color:black;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862042" STYLE="color:white;" TARGET="almagest">3  ASHKANIAN</A>
	</TD>
</TR>

<TR>
	<TD COLSPAN="2" STYLE="font-weight:bold;color:white;background-color:gray;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862044" STYLE="color:white;" TARGET="almagest">3.1 TRIBAL KINGS</A>
	</TD>
</TR>

<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862046" TARGET="almagest" NAME="03.01.01.01.01"> <SPAN STYLE="color:green;font-size:9pt;">
			[03.01.01.01.01]
		</SPAN>
		   <SPAN>
			 Ashkaniyan
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR>
	<TD COLSPAN="2" STYLE="font-weight:bold;color:white;background-color:gray;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862048" STYLE="color:white;" TARGET="almagest">3.2 BABAK</A>
	</TD>
</TR>

<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862050" TARGET="almagest" NAME="03.02.01.01.01"> <SPAN STYLE="color:green;font-size:9pt;">
			[03.02.01.01.01]
		</SPAN>
		   <SPAN>
			 Babak sees Sasan in a Dream and gives him a Daughter in Marriage
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR>
	<TD COLSPAN="2" STYLE="font-weight:bold;color:white;background-color:gray;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862053" STYLE="color:white;" TARGET="almagest">3.3  ARDISHIR</A>
	</TD>
</TR>

<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862055" TARGET="almagest" NAME="03.03.01.01.01"> <SPAN STYLE="color:green;font-size:9pt;">
			[03.03.01.01.01]
		</SPAN>
		   <SPAN>
			 Ardishir is born, and of his Case with Ardavan
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862058" TARGET="almagest" NAME="03.03.01.01.02"> <SPAN STYLE="color:green;font-size:9pt;">
			[03.03.01.01.02]
		</SPAN>
		   <SPAN>
			 Ardavan's Slave-girl falls in Love with Ardshir and how he flees with her to Pars
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792756,'PECK-305-1','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792756&t=" ALT="Ardishir arrives at the court of Ardavan [PECK-305-1]" HEIGHT="100"></A>
				</TD>
				
				 <TD>
					<A HREF="javascript:viewImage(792951,'ILKH-180-1','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792951&t=" ALT="Ardishir meets Gulnar [ILKH-180-1]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=868459" TARGET="almagest">PECK-305-1</A>
				</TD>
				
				 <TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=869398" TARGET="almagest">ILKH-180-1</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862064" TARGET="almagest" NAME="03.03.01.01.03"> <SPAN STYLE="color:green;font-size:9pt;">
			[03.03.01.01.03]
		</SPAN>
		   <SPAN>
			 Ardavan hears of the Flight of Ardshir with the Damsel and pursues them
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862066" TARGET="almagest" NAME="03.03.01.01.04"> <SPAN STYLE="color:green;font-size:9pt;">
			[03.03.01.01.04]
		</SPAN>
		   <SPAN>
			 Ardavan writes to Bahman his Son, to take Ardshir
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862068" TARGET="almagest" NAME="03.03.01.01.05"> <SPAN STYLE="color:green;font-size:9pt;">
			[03.03.01.01.05]
		</SPAN>
		   <SPAN>
			 Tabak helps Ardshir, fights with Bahman, and conquers him
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862070" TARGET="almagest" NAME="03.03.01.01.06"> <SPAN STYLE="color:green;font-size:9pt;">
			[03.03.01.01.06]
		</SPAN>
		   <SPAN>
			 Ardavan leads forth his Host for Battle with Ardshir and is killed
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792952,'ILKH-181-1','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792952&t=" ALT="Ardishir, Ardavan battle [ILKH-181-1]" HEIGHT="100"></A>
				</TD>
				
				 <TD>
					<A HREF="javascript:viewImage(792953,'ILKH-181-2','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792953&t=" ALT="Ardavan captive before Ardishir [ILKH-181-2]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=869272" TARGET="almagest">ILKH-181-1</A>
				</TD>
				
				 <TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=869277" TARGET="almagest">ILKH-181-2</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862075" TARGET="almagest" NAME="03.03.01.01.07"> <SPAN STYLE="color:green;font-size:9pt;">
			[03.03.01.01.07]
		</SPAN>
		   <SPAN>
			 Ardshir fights with the Kurds and is defeated
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862078" TARGET="almagest" NAME="03.03.01.01.08"> <SPAN STYLE="color:green;font-size:9pt;">
			[03.03.01.01.08]
		</SPAN>
		   <SPAN>
			 Ardshir attacks the Kurds by Night and overthrows them
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862080" TARGET="almagest" NAME="03.03.01.01.09"> <SPAN STYLE="color:green;font-size:9pt;">
			[03.03.01.01.09]
		</SPAN>
		   <SPAN>
			 The Story of Haftvad and the Case of the Worm
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862083" TARGET="almagest" NAME="03.03.01.01.10"> <SPAN STYLE="color:green;font-size:9pt;">
			[03.03.01.01.10]
		</SPAN>
		   <SPAN>
			 Ardshir fights with Haftvad and is worsted
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862085" TARGET="almagest" NAME="03.03.01.01.11"> <SPAN STYLE="color:green;font-size:9pt;">
			[03.03.01.01.11]
		</SPAN>
		   <SPAN>
			 Mihrak of Jahrum sacks the Palace of Ardshir
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862087" TARGET="almagest" NAME="03.03.01.01.12"> <SPAN STYLE="color:green;font-size:9pt;">
			[03.03.01.01.12]
		</SPAN>
		   <SPAN>
			 Ardshir hears about the Worm and makes Shift to kill it
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862090" TARGET="almagest" NAME="03.03.01.01.13"> <SPAN STYLE="color:green;font-size:9pt;">
			[03.03.01.01.13]
		</SPAN>
		   <SPAN>
			 Ardshir kills Haftvad
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR>
	<TD COLSPAN="2" STYLE="font-weight:bold;color:white;background-color:black;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862094" STYLE="color:white;" TARGET="almagest">4  SASANIAN</A>
	</TD>
</TR>

<TR>
	<TD COLSPAN="2" STYLE="font-weight:bold;color:white;background-color:gray;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862053" STYLE="color:white;" TARGET="almagest">4.1  ARDISHIR</A>
	</TD>
</TR>

<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862097" TARGET="almagest" NAME="04.01.01.01.01"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.01.01.01.01]
		</SPAN>
		   <SPAN>
			 Ardishir sits upon the Throne in Baghdad
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862100" TARGET="almagest" NAME="04.01.01.01.02"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.01.01.01.02]
		</SPAN>
		   <SPAN>
			 The Case of Ardshir and the Daughter of Ardavan
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792954,'ILKH-184-1','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792954&t=" ALT="Gulnar tries to poison her husband Ardishir [ILKH-184-1]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=869282" TARGET="almagest">ILKH-184-1</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862103" TARGET="almagest" NAME="04.01.01.01.03"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.01.01.01.03]
		</SPAN>
		   <SPAN>
			 Shapur is born to Ardshir by the Daughter of Ardavan, and how after seven Years Ardshir hears of his Son and acknowledges him
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862105" TARGET="almagest" NAME="04.01.01.01.04"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.01.01.01.04]
		</SPAN>
		   <SPAN>
			 Ardshir, to find out the Future of his Reign, sends to Kayd, and Kaid's Reply
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862107" TARGET="almagest" NAME="04.01.01.01.05"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.01.01.01.05]
		</SPAN>
		   <SPAN>
			 The Adventure of Shapur with the Daughter of Mihrak, and his taking her to Wife
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862109" TARGET="almagest" NAME="04.01.01.01.06"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.01.01.01.06]
		</SPAN>
		   <SPAN>
			 Urmazd  is born to Shapur by the Daughter of Mihrak
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862111" TARGET="almagest" NAME="04.01.01.01.07"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.01.01.01.07]
		</SPAN>
		   <SPAN>
			 Of the Wisdom of Ardshir and his Method of administering the Realm
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792955,'ILKH-184-2','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792955&t=" ALT="Ardishir minister stands before him [ILKH-184-2]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=869287" TARGET="almagest">ILKH-184-2</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862114" TARGET="almagest" NAME="04.01.01.01.08"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.01.01.01.08]
		</SPAN>
		   <SPAN>
			 Kharrad praises Ardshir
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862116" TARGET="almagest" NAME="04.01.01.01.09"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.01.01.01.09]
		</SPAN>
		   <SPAN>
			 On the Faithlessness of Fortune
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792757,'PECK-312-1','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792757&t=" ALT="Ardishir recognizes Shapur during polo game [PECK-312-1]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=869406" TARGET="almagest">PECK-312-1</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862121" TARGET="almagest" NAME="04.01.01.01.10"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.01.01.01.10]
		</SPAN>
		   <SPAN>
			 Ardshir charges Shapur and dies
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862124" TARGET="almagest" NAME="04.01.01.01.11"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.01.01.01.11]
		</SPAN>
		   <SPAN>
			 Thanksgiving to the Maker and Praise of Mahmud, the great King
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR>
	<TD COLSPAN="2" STYLE="font-weight:bold;color:white;background-color:gray;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862126" STYLE="color:white;" TARGET="almagest">4.2 SHAPUR</A>
	</TD>
</TR>

<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862128" TARGET="almagest" NAME="04.02.01.01.01"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.02.01.01.01]
		</SPAN>
		   <SPAN>
			 Shapur sits upon the Throne and delivers a Charge to the Chieftains
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862130" TARGET="almagest" NAME="04.02.01.01.02"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.02.01.01.02]
		</SPAN>
		   <SPAN>
			 Shapur fights with the Rumans, how Bazanush, their General, is taken prisoner, and how C?sar makes Peace with Shapur
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862133" TARGET="almagest" NAME="04.02.01.01.04"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.02.01.01.04]
		</SPAN>
		   <SPAN>
			 Shapur seats Urmazd  upon the Throne and dies
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR>
	<TD COLSPAN="2" STYLE="font-weight:bold;color:white;background-color:gray;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862135" STYLE="color:white;" TARGET="almagest">4.3 URMAZD</A>
	</TD>
</TR>

<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862137" TARGET="almagest" NAME="04.03.01.01.01"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.03.01.01.01]
		</SPAN>
		   <SPAN>
			 Urmazd  addresses the Assembly
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862139" TARGET="almagest" NAME="04.03.01.01.02"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.03.01.01.02]
		</SPAN>
		   <SPAN>
			 Urmazd  gives up the Throne to Bahram, charges him, and dies
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR>
	<TD COLSPAN="2" STYLE="font-weight:bold;color:white;background-color:gray;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862141" STYLE="color:white;" TARGET="almagest">4.4 BAHRAM, SON OF URMAZD</A>
	</TD>
</TR>

<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862143" TARGET="almagest" NAME="04.04.01.01.01"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.04.01.01.01]
		</SPAN>
		   <SPAN>
			 Bahram succeedes to the Throne, charges the Nobles, and dies
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR>
	<TD COLSPAN="2" STYLE="font-weight:bold;color:white;background-color:gray;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862145" STYLE="color:white;" TARGET="almagest">4.5 BAHRAM, SON OF BAHRAM</A>
	</TD>
</TR>

<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862147" TARGET="almagest" NAME="04.05.01.01.01"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.05.01.01.01]
		</SPAN>
		   <SPAN>
			 Bahram, Son of Bahram, ascendes the Throne, charges the Nobles, and dies
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR>
	<TD COLSPAN="2" STYLE="font-weight:bold;color:white;background-color:gray;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862149" STYLE="color:white;" TARGET="almagest">4.6 BAHRAM BAHRAMIYAN</A>
	</TD>
</TR>

<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862151" TARGET="almagest" NAME="04.06.01.01.01"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.06.01.01.01]
		</SPAN>
		   <SPAN>
			 Bahram Bahramiyan succeeded to the Throne and dies Fur Months after
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792956,'ILKH-190-2','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792956&t=" ALT="Bahram Bahramiyan enthroned [ILKH-190-2]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=869292" TARGET="almagest">ILKH-190-2</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR>
	<TD COLSPAN="2" STYLE="font-weight:bold;color:white;background-color:gray;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862154" STYLE="color:white;" TARGET="almagest">4.7 NARMPAY</A>
	</TD>
</TR>

<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862156" TARGET="almagest" NAME="04.07.01.01.01"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.07.01.01.01]
		</SPAN>
		   <SPAN>
			 Narmpay succeeds to the Throne, counsels his Son, and dies
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR>
	<TD COLSPAN="2" STYLE="font-weight:bold;color:white;background-color:gray;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862158" STYLE="color:white;" TARGET="almagest">4.8 URMAZD , SON OF NARMPAY</A>
	</TD>
</TR>

<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862160" TARGET="almagest" NAME="04.08.01.01.01"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.08.01.01.01]
		</SPAN>
		   <SPAN>
			 Urmazd , Son of Narmpay, ascends the Throne, and how his Life ends
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR>
	<TD COLSPAN="2" STYLE="font-weight:bold;color:white;background-color:gray;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862162" STYLE="color:white;" TARGET="almagest">4.9 SHAPUR SURNAMED ZU'L AKTAF</A>
	</TD>
</TR>

<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862164" TARGET="almagest" NAME="04.09.01.01.01"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.09.01.01.01]
		</SPAN>
		   <SPAN>
			 Shapur, Son of Urmazd  is born forty Days after his Father's Death, and how he is crowned
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862167" TARGET="almagest" NAME="04.09.01.01.02"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.09.01.01.02]
		</SPAN>
		   <SPAN>
			 Ta'ir, the Arab, carries off the Daughter of Narmpay and marries her,
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862169" TARGET="almagest" NAME="04.09.01.01.03"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.09.01.01.03]
		</SPAN>
		   <SPAN>
			 how Shapur goes to Yaman to fight him, and how his Daughter falls in Love with Shapur
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862172" TARGET="almagest" NAME="04.09.01.01.04"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.09.01.01.04]
		</SPAN>
		   <SPAN>
			 Ta'ir is bemused by his Daughter and how she comes to Shapur, who takes the Hold and kills Ta'ir
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792758,'PECK-321-2','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792758&t=" ALT="Ta'ir beheaded before Shapur [PECK-321-2]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=868464" TARGET="almagest">PECK-321-2</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862175" TARGET="almagest" NAME="04.09.01.01.05"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.09.01.01.05]
		</SPAN>
		   <SPAN>
			 Shapur goes disguised as a Merchant to Rum, how he is taken by Caesar, and how C?sar ravages the Land of Iran
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862178" TARGET="almagest" NAME="04.09.01.01.06"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.09.01.01.06]
		</SPAN>
		   <SPAN>
			 the Damsel takes Measures to free Shapur, and how he flees with her from Rum
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862180" TARGET="almagest" NAME="04.09.01.01.07"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.09.01.01.07]
		</SPAN>
		   <SPAN>
			 the High Priest and the Captain of the Host, hearing of Shapur's Arrival, goes to him with the Troops
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862182" TARGET="almagest" NAME="04.09.01.01.08"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.09.01.01.08]
		</SPAN>
		   <SPAN>
			 Shapur makes a Night-attack, and how Caesar is taken
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792819,'57G-353-2','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792819&t=" ALT="Shapur cuts off nose, ears of King Rum [57G-353-2]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=868635" TARGET="almagest">57G-353-2</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862187" TARGET="almagest" NAME="04.09.01.01.09"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.09.01.01.09]
		</SPAN>
		   <SPAN>
			 Shapur goes to Rum and fights with Yanus, Caesar's Brother
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862189" TARGET="almagest" NAME="04.09.01.01.10"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.09.01.01.10]
		</SPAN>
		   <SPAN>
			 the Rumans place Bazanush upon the Throne of C?sar; his Letter to Shapur and the Answer
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862191" TARGET="almagest" NAME="04.09.01.01.11"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.09.01.01.11]
		</SPAN>
		   <SPAN>
			 Bazanush goes to Shapur and makes a Treaty of Peace
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862193" TARGET="almagest" NAME="04.09.01.01.12"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.09.01.01.12]
		</SPAN>
		   <SPAN>
			 Mani, the Painter, comes to Shapur with Pretence of being a Prophet, and is killed
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792957,'ILKH-195-2','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792957&t=" ALT="Shapur executes Mani [ILKH-195-2]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=869297" TARGET="almagest">ILKH-195-2</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862196" TARGET="almagest" NAME="04.09.01.01.13"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.09.01.01.13]
		</SPAN>
		   <SPAN>
			 Shapur makes his Brother Ardshir Regent till his own Son should grow up, and how his Days ended
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR>
	<TD COLSPAN="2" STYLE="font-weight:bold;color:white;background-color:gray;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862198" STYLE="color:white;" TARGET="almagest">4.10 ARDSHIR BROTHER OF SHAPUR</A>
	</TD>
</TR>

<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862200" TARGET="almagest" NAME="04.10.01.01.01"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.10.01.01.01]
		</SPAN>
		   <SPAN>
			 Ardshir sits upon the Throne and gives a Charge to the Officers
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR>
	<TD COLSPAN="2" STYLE="font-weight:bold;color:white;background-color:gray;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862202" STYLE="color:white;" TARGET="almagest">4.11 SHAPUR SON OF SHAPUR</A>
	</TD>
</TR>

<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862204" TARGET="almagest" NAME="04.11.01.01.01"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.11.01.01.01]
		</SPAN>
		   <SPAN>
			 Shapur, Son of Shapur, sits upon the Throne and gives a Charge to the Officers
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR>
	<TD COLSPAN="2" STYLE="font-weight:bold;color:white;background-color:gray;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862206" STYLE="color:white;" TARGET="almagest">4.12 BAHRAM SON OF SHAPUR</A>
	</TD>
</TR>

<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862208" TARGET="almagest" NAME="04.12.01.01.01"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.12.01.01.01]
		</SPAN>
		   <SPAN>
			 Bahram sits upon the Throne and gives a Charge to the Officers
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR>
	<TD COLSPAN="2" STYLE="font-weight:bold;color:white;background-color:gray;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862210" STYLE="color:white;" TARGET="almagest">4.13 YAZDIGIRD, SON OF SHAPUR</A>
	</TD>
</TR>

<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862212" TARGET="almagest" NAME="04.13.01.01.01"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.13.01.01.01]
		</SPAN>
		   <SPAN>
			 Yazdigird sits upon the Throne and gives a Charge to the Officers
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862215" TARGET="almagest" NAME="04.13.01.01.02"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.13.01.01.02]
		</SPAN>
		   <SPAN>
			 Bahram Gur, Son of Yazdigird, is born and sends to be brought up by Mumzir, the Arab
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862218" TARGET="almagest" NAME="04.13.01.01.03"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.13.01.01.03]
		</SPAN>
		   <SPAN>
			 Bahram goes to the Chase with a Damsel and how he displays his Accomplishment
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862220" TARGET="almagest" NAME="04.13.01.01.04"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.13.01.01.04]
		</SPAN>
		   <SPAN>
			 Bahram showes his Accomplishment in the Chase before Mundhir
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792759,'PECK-329-2','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792759&t=" ALT="Bahram Gur hunts in company of Azadah [PECK-329-2]" HEIGHT="100"></A>
				</TD>
				
				 <TD>
					<A HREF="javascript:viewImage(792958,'ILKH-197-2','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792958&t=" ALT="Bahram Gur hunts in company of Azadah [ILKH-197-2]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=868469" TARGET="almagest">PECK-329-2</A>
				</TD>
				
				 <TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=869302" TARGET="almagest">ILKH-197-2</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862224" TARGET="almagest" NAME="04.13.01.01.05"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.13.01.01.05]
		</SPAN>
		   <SPAN>
			 Bahram comes with Nu'man to Yazdigird
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862226" TARGET="almagest" NAME="04.13.01.01.06"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.13.01.01.06]
		</SPAN>
		   <SPAN>
			 Yazdigird puts Bahram in Bonds, how he escapes by the good Offices of Tinush, and how he returns to Mundhir
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862228" TARGET="almagest" NAME="04.13.01.01.07"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.13.01.01.07]
		</SPAN>
		   <SPAN>
			 Yazdigird, by the Advice of an Archmage, goes to the Spring of Sav and is killed by a Water-horse
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792820,'57G-360-2','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792820&t=" ALT="Sea monster kills Yazdigird [57G-360-2]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=868640" TARGET="almagest">57G-360-2</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862231" TARGET="almagest" NAME="04.13.01.01.08"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.13.01.01.08]
		</SPAN>
		   <SPAN>
			 the Iranians take Counsel and placed Khusraw upon the Throne
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862233" TARGET="almagest" NAME="04.13.01.01.09"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.13.01.01.09]
		</SPAN>
		   <SPAN>
			 Bahram Gur hears of the Death of his Father and invades Iran
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862235" TARGET="almagest" NAME="04.13.01.01.10"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.13.01.01.10]
		</SPAN>
		   <SPAN>
			 the Iranians hear of Bahram's Pillaging and writes to Mundhir, and how he replies
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862237" TARGET="almagest" NAME="04.13.01.01.11"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.13.01.01.11]
		</SPAN>
		   <SPAN>
			 Bahram Gur arrives at Jahrum with the Host of Mundhir, and how the Iranians go out to him
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862240" TARGET="almagest" NAME="04.13.01.01.12"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.13.01.01.12]
		</SPAN>
		   <SPAN>
			 Bahram Gur harangs Iranians as to his Fitness to rule, how they reject him, but promis him the Kingship if he would take the Crown from between the Lions
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862242" TARGET="almagest" NAME="04.13.01.01.13"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.13.01.01.13]
		</SPAN>
		   <SPAN>
			 Bahram and Khusraw go to the Waste, and how Bahram kills the Lions and takes his Seat upon the Throne
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862245" TARGET="almagest" NAME="04.13.01.01.14"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.13.01.01.14]
		</SPAN>
		   <SPAN>
			 Bahram and Khusraw go to the Waste, and Bahram kills the Lions and takes his Seat upon the Throne
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR>
	<TD COLSPAN="2" STYLE="font-weight:bold;color:white;background-color:gray;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862247" STYLE="color:white;" TARGET="almagest">4.14 BAHRAM GUR</A>
	</TD>
</TR>

<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862249" TARGET="almagest" NAME="04.14.01.01.01"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.14.01.01.01]
		</SPAN>
		   <SPAN>
			 Bahram ascends the Throne, charges the Officers, and writes Letters to all the Chiefs
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862252" TARGET="almagest" NAME="04.14.01.01.02"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.14.01.01.02]
		</SPAN>
		   <SPAN>
			 Bahram pardons the Fault of the Iranians, farewells Mundhir and Nu'man, and remits the Iranians' Arrears of Taxes
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862254" TARGET="almagest" NAME="04.14.01.01.03"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.14.01.01.03]
		</SPAN>
		   <SPAN>
			 Bahram goes to the House of Lambak, the Water-carrier, and becomes his Guest
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862257" TARGET="almagest" NAME="04.14.01.01.04"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.14.01.01.04]
		</SPAN>
		   <SPAN>
			 Bahram goes to the House of Baraham, who treats him scurvily
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862260" TARGET="almagest" NAME="04.14.01.01.05"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.14.01.01.05]
		</SPAN>
		   <SPAN>
			 Bahram bestows the Wealth of Baraham upon Lambak
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862262" TARGET="almagest" NAME="04.14.01.01.06"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.14.01.01.06]
		</SPAN>
		   <SPAN>
			 Bahrarn kills Lions and forbids Winedrinking
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862264" TARGET="almagest" NAME="04.14.01.01.07"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.14.01.01.07]
		</SPAN>
		   <SPAN>
			 Story of the young Shoemaker and how Bahram allows Wine again
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862267" TARGET="almagest" NAME="04.14.01.01.08"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.14.01.01.08]
		</SPAN>
		   <SPAN>
			 Ruzbih, Bahram's High Priest, ruins a Village by a Stratagem and restores it
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862269" TARGET="almagest" NAME="04.14.01.01.09"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.14.01.01.09]
		</SPAN>
		   <SPAN>
			 Bahram marries a Country Miller's Daughter
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862273" TARGET="almagest" NAME="04.14.01.01.10"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.14.01.01.10]
		</SPAN>
		   <SPAN>
			 Bahram finds the Treasures of Jamshid and bestows them upon the Poor
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792959,'ILKH-204-2','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792959&t=" ALT="Bahram Gur finds Jamshid treasures [ILKH-204-2]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=869305" TARGET="almagest">ILKH-204-2</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862276" TARGET="almagest" NAME="04.14.01.01.11"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.14.01.01.11]
		</SPAN>
		   <SPAN>
			 Bahram, returning from Hunting, goes to the House of a Merchant and departs displeased
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862278" TARGET="almagest" NAME="04.14.01.01.12"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.14.01.01.12]
		</SPAN>
		   <SPAN>
			 Bahram kills a Dragon and goes to a Yokel's House
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792849,'59G-387-1','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792849&t=" ALT="Bahram Gur kills dragon which devoured youth [59G-387-1]" HEIGHT="100"></A>
				</TD>
				
				 <TD>
					<A HREF="javascript:viewImage(792961,'ILKH-209-2','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792961&t=" ALT="Bahram Gur kills dragon which devoured youth [ILKH-209-2]" HEIGHT="100"></A>
				</TD>
				
				 <TD>
					<A HREF="javascript:viewImage(792962,'ILKH-210-1','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792962&t=" ALT="Bahram Gur at peasant house [ILKH-210-1]" HEIGHT="100"></A>
				</TD>
				
				 <TD>
					<A HREF="javascript:viewImage(792818,'57G-321-1','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792818&t=" ALT="Bahram Gur kills dragon which devoured youth [57G-321-1]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=868882" TARGET="almagest">59G-387-1</A>
				</TD>
				
				 <TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=869310" TARGET="almagest">ILKH-209-2</A>
				</TD>
				
				 <TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=869313" TARGET="almagest">ILKH-210-1</A>
				</TD>
				
				 <TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=868630" TARGET="almagest">57G-321-1</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862282" TARGET="almagest" NAME="04.14.01.01.13"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.14.01.01.13]
		</SPAN>
		   <SPAN>
			 Bahram goes to the Chase and espouses the Daughters of the Thane Burzin
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862285" TARGET="almagest" NAME="04.14.01.01.14"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.14.01.01.14]
		</SPAN>
		   <SPAN>
			 Bahram kills Lions, goes to the House of a jeweller, and marries his Daughter
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862287" TARGET="almagest" NAME="04.14.01.01.15"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.14.01.01.15]
		</SPAN>
		   <SPAN>
			 Bahram goes to the Chase and passes the Night in the House of Farshidvard
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862289" TARGET="almagest" NAME="04.14.01.01.16"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.14.01.01.16]
		</SPAN>
		   <SPAN>
			 A Bramble-grubber reveals the Case of Farshidvard, and how Bahram bestows that Householder's Wealth upon the Poor
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792875,'58G-392-2','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792875&t=" ALT="Bahram Gur skewers doe, buck while hunting [58G-392-2]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=868978" TARGET="almagest">58G-392-2</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862293" TARGET="almagest" NAME="04.14.01.01.17"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.14.01.01.17]
		</SPAN>
		   <SPAN>
			 Bahram goes to the Chase and kills Lions
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792760,'PECK-336-2','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792760&t=" ALT="Bahram Gur hunts lions [PECK-336-2]" HEIGHT="100"></A>
				</TD>
				
				 <TD>
					<A HREF="javascript:viewImage(792817,'57G-319-1','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792817&t=" ALT="Bahram Gur hunts lions [57G-319-1]" HEIGHT="100"></A>
				</TD>
				
				 <TD>
					<A HREF="javascript:viewImage(792848,'59G-385-1','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792848&t=" ALT="Bahram Gur hunts lions [59G-385-1]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=868474" TARGET="almagest">PECK-336-2</A>
				</TD>
				
				 <TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=868627" TARGET="almagest">57G-319-1</A>
				</TD>
				
				 <TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=868879" TARGET="almagest">59G-385-1</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862296" TARGET="almagest" NAME="04.14.01.01.18"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.14.01.01.18]
		</SPAN>
		   <SPAN>
			 Bahram goes to hunt the Onager, shows his Skill before the Princes, and returns to Baghdad and Istakhr
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792960,'ILKH-208-2','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792960&t=" ALT="Bahram Gur hunts onager [ILKH-208-2]" HEIGHT="100"></A>
				</TD>
				
				 <TD>
					<A HREF="javascript:viewImage(792761,'PECK-344-2','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792761&t=" ALT="Bahram Gur hunts onager [PECK-344-2]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=869318" TARGET="almagest">ILKH-208-2</A>
				</TD>
				
				 <TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=869437" TARGET="almagest">PECK-344-2</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862299" TARGET="almagest" NAME="04.14.01.01.19"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.14.01.01.19]
		</SPAN>
		   <SPAN>
			 Khaqan of China leads forth a Host to war with Bahrmm, and how the Iranians ask Quarter of the Khan and submit to him
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862302" TARGET="almagest" NAME="04.14.01.01.20"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.14.01.01.20]
		</SPAN>
		   <SPAN>
			 Bahram attacks the Host of the Khan and takes him
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792798,'56G-488-2','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792798&t=" ALT="Bahram Gur Takes the Chinese Khaqan Prisoner [56G-488-2]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=868799" TARGET="almagest">56G-488-2</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862306" TARGET="almagest" NAME="04.14.01.01.21"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.14.01.01.21]
		</SPAN>
		   <SPAN>
			 Bahram takes a Pledge from the Turanian, how he sets up a Pillar to delimit the Realm, and places Shabra upon the Throne of Turan
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862308" TARGET="almagest" NAME="04.14.01.01.22"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.14.01.01.22]
		</SPAN>
		   <SPAN>
			 Bahram writes to announce his Victory to his Brother Narmpay and returns to Iran
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792963,'ILKH-212-1','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792963&t=" ALT="Bahram Gur appoints Narmpay, Kurasan viceroy [ILKH-212-1]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=869323" TARGET="almagest">ILKH-212-1</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862311" TARGET="almagest" NAME="04.14.01.01.23"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.14.01.01.23]
		</SPAN>
		   <SPAN>
			 Bahram writes a Letter of Directions to his Officials
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862313" TARGET="almagest" NAME="04.14.01.01.24"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.14.01.01.24]
		</SPAN>
		   <SPAN>
			 Bahram calls before him the Envoy of Coesar, and how the Envoy questions and answers the Mubad
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862315" TARGET="almagest" NAME="04.14.01.01.25"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.14.01.01.25]
		</SPAN>
		   <SPAN>
			 Bahram dismisses Caesar's Envoy and charges his own Officials
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862318" TARGET="almagest" NAME="04.14.01.01.26"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.14.01.01.26]
		</SPAN>
		   <SPAN>
			 Bahram goes with his own Letter to Shangul King of Hind
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862320" TARGET="almagest" NAME="04.14.01.01.27"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.14.01.01.27]
		</SPAN>
		   <SPAN>
			 Shangul receives the Letter from Bahram and makes Reply
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862322" TARGET="almagest" NAME="04.14.01.01.28"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.14.01.01.28]
		</SPAN>
		   <SPAN>
			 Shangul prepared a Feast for Bahram, and Bahram displays his Prowess
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792762,'PECK-352-2','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792762&t=" ALT="Bahram Gur wrestles before Shangul [PECK-352-2]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=868479" TARGET="almagest">PECK-352-2</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862326" TARGET="almagest" NAME="04.14.01.01.29"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.14.01.01.29]
		</SPAN>
		   <SPAN>
			 Shangul suspects Bahrarm and keeps him from Iran
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862328" TARGET="almagest" NAME="04.14.01.01.30"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.14.01.01.30]
		</SPAN>
		   <SPAN>
			 Bahram fights with the Wolf at the Bidding of Shangul and kills it
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792964,'ILKH-214-2','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792964&t=" ALT="Bahram Gur kills unicorn [ILKH-214-2]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=869328" TARGET="almagest">ILKH-214-2</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862331" TARGET="almagest" NAME="04.14.01.01.31"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.14.01.01.31]
		</SPAN>
		   <SPAN>
			 Bahram kills a Dragon
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792849,'59G-387-1','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792849&t=" ALT="Bahram Gur kills dragon which devoured youth [59G-387-1]" HEIGHT="100"></A>
				</TD>
				
				 <TD>
					<A HREF="javascript:viewImage(792961,'ILKH-209-2','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792961&t=" ALT="Bahram Gur kills dragon which devoured youth [ILKH-209-2]" HEIGHT="100"></A>
				</TD>
				
				 <TD>
					<A HREF="javascript:viewImage(792818,'57G-321-1','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792818&t=" ALT="Bahram Gur kills dragon which devoured youth [57G-321-1]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=868882" TARGET="almagest">59G-387-1</A>
				</TD>
				
				 <TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=869310" TARGET="almagest">ILKH-209-2</A>
				</TD>
				
				 <TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=868630" TARGET="almagest">57G-321-1</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862334" TARGET="almagest" NAME="04.14.01.01.32"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.14.01.01.32]
		</SPAN>
		   <SPAN>
			 Shangul becomes troubled about Bahram and gives a Daughter to him
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792887,'58G-397-2','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792887&t=" ALT="Bahram Gur kills monkey-lion before throne of Hind Shapur [58G-397-2]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=869024" TARGET="almagest">58G-397-2</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862338" TARGET="almagest" NAME="04.14.01.01.33"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.14.01.01.33]
		</SPAN>
		   <SPAN>
			 Gang Dizh writes to Bahram and how he replies
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862341" TARGET="almagest" NAME="04.14.01.01.34"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.14.01.01.34]
		</SPAN>
		   <SPAN>
			 Bahram flees from Hindustan to Iran with the Daughter of Shangul
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862344" TARGET="almagest" NAME="04.14.01.01.35"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.14.01.01.35]
		</SPAN>
		   <SPAN>
			 Shangul follows Bahram, learns who he is, and is reconciled to him
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862346" TARGET="almagest" NAME="04.14.01.01.36"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.14.01.01.36]
		</SPAN>
		   <SPAN>
			 Shangul goes back to Hind and Bahram to Iran
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862348" TARGET="almagest" NAME="04.14.01.01.37"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.14.01.01.37]
		</SPAN>
		   <SPAN>
			 Shangul, with seven Kings, visits Bahram
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862351" TARGET="almagest" NAME="04.14.01.01.38"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.14.01.01.38]
		</SPAN>
		   <SPAN>
			 Shangul returns to Hindustan, and Bahram remits the Property-tax to the Land-owners
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862353" TARGET="almagest" NAME="04.14.01.01.39"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.14.01.01.39]
		</SPAN>
		   <SPAN>
			 Bahram summons Gipsies from Hindustan
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862355" TARGET="almagest" NAME="04.14.01.01.40"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.14.01.01.40]
		</SPAN>
		   <SPAN>
			 Time of Bahram comes to an End
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR>
	<TD COLSPAN="2" STYLE="font-weight:bold;color:white;background-color:gray;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862358" STYLE="color:white;" TARGET="almagest">4.15 YAZDIGIRD, SON OF BAHRAM GUR</A>
	</TD>
</TR>

<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862360" TARGET="almagest" NAME="04.15.01.01.01"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.15.01.01.01]
		</SPAN>
		   <SPAN>
			 Yazdigird sits upon the Throne and exhorts the Captains of the Host
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR>
	<TD COLSPAN="2" STYLE="font-weight:bold;color:white;background-color:gray;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862362" STYLE="color:white;" TARGET="almagest">4.16 HURMUZ</A>
	</TD>
</TR>

<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862364" TARGET="almagest" NAME="04.16.01.01.01"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.16.01.01.01]
		</SPAN>
		   <SPAN>
			 Hurmuz, Son of Yazdigird, ascends the Throne
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR>
	<TD COLSPAN="2" STYLE="font-weight:bold;color:white;background-color:gray;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862366" STYLE="color:white;" TARGET="almagest">4.17 PIRUZ</A>
	</TD>
</TR>

<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862368" TARGET="almagest" NAME="04.17.01.01.01"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.17.01.01.01]
		</SPAN>
		   <SPAN>
			 Piruz sits upon the Throne and makes an Oration
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862370" TARGET="almagest" NAME="04.17.01.01.02"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.17.01.01.02]
		</SPAN>
		   <SPAN>
			 Piruz builds the Cities of Piruz-Ram and Badan-Piruz, and how he goes to war with Turan
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862372" TARGET="almagest" NAME="04.17.01.01.03"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.17.01.01.03]
		</SPAN>
		   <SPAN>
			 The Letter of Kushnawaz to Piruz
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862374" TARGET="almagest" NAME="04.17.01.01.04"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.17.01.01.04]
		</SPAN>
		   <SPAN>
			 Piruz fights with Khushnawaz and is killed
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR>
	<TD COLSPAN="2" STYLE="font-weight:bold;color:white;background-color:gray;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862377" STYLE="color:white;" TARGET="almagest">4.18 BALASH</A>
	</TD>
</TR>

<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862379" TARGET="almagest" NAME="04.18.01.01.01"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.18.01.01.01]
		</SPAN>
		   <SPAN>
			 Balash ascends the Throne and harangues the Iranians
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862382" TARGET="almagest" NAME="04.18.01.01.02"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.18.01.01.02]
		</SPAN>
		   <SPAN>
			 Sufaray has Tidings of the Slaying of Piruz, how he writes a Letter to Khushnawaz, and how Khushnawaz replies
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862384" TARGET="almagest" NAME="04.18.01.01.03"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.18.01.01.03]
		</SPAN>
		   <SPAN>
			 Sufaray fights with Khushnawaz, and how Qubad is released from his Bondage
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR>
	<TD COLSPAN="2" STYLE="font-weight:bold;color:white;background-color:gray;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862388" STYLE="color:white;" TARGET="almagest">4.19 QUBAD, SON OF PIRUZ</A>
	</TD>
</TR>

<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862390" TARGET="almagest" NAME="04.19.01.01.01"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.19.01.01.01]
		</SPAN>
		   <SPAN>
			 Qubad sits upon the Throne and makes an Oration to the Iranians
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862392" TARGET="almagest" NAME="04.19.01.01.02"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.19.01.01.02]
		</SPAN>
		   <SPAN>
			 Sufaray goes to Shirdz, how the Iranians slander him to Qubad, and how Qubad kills him
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862395" TARGET="almagest" NAME="04.19.01.01.03"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.19.01.01.03]
		</SPAN>
		   <SPAN>
			 The Iranians put Kubdd in Bonds and commit him to Rizmihr, the Son of Sufaray, and how Jamasp, the Brother of Qubad, is set upon the Throne
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862399" TARGET="almagest" NAME="04.19.01.01.04"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.19.01.01.04]
		</SPAN>
		   <SPAN>
			 Qubad escapes from Ward with Rizmihr, how he weds the Daughter of a Thane, and how he takes Refuge with the Hital
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862402" TARGET="almagest" NAME="04.19.01.01.05"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.19.01.01.05]
		</SPAN>
		   <SPAN>
			 Qubad returns from Haital to Iran, how he has Tidings of the Birth of his Son, Nushirvan, and reascends the Throne
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862404" TARGET="almagest" NAME="04.19.01.01.06"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.19.01.01.06]
		</SPAN>
		   <SPAN>
			 The Story of Qubad and Mazdak, and how Qubad adopts the Faith of Mazdak
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862407" TARGET="almagest" NAME="04.19.01.01.07"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.19.01.01.07]
		</SPAN>
		   <SPAN>
			 Nushirvan rejects the Faith of Mazdak and kills him and his Followers
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862410" TARGET="almagest" NAME="04.19.01.01.08"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.19.01.01.08]
		</SPAN>
		   <SPAN>
			 Qubad nominats Kisra as Successor, and how the Great gives him the name of Nushirvan
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR>
	<TD COLSPAN="2" STYLE="font-weight:bold;color:white;background-color:gray;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862412" STYLE="color:white;" TARGET="almagest">4.20 NUSHIRVAN</A>
	</TD>
</TR>

<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862414" TARGET="almagest" NAME="04.20.01.01.01"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.20.01.01.01]
		</SPAN>
		   <SPAN>
			 The Prelude
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862416" TARGET="almagest" NAME="04.20.01.01.02"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.20.01.01.02]
		</SPAN>
		   <SPAN>
			 Nushirvan ascends the Throne and makes an Oration to the Iranians
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862419" TARGET="almagest" NAME="04.20.01.01.03"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.20.01.01.03]
		</SPAN>
		   <SPAN>
			 Nushirvan divides his Realm into Fur Parts and writes a Decree to his Officers on the Administration of justice
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862421" TARGET="almagest" NAME="04.20.01.01.04"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.20.01.01.04]
		</SPAN>
		   <SPAN>
			 Nushirvan requires Babak to muster the Host
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862424" TARGET="almagest" NAME="04.20.01.01.05"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.20.01.01.05]
		</SPAN>
		   <SPAN>
			 Nushirvan harangues the Iranians and the Kings acknowledge his Supremacy
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862426" TARGET="almagest" NAME="04.20.01.01.06"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.20.01.01.06]
		</SPAN>
		   <SPAN>
			 Nushirvan goes round his Empire and builds a Wall in the Pass between Iran and Turan
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862428" TARGET="almagest" NAME="04.20.01.01.07"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.20.01.01.07]
		</SPAN>
		   <SPAN>
			 Nushirvan chastises the Alans and the Baluchi and Gilan
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792764,'PECK-368-2','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792764&t=" ALT="Nushirvan receives Gilan [PECK-368-2]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=868484" TARGET="almagest">PECK-368-2</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862431" TARGET="almagest" NAME="04.20.01.01.08"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.20.01.01.08]
		</SPAN>
		   <SPAN>
			 Mundhir, the Arab, comes to Nushirvan for Succour against the Injustice of Caesar
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862434" TARGET="almagest" NAME="04.20.01.01.09"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.20.01.01.09]
		</SPAN>
		   <SPAN>
			 The Letter of Shah Nushirvan to Caesar of Rum
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862436" TARGET="almagest" NAME="04.20.01.01.10"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.20.01.01.10]
		</SPAN>
		   <SPAN>
			 The Letter of Nushirvan reaches Caesar and how he replies
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862438" TARGET="almagest" NAME="04.20.01.01.11"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.20.01.01.11]
		</SPAN>
		   <SPAN>
			 Nushirvan goes to war with Caesar
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862440" TARGET="almagest" NAME="04.20.01.01.12"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.20.01.01.12]
		</SPAN>
		   <SPAN>
			 Nushirvan takes divers Strongholds in his March to Rum
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862443" TARGET="almagest" NAME="04.20.01.01.13"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.20.01.01.13]
		</SPAN>
		   <SPAN>
			 The Battle of Nushirvan with Farfuriyus, the Leader of Caesar's Host, the Victory of Nishirwan, and his Capture of Kaliniyus and Antakiyah
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862446" TARGET="almagest" NAME="04.20.01.01.14"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.20.01.01.14]
		</SPAN>
		   <SPAN>
			 Nushirvan builds the City of Zib-i-Khusraw in the Likeness of Antakiyah and settles the Roman Captives therein
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862448" TARGET="almagest" NAME="04.20.01.01.15"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.20.01.01.15]
		</SPAN>
		   <SPAN>
			 Caesar writes to Nushirvan and sends Tribute
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862452" TARGET="almagest" NAME="04.20.01.01.16"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.20.01.01.16]
		</SPAN>
		   <SPAN>
			 The Birth of Nushzad, the Son of Nushirvan, by a Woman who is a Christian
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862454" TARGET="almagest" NAME="04.20.01.01.17"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.20.01.01.17]
		</SPAN>
		   <SPAN>
			 The Sickness of Nushirvan and the Sedition of Nushzad
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862456" TARGET="almagest" NAME="04.20.01.01.18"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.20.01.01.18]
		</SPAN>
		   <SPAN>
			 The Letter of Nushirvan to Ram Burzin, the Warden of the March of Mada'in, respecting the Taking of Nushzad
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862459" TARGET="almagest" NAME="04.20.01.01.19"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.20.01.01.19]
		</SPAN>
		   <SPAN>
			 Ram Burzin fights with Nashzad, and how Nashzad is killed
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862462" TARGET="almagest" NAME="04.20.02.01.01"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.20.02.01.01]
		</SPAN>
		   <SPAN>
			 Nushirvan has a Dream and how Buzurjmihr interprets it
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792900,'58G-445-2','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792900&t=" ALT="Nushirvan finds male youth in harem [58G-445-2]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=869370" TARGET="almagest">58G-445-2</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862467" TARGET="almagest" NAME="04.20.02.01.02"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.20.02.01.02]
		</SPAN>
		   <SPAN>
			 The first Banquet of Nushirvan to the Sages, and the Counsels of Buzurjmihr
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862470" TARGET="almagest" NAME="04.20.02.01.03"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.20.02.01.03]
		</SPAN>
		   <SPAN>
			 The second Banquet of Nushirvan to Buzurjmihr and the Archimages
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862472" TARGET="almagest" NAME="04.20.02.01.04"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.20.02.01.04]
		</SPAN>
		   <SPAN>
			 The third Banquet of Nushirvan to Buzurjmihr and the Archimages
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862474" TARGET="almagest" NAME="04.20.02.01.05"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.20.02.01.05]
		</SPAN>
		   <SPAN>
			 The fourth Banquet of Nashirwan to Buzurjmihr and the Archimages
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862476" TARGET="almagest" NAME="04.20.02.01.06"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.20.02.01.06]
		</SPAN>
		   <SPAN>
			 The fifth Banquet of Nushirvan to Buzurjmihr and the Archimages
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792965,'ILKH-229-2','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792965&t=" ALT="Nushirvan fifth banquet for Buzurjmihr [ILKH-229-2]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=869333" TARGET="almagest">ILKH-229-2</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862479" TARGET="almagest" NAME="04.20.02.01.07"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.20.02.01.07]
		</SPAN>
		   <SPAN>
			 The sixth Banquet of Nushirvan to Buzurjmihr and the Archimages
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862482" TARGET="almagest" NAME="04.20.02.01.08"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.20.02.01.08]
		</SPAN>
		   <SPAN>
			 The seventh Banquet of Nushirvan to Buzurjmihr and the Archimages
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792966,'ILKH-230-2','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792966&t=" ALT="Nushirvan seventh banquet for Buzurjmihr [ILKH-230-2]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=869338" TARGET="almagest">ILKH-230-2</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862485" TARGET="almagest" NAME="04.20.03.01.01"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.20.03.01.01]
		</SPAN>
		   <SPAN>
			 The Story of Mahbud, the Dastur of Nushirvan, and how Mahbud and his Sons are killed by the Sorcery of Zarvan and a Jew
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862489" TARGET="almagest" NAME="04.20.03.01.02"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.20.03.01.02]
		</SPAN>
		   <SPAN>
			 How the Sorcery of Zarvan and the Jew in the Matter of Mahbud is discovered, and how both are killed by Command of Nushirvan
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792967,'ILKH-231-2','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792967&t=" ALT="Nushirvan questions Zarvan on role in Mahbud murder [ILKH-231-2]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=869343" TARGET="almagest">ILKH-231-2</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862493" TARGET="almagest" NAME="04.20.03.01.03"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.20.03.01.03]
		</SPAN>
		   <SPAN>
			 In Praise of the Wisdom of Nushirvan, and how he builds the City of Sarsan
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862495" TARGET="almagest" NAME="04.20.03.01.04"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.20.03.01.04]
		</SPAN>
		   <SPAN>
			 The Story of the War of the Khan with Ghatqar, the Prince of the Hital, the Defeat of Ghatqar, and how they set Fughanish upon the Throne
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862498" TARGET="almagest" NAME="04.20.03.01.05"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.20.03.01.05]
		</SPAN>
		   <SPAN>
			 Nushirvan has Tidings of the Battle of the Khan with the Hital and how he leads a Host against the Khan
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862500" TARGET="almagest" NAME="04.20.03.01.06"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.20.03.01.06]
		</SPAN>
		   <SPAN>
			 the Khan has Tidings of the Coming of the Host of Nushirvan to Gurgan and writes a Letter in the Cause of Peace
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862502" TARGET="almagest" NAME="04.20.03.01.07"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.20.03.01.07]
		</SPAN>
		   <SPAN>
			 Nushirvan answers the Letter of the Khan
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792968,'ILKH-233-2','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792968&t=" ALT="Nushirvan dictates answer to Khagan letter [ILKH-233-2]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=869348" TARGET="almagest">ILKH-233-2</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862505" TARGET="almagest" NAME="04.20.03.01.08"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.20.03.01.08]
		</SPAN>
		   <SPAN>
			 The Khan bethinks himself and writes offering his Daughter in Marriage to Nushirvan
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862507" TARGET="almagest" NAME="04.20.03.01.09"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.20.03.01.09]
		</SPAN>
		   <SPAN>
			 Nushirvan answers the Letter, and sends Mihran Sitad to see and fetch the Daughter of the Khan
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792969,'ILKH-234-2','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792969&t=" ALT="Nushirvan envoy Mihran Setad in Khagan court [ILKH-234-2]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=869353" TARGET="almagest">ILKH-234-2</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862510" TARGET="almagest" NAME="04.20.03.01.10"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.20.03.01.10]
		</SPAN>
		   <SPAN>
			 The Khan sends his Daughter, escorted by Mihran, with a Letter and Treasures to Nushirvan
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862513" TARGET="almagest" NAME="04.20.03.01.11"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.20.03.01.11]
		</SPAN>
		   <SPAN>
			 The Khan withdraws, and how Nushirvan marches from Gurgan to Tisifun
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862515" TARGET="almagest" NAME="04.20.03.01.12"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.20.03.01.12]
		</SPAN>
		   <SPAN>
			 Discourse on the justice of Nushirvan and how Mortals have Peace under his Usages
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862517" TARGET="almagest" NAME="04.20.03.01.13"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.20.03.01.13]
		</SPAN>
		   <SPAN>
			 Buzurjmihr counsels Nushirvan and discourses on good Deeds and Words
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862520" TARGET="almagest" NAME="04.20.04.01.01"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.20.04.01.01]
		</SPAN>
		   <SPAN>
			 The Raja of Hind sends the Game of Chess to Nushirvan
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792765,'PECK-392-1','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792765&t=" ALT="Talkhand Despairs and Dies [PECK-392-1]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=868494" TARGET="almagest">PECK-392-1</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862525" TARGET="almagest" NAME="04.20.04.01.02"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.20.04.01.02]
		</SPAN>
		   <SPAN>
			 How Buzurjmihr invents Nard, and how Nushirvan sends it with a Letter to the Raja of Hind
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862529" TARGET="almagest" NAME="04.20.04.01.03"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.20.04.01.03]
		</SPAN>
		   <SPAN>
			 The Story of Gav and Talhand, and the Invention of Chess
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862535" TARGET="almagest" NAME="04.20.04.01.04"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.20.04.01.04]
		</SPAN>
		   <SPAN>
			 Nushirvan sends Barzuy, the Leech, to Hindustan to fetch a wondrous Drug, and how Barzuy brings back the Book of Kalilah and Damnah
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862538" TARGET="almagest" NAME="04.20.05.01.01"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.20.05.01.01]
		</SPAN>
		   <SPAN>
			 Nushdirwan is wroth with Buzurjmihr and orders him to be put in Ward
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862541" TARGET="almagest" NAME="04.20.05.01.02"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.20.05.01.02]
		</SPAN>
		   <SPAN>
			 The Ambassador of Caesar comes to Nushirvan with a locked Casket and how Buzurjmihr is set at large to declare its Contents
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862545" TARGET="almagest" NAME="04.20.05.01.03"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.20.05.01.03]
		</SPAN>
		   <SPAN>
			 Discourse on the Responses of Nushirvan
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862548" TARGET="almagest" NAME="04.20.05.01.04"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.20.05.01.04]
		</SPAN>
		   <SPAN>
			 Nushirvan's Letter of Counsel to his Son Hurmazd
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862550" TARGET="almagest" NAME="04.20.05.01.05"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.20.05.01.05]
		</SPAN>
		   <SPAN>
			 How an Archmage questions Nushirvan and how he makes Answer
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862552" TARGET="almagest" NAME="04.20.06.01.01"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.20.06.01.01]
		</SPAN>
		   <SPAN>
			 Nushirvan makes ready to war against Caesar
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862554" TARGET="almagest" NAME="04.20.06.01.02"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.20.06.01.02]
		</SPAN>
		   <SPAN>
			 Nushirvan takes the Stronghold of Sakila and how a Shoemaker has Dealings with him
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792766,'PECK-400-2','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792766&t=" ALT="Nushirvan besieging the Byzantine fortress of Saqila [PECK-400-2]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=868499" TARGET="almagest">PECK-400-2</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862558" TARGET="almagest" NAME="04.20.06.01.03"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.20.06.01.03]
		</SPAN>
		   <SPAN>
			 The Envoys of Caesar come to Nushirvan with Apologies and Presents
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862561" TARGET="almagest" NAME="04.20.06.01.04"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.20.06.01.04]
		</SPAN>
		   <SPAN>
			 Nushirvan choses Hurmazd  as his Successor
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862563" TARGET="almagest" NAME="04.20.06.01.05"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.20.06.01.05]
		</SPAN>
		   <SPAN>
			 The Archimages question Hurmazd  and how he replies
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862565" TARGET="almagest" NAME="04.20.06.01.06"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.20.06.01.06]
		</SPAN>
		   <SPAN>
			 Nushirvan appoints Hurmazd  as his Successor and gives him parting Counsels
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862567" TARGET="almagest" NAME="04.20.06.01.07"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.20.06.01.07]
		</SPAN>
		   <SPAN>
			 Nushirvan has a Dream and how Buzurjmihr interprets it as signifying the Appearance of Muhammad
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR>
	<TD COLSPAN="2" STYLE="font-weight:bold;color:white;background-color:gray;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862569" STYLE="color:white;" TARGET="almagest">4.21 HURMAZD , SON OF NUSHIRVAN</A>
	</TD>
</TR>

<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862571" TARGET="almagest" NAME="04.21.01.01.01"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.21.01.01.01]
		</SPAN>
		   <SPAN>
			 The Prelude
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862573" TARGET="almagest" NAME="04.21.01.01.02"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.21.01.01.02]
		</SPAN>
		   <SPAN>
			 Hurmazd  ascends the Throne and harangues the Chiefs
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862576" TARGET="almagest" NAME="04.21.01.01.03"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.21.01.01.03]
		</SPAN>
		   <SPAN>
			 Hurmazd  kills Izad Gushasp, Zardusht , Simah Burzin, and Bahrani Azarmihan, his Father's Ministers
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862579" TARGET="almagest" NAME="04.21.01.01.04"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.21.01.01.04]
		</SPAN>
		   <SPAN>
			 Hurmazd  turns from Tyranny to justice
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862583" TARGET="almagest" NAME="04.21.01.01.05"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.21.01.01.05]
		</SPAN>
		   <SPAN>
			 Hosts gathers from all Sides against Hurmazd , and how he takes Counsel with his Wazirs
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862586" TARGET="almagest" NAME="04.21.01.01.06"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.21.01.01.06]
		</SPAN>
		   <SPAN>
			 Hurmazd  hears of Bahram Chubinah and sends for him
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862588" TARGET="almagest" NAME="04.21.01.01.07"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.21.01.01.07]
		</SPAN>
		   <SPAN>
			 Bahram Chubinah comes to Hurmazd  and is made Captain of the Host
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862591" TARGET="almagest" NAME="04.21.01.01.08"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.21.01.01.08]
		</SPAN>
		   <SPAN>
			 Bahram Chubinah goes with twelve thousand Cavaliers to fight King Savah
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862595" TARGET="almagest" NAME="04.21.01.01.09"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.21.01.01.09]
		</SPAN>
		   <SPAN>
			 King Savah sends a Message to Bahram Chubinah and his Answer
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862597" TARGET="almagest" NAME="04.21.01.01.10"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.21.01.01.10]
		</SPAN>
		   <SPAN>
			 King Savah and Bahram Chubinah set the Battle in Array against each other
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862599" TARGET="almagest" NAME="04.21.01.01.11"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.21.01.01.11]
		</SPAN>
		   <SPAN>
			 King Savah sends another Message to Bahrani Chubinah and his Answer
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862601" TARGET="almagest" NAME="04.21.01.01.12"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.21.01.01.12]
		</SPAN>
		   <SPAN>
			 Bahram Chubinah has a Dream in the Night, how he gives Battle the next Morning, and how King Savah is killed
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792767,'PECK-411-2','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792767&t=" ALT="Bahram Chubinah defeats, kills Savah [PECK-411-2]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=868504" TARGET="almagest">PECK-411-2</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862604" TARGET="almagest" NAME="04.21.01.01.13"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.21.01.01.13]
		</SPAN>
		   <SPAN>
			 Bahram Chubinah sends a Letter announcing his Victory, and the Head of King Savah, to Hurmazd , and his Answer
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862606" TARGET="almagest" NAME="04.21.01.01.14"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.21.01.01.14]
		</SPAN>
		   <SPAN>
			 Bahram Chubinah fights with Parmudah, Son of King Savah, and overcomes him, and how Parmudah takes Refuge in the Hold of Avazah
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862608" TARGET="almagest" NAME="04.21.01.01.15"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.21.01.01.15]
		</SPAN>
		   <SPAN>
			 Bahram Chubinah sends a Message to Parmfida and how Parmudah asks Quarter
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862611" TARGET="almagest" NAME="04.21.01.01.16"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.21.01.01.16]
		</SPAN>
		   <SPAN>
			 Bahram Chubinah asks of Hurmazd  a Warrant to spare the Life of Parmudah and the Answer
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862613" TARGET="almagest" NAME="04.21.01.01.17"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.21.01.01.17]
		</SPAN>
		   <SPAN>
			 Hurmazd 's Letter, granting Quarter to Parmudah, reaches Bahram Chubinah, and how Bahram Chubinah is wroth with Parmudah
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862615" TARGET="almagest" NAME="04.21.01.01.18"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.21.01.01.18]
		</SPAN>
		   <SPAN>
			 Parmida comes before Hurmazd  with the Treasures sent by Bahram Chubinah
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862617" TARGET="almagest" NAME="04.21.01.01.19"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.21.01.01.19]
		</SPAN>
		   <SPAN>
			 Hurmazd  hears of the Ill-doing of Bahram Chubinah and makes a Compact with the Khan
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862620" TARGET="almagest" NAME="04.21.01.01.20"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.21.01.01.20]
		</SPAN>
		   <SPAN>
			 Hurmazd  writes a chiding Letter to Bahram Chubinah and sends him a Distaff-case, Cotton, and Women's Raiment
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862622" TARGET="almagest" NAME="04.21.01.01.21"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.21.01.01.21]
		</SPAN>
		   <SPAN>
			 Bahram Chubinah puts on the Woman's Dress and shows himself therein to the Chiefs of the Host
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862626" TARGET="almagest" NAME="04.21.01.01.22"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.21.01.01.22]
		</SPAN>
		   <SPAN>
			 Bahram Chubinah goes to hunt and sees a Lady who foretells the Future to him
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862629" TARGET="almagest" NAME="04.21.01.01.23"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.21.01.01.23]
		</SPAN>
		   <SPAN>
			 Bahram Chubinah assumes the royal Style and how Kharrad, Son of Burzin, and the Archscribe flee
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862631" TARGET="almagest" NAME="04.21.01.01.24"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.21.01.01.24]
		</SPAN>
		   <SPAN>
			 Hurmazd  receives News of Bahram Chubinah's Doings, and how Bahram Chubinah sends a Frail of Swords to Hurmazd
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862633" TARGET="almagest" NAME="04.21.01.01.25"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.21.01.01.25]
		</SPAN>
		   <SPAN>
			 Bahram Chubinah makes known to the Chiefs his Designs upon the Throne, and how his Sister Gurdya advises him
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862636" TARGET="almagest" NAME="04.21.01.01.26"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.21.01.01.26]
		</SPAN>
		   <SPAN>
			 Bahram Chubinah's Letter to the Khan and how he coins Money with the name of Khusraw Parwiz and sent it to Hurmazd
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862638" TARGET="almagest" NAME="04.21.01.01.27"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.21.01.01.27]
		</SPAN>
		   <SPAN>
			 Bahram Chubinah writes to Hurmazd  and how Khusraw Parwiz flees from his Father
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862640" TARGET="almagest" NAME="04.21.01.01.28"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.21.01.01.28]
		</SPAN>
		   <SPAN>
			 Hurmazd  sends Ayin Gushasp with an Army to fight Bahram Chubinah and how he is killed by his Comrade
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862644" TARGET="almagest" NAME="04.21.01.01.29"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.21.01.01.29]
		</SPAN>
		   <SPAN>
			 Hurmazd  grieved, refuses Audience to the Iranians, and is blinded by Banduy and Gustaham
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862647" TARGET="almagest" NAME="04.21.01.01.30"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.21.01.01.30]
		</SPAN>
		   <SPAN>
			 Khusraw Parwiz hears of the Blinding of Hurmazd
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR>
	<TD COLSPAN="2" STYLE="font-weight:bold;color:white;background-color:gray;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862649" STYLE="color:white;" TARGET="almagest">4.22 KHUSRAW PARWIZ</A>
	</TD>
</TR>

<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862651" TARGET="almagest" NAME="04.22.01.01.01"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.22.01.01.01]
		</SPAN>
		   <SPAN>
			 The Prelude
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862653" TARGET="almagest" NAME="04.22.01.01.02"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.22.01.01.02]
		</SPAN>
		   <SPAN>
			 Khusraw Parwiz sits upon the Throne and makes an Oration
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862656" TARGET="almagest" NAME="04.22.01.01.03"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.22.01.01.03]
		</SPAN>
		   <SPAN>
			 Khusraw Parwiz visits his Father and asks Forgiveness
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862658" TARGET="almagest" NAME="04.22.01.01.04"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.22.01.01.04]
		</SPAN>
		   <SPAN>
			 Bahram Chubinah hears of the Blinding of Shah Hurmazd  and how he leads his Troops against Khusraw Parwiz
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862660" TARGET="almagest" NAME="04.22.01.01.05"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.22.01.01.05]
		</SPAN>
		   <SPAN>
			 Khusraw Parwiz and Bahram Chubinah meet and parley
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792769,'PECK-422-2','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792769&t=" ALT="Khusraw Parviz, Bahram Chubinah dispute kingship [PECK-422-2]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=869440" TARGET="almagest">PECK-422-2</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862664" TARGET="almagest" NAME="04.22.01.01.06"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.22.01.01.06]
		</SPAN>
		   <SPAN>
			 Bahrain Chubinah and Khusraw Parwiz returns, how Gurdya advised Bahram Chubinah, and how Khusraw Parwiz told his Purpose to the Iranians
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862666" TARGET="almagest" NAME="04.22.01.01.07"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.22.01.01.07]
		</SPAN>
		   <SPAN>
			 Bahram Chubinah attacks the Army of Khusraw Parwiz by Night and how Khusraw Parwiz flee
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862669" TARGET="almagest" NAME="04.22.01.01.08"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.22.01.01.08]
		</SPAN>
		   <SPAN>
			 Khusraw Parwiz goes to his Sire and fled to Rum, and how Hurmazd  is killed
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862672" TARGET="almagest" NAME="04.22.01.01.09"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.22.01.01.09]
		</SPAN>
		   <SPAN>
			 Bahram Chubinah sent Troops after Khusraw Parwiz and how Banduy contrives to rescue him from their Hands
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862674" TARGET="almagest" NAME="04.22.01.01.10"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.22.01.01.10]
		</SPAN>
		   <SPAN>
			 Bahram, the Son of Siyavash, takes Banduy and carries him to Bahram Chubinah
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862676" TARGET="almagest" NAME="04.22.01.01.11"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.22.01.01.11]
		</SPAN>
		   <SPAN>
			 Bahram Chubinah summons the Magnates of Iran, how they discuss his Pretensions to the Kingdom, and how he acceds to the Throne
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862679" TARGET="almagest" NAME="04.22.01.01.12"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.22.01.01.12]
		</SPAN>
		   <SPAN>
			 Banduy plots with Bahrani, the Son of Siyavash, to slay Bahrarn Chubinah, and how Banduy flees from Bond
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862682" TARGET="almagest" NAME="04.22.01.01.13"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.22.01.01.13]
		</SPAN>
		   <SPAN>
			 Khusraw Parwiz goes toward Rum by the Desert-route and how a Hermit tells him of the Past and Future
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862685" TARGET="almagest" NAME="04.22.01.01.14"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.22.01.01.14]
		</SPAN>
		   <SPAN>
			 A Cavalier of Caesar came to Khusraw Parwiz and how he sends an Answer by Gustaham, Balwi, Andiyan, Kharrad, Son of Burzin, and Shapur to Caesar
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862688" TARGET="almagest" NAME="04.22.01.01.15"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.22.01.01.15]
		</SPAN>
		   <SPAN>
			 Caesar answers the Letter of Khusraw Parwiz
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862690" TARGET="almagest" NAME="04.22.01.01.16"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.22.01.01.16]
		</SPAN>
		   <SPAN>
			 Caesar writes to Khusraw Parwiz, declining to help him, and his Answer
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862692" TARGET="almagest" NAME="04.22.01.01.17"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.22.01.01.17]
		</SPAN>
		   <SPAN>
			 Caesar writes the second Time to Khusraw Parwiz about giving him Aid
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862694" TARGET="almagest" NAME="04.22.01.01.18"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.22.01.01.18]
		</SPAN>
		   <SPAN>
			 Khusraw Parwiz answers Caesar about the Alliance
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862696" TARGET="almagest" NAME="04.22.01.01.19"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.22.01.01.19]
		</SPAN>
		   <SPAN>
			 Caesar makes a Talisman and deceives the Envoys of Khusraw Parwiz, and how Kharrad, Son of Burzin, solves the Mystery
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862700" TARGET="almagest" NAME="04.22.01.01.20"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.22.01.01.20]
		</SPAN>
		   <SPAN>
			 Kharrad, Son of Burzin, expounds the Faith of the Indians and exhorts Caesar
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862703" TARGET="almagest" NAME="04.22.01.01.21"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.22.01.01.21]
		</SPAN>
		   <SPAN>
			 Caesar sends a Host and his Daughter to Khusran Parwiz
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862706" TARGET="almagest" NAME="04.22.01.01.22"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.22.01.01.22]
		</SPAN>
		   <SPAN>
			 Khusraw Parwiz leads his Host to Adhar Abadagan and how Banduy meets him on the Way
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862708" TARGET="almagest" NAME="04.22.01.01.23"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.22.01.01.23]
		</SPAN>
		   <SPAN>
			 Bahram Chubinah has Tidings of the Coming of Khusraw Parwiz and writes a Letter to the Chiefs of Iran, and how the Letter falls into the Hands of Khusraw Parwiz and his Answer
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862710" TARGET="almagest" NAME="04.22.01.01.24"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.22.01.01.24]
		</SPAN>
		   <SPAN>
			 Khusraw Parwiz fights with Bahram Chubinah and how Kut, the Ruman, is killed
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792770,'PECK-437-1','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792770&t=" ALT="Bahram Chubinah slays Kut Ruman [PECK-437-1]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=869450" TARGET="almagest">PECK-437-1</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862713" TARGET="almagest" NAME="04.22.01.01.25"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.22.01.01.25]
		</SPAN>
		   <SPAN>
			 Khusraw Parwiz fights with Pahram Chubinah the second Time, is defeated, and escapes from him by the Help of Surush
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862718" TARGET="almagest" NAME="04.22.01.01.26"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.22.01.01.26]
		</SPAN>
		   <SPAN>
			 Khusraw Parwiz fights the third Time with Bahram Chubinah and defeats him
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862722" TARGET="almagest" NAME="04.22.01.01.27"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.22.01.01.27]
		</SPAN>
		   <SPAN>
			 Khusraw Parwiz sends an Army under Nastuh after Bahram Chubinah, and how Bahram Chubinah captures him and reaches Khaqan of China
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862725" TARGET="almagest" NAME="04.22.01.01.28"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.22.01.01.28]
		</SPAN>
		   <SPAN>
			 Khusraw Parwiz pillages the Camp of Bahram Chubinah and writes a Letter to Caesar who answers it with a Robe of Honour and Gifts
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862727" TARGET="almagest" NAME="04.22.01.01.29"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.22.01.01.29]
		</SPAN>
		   <SPAN>
			 Niyatus is wroth with Banduy and how Maryam makes Peace between them
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862729" TARGET="almagest" NAME="04.22.01.01.30"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.22.01.01.30]
		</SPAN>
		   <SPAN>
			 Khusraw Parwiz gives Presents to Niyatus and the Rumans, how he dismisses them to Rum, and writes Patents for the Nobles of Iran
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862731" TARGET="almagest" NAME="04.22.01.01.31"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.22.01.01.31]
		</SPAN>
		   <SPAN>
			 Firdawsi's Lament for the Death of his Son
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862733" TARGET="almagest" NAME="04.22.01.01.32"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.22.01.01.32]
		</SPAN>
		   <SPAN>
			 The Story of Bahram Chubinah and Khaqan of China
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862735" TARGET="almagest" NAME="04.22.01.01.33"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.22.01.01.33]
		</SPAN>
		   <SPAN>
			 Maqaturah is killed by Bahram Chubinah
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862738" TARGET="almagest" NAME="04.22.01.01.34"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.22.01.01.34]
		</SPAN>
		   <SPAN>
			 The Lion-ape kills a Daughter of the Khan, how it is killed by Bahram Chubinah, and how the Khan gives him a Daughter and the Kingdom of Chin
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862742" TARGET="almagest" NAME="04.22.01.01.35"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.22.01.01.35]
		</SPAN>
		   <SPAN>
			 Khusraw Parwiz hears of the Case of Bahram Chubinah and writes a Letter to the Khan, and how he replies
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862744" TARGET="almagest" NAME="04.22.01.01.36"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.22.01.01.36]
		</SPAN>
		   <SPAN>
			 Khusraw Parwiz sends Kharrad, Son of Burzin, to the Khan and how he schemes to slay Bahram Chubinah
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862746" TARGET="almagest" NAME="04.22.01.01.37"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.22.01.01.37]
		</SPAN>
		   <SPAN>
			 Bahram Chubinah is killed by Qulun as Kharrad, Son of Burzin, had planned
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792801,'56G-598-1','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792801&t=" ALT="Captured Qolun mortally wounds Bahram Chubinah [56G-598-1]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=868813" TARGET="almagest">56G-598-1</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862749" TARGET="almagest" NAME="04.22.01.01.38"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.22.01.01.38]
		</SPAN>
		   <SPAN>
			 The Khan has Tidings of Bahram Chubinah's Death and how he destroys the House and Family of Qulun
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862751" TARGET="almagest" NAME="04.22.01.01.39"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.22.01.01.39]
		</SPAN>
		   <SPAN>
			 Khusraw Parwiz had Tidings of the Slaying of Bahram Chubinah and honours Kharrad, Son of Burzin
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862753" TARGET="almagest" NAME="04.22.01.01.40"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.22.01.01.40]
		</SPAN>
		   <SPAN>
			 The Khan sends his Brother to Gurdya, the Sister of Bahram Chubinah, with a Letter touching her Brother's Death and asking her in Marriage as his Queen, and her Answer
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862755" TARGET="almagest" NAME="04.22.01.01.41"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.22.01.01.41]
		</SPAN>
		   <SPAN>
			 Gurdya consults her Nobles and fled from Marv
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862758" TARGET="almagest" NAME="04.22.01.01.42"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.22.01.01.42]
		</SPAN>
		   <SPAN>
			 The Khan received Tidings of the Flight of Gurdya and how he sent Tuvurg with an Army after her, and how Gurdya kills Tuvurg
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862761" TARGET="almagest" NAME="04.22.01.01.43"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.22.01.01.43]
		</SPAN>
		   <SPAN>
			 Gurdya writes to Gurdiyah
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862763" TARGET="almagest" NAME="04.22.01.01.44"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.22.01.01.44]
		</SPAN>
		   <SPAN>
			 Khusraw Parwiz kills Banduy
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862766" TARGET="almagest" NAME="04.22.01.01.45"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.22.01.01.45]
		</SPAN>
		   <SPAN>
			 Gustaham rebels against Khusraw Parwiz and takes Gurdya to Wife
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862768" TARGET="almagest" NAME="04.22.01.01.46"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.22.01.01.46]
		</SPAN>
		   <SPAN>
			 Khusraw Parwiz takes Counsel with Gurdiyah concerning Gustaham and how Gurdya, prompted by Gurdiyah, kills him
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862771" TARGET="almagest" NAME="04.22.01.01.47"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.22.01.01.47]
		</SPAN>
		   <SPAN>
			 Gurdya writes to Khusraw Parwiz and how he summons and married her
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862775" TARGET="almagest" NAME="04.22.01.01.48"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.22.01.01.48]
		</SPAN>
		   <SPAN>
			 Gurdya shows her Accomplishment before Khusraw Parwiz
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862778" TARGET="almagest" NAME="04.22.01.01.49"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.22.01.01.49]
		</SPAN>
		   <SPAN>
			 Khusraw Parwiz sends an ill-disposed Marchlord to Ray and how he oppresses the Folk there
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862780" TARGET="almagest" NAME="04.22.01.01.50"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.22.01.01.50]
		</SPAN>
		   <SPAN>
			 Gurdya makes Sport before Khusraw Parwiz and how he gives Ray to her
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862782" TARGET="almagest" NAME="04.22.01.01.51"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.22.01.01.51]
		</SPAN>
		   <SPAN>
			 Khusraw Parwiz portions out his Realm
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862784" TARGET="almagest" NAME="04.22.01.01.52"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.22.01.01.52]
		</SPAN>
		   <SPAN>
			 Shiruy, the Son of Khusraw Parwiz, is born of Maryam with bad Auspices and how Khusraw Parwiz informs Cesar
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862786" TARGET="almagest" NAME="04.22.01.01.53"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.22.01.01.53]
		</SPAN>
		   <SPAN>
			 Caesar writes a Letter to Khusraw Parwiz, sent Gifts, and asks for the Cross of Christ
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862788" TARGET="almagest" NAME="04.22.01.01.54"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.22.01.01.54]
		</SPAN>
		   <SPAN>
			 Khusraw Parwiz answers Caesar's Letter and sends Gifts
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862790" TARGET="almagest" NAME="04.22.02.01.01"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.22.02.01.01]
		</SPAN>
		   <SPAN>
			 The Prelude
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862792" TARGET="almagest" NAME="04.22.02.01.02"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.22.02.01.02]
		</SPAN>
		   <SPAN>
			 Khusraw Parwiz loves Shirin, how they part, how he meets her again while hunting and sends her to his Bower <No number in Peck #8 CTL>
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792771,'PECK-452-2','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792771&t=" ALT="Khusraw Parviz visits Shirin in her castle [PECK-452-2]" HEIGHT="100"></A>
				</TD>
				
				 <TD>
					<A HREF="javascript:viewImage(792768,'PECK-416-2','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792768&t=" ALT="Khusraw Parviz visits Shirin in her castle [PECK-416-2]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=868668" TARGET="almagest">PECK-452-2</A>
				</TD>
				
				 <TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=869416" TARGET="almagest">PECK-416-2</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862801" TARGET="almagest" NAME="04.22.02.01.03"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.22.02.01.03]
		</SPAN>
		   <SPAN>
			 The Nobles hear that Shirin has come to the Bower of Khusraw Parwiz and how they advise him and are satisfied with his Answer
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862803" TARGET="almagest" NAME="04.22.02.01.04"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.22.02.01.04]
		</SPAN>
		   <SPAN>
			 Shirin murders Maryam and Khusraw Parwiz puts Shirin in Bonds
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862805" TARGET="almagest" NAME="04.22.02.01.05"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.22.02.01.05]
		</SPAN>
		   <SPAN>
			 Khusraw Parwiz makes the Throne of Taqdis
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862807" TARGET="almagest" NAME="04.22.02.01.06"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.22.02.01.06]
		</SPAN>
		   <SPAN>
			 The Story of Sarkash and Barbad, the Minstrel, and Khusraw Parwiz
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862809" TARGET="almagest" NAME="04.22.02.01.07"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.22.02.01.07]
		</SPAN>
		   <SPAN>
			 Khusran Parwiz builds the Palace of Mada'in Discourse on the Splendour and Greatness of Khusraw Parwiz
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862811" TARGET="almagest" NAME="04.22.02.01.08"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.22.02.01.08]
		</SPAN>
		   <SPAN>
			 Khusraw Parwiz turns from justice, how the Chiefs revolt, and how Guraz calls in Caesar
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862813" TARGET="almagest" NAME="04.22.02.01.09"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.22.02.01.09]
		</SPAN>
		   <SPAN>
			 Caesar withdraws through an Expedient of Khusraw Parwiz and how the Chiefs release Shiruy from Bonds
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862815" TARGET="almagest" NAME="04.22.02.01.10"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.22.02.01.10]
		</SPAN>
		   <SPAN>
			 Khusraw Parwiz is taken and how Shiruy sends him to Tisifun
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR>
	<TD COLSPAN="2" STYLE="font-weight:bold;color:white;background-color:gray;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862818" STYLE="color:white;" TARGET="almagest">4.23 QUBAD (COMMONLY CALLED SHIRUY)</A>
	</TD>
</TR>

<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862820" TARGET="almagest" NAME="04.23.01.01.01"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.23.01.01.01]
		</SPAN>
		   <SPAN>
			 Shiruy ascends the Throne, announces his Will, and sends Chiefs to his Father with Counsel and Excuses
		</SPAN>
	</TD>
	 <TD>
		<TABLE>
			<TR>
				<TD>
					<A HREF="javascript:viewImage(792763,'PECK-361-1','SHAHNAMEH');"><IMG SRC="/almagest3/Mediafile?proj=SHAHNAMEH&id=792763&t=" ALT="Qubad on the throne set up for him by his brother [PECK-361-1]" HEIGHT="100"></A>
				</TD>
				
			</TR>
			 <TR>
				<TD STYLE="font-size:8pt;text-align:center;color:green;">
					<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=869411" TARGET="almagest">PECK-361-1</A>
				</TD>
				
			</TR>
		</TABLE>
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862825" TARGET="almagest" NAME="04.23.01.01.02"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.23.01.01.02]
		</SPAN>
		   <SPAN>
			 Khusraw Parwiz answeres Shiruy
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862828" TARGET="almagest" NAME="04.23.01.01.03"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.23.01.01.03]
		</SPAN>
		   <SPAN>
			 Shiruy grieves for Khusraw Parwiz and how the Chiefs sre displeased thereat
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862830" TARGET="almagest" NAME="04.23.01.01.04"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.23.01.01.04]
		</SPAN>
		   <SPAN>
			 Barbad laments Khusraw Parwiz, cuts off his own Fingers, and burns his Instruments of Music
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862832" TARGET="almagest" NAME="04.23.01.01.05"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.23.01.01.05]
		</SPAN>
		   <SPAN>
			 The Chiefs demand from Shiruy the Death of Khusraw Parwiz and how he is killed by Mihr Hurmazd
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862836" TARGET="almagest" NAME="04.23.01.01.06"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.23.01.01.06]
		</SPAN>
		   <SPAN>
			 Shiruy asks Shirin in Marriage, how Shirin kills herself, and how Shiruy is killed
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR>
	<TD COLSPAN="2" STYLE="font-weight:bold;color:white;background-color:gray;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862841" STYLE="color:white;" TARGET="almagest">4.24 ARDSHIR, SON OF SHIRWA</A>
	</TD>
</TR>

<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862843" TARGET="almagest" NAME="04.24.01.01.01"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.24.01.01.01]
		</SPAN>
		   <SPAN>
			 Ardshir, Son of Shiruy, ascended the Throne and harangued the Chiefs
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862846" TARGET="almagest" NAME="04.24.01.01.02"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.24.01.01.02]
		</SPAN>
		   <SPAN>
			 Guraz is displeased at Ardshir being Shah and how he causes Ardshir to be killed by Piruz Son of Khusraw
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR>
	<TD COLSPAN="2" STYLE="font-weight:bold;color:white;background-color:gray;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862848" STYLE="color:white;" TARGET="almagest">4.25 GURAZ</A>
	</TD>
</TR>

<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862850" TARGET="almagest" NAME="04.25.01.01.01"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.25.01.01.01]
		</SPAN>
		   <SPAN>
			 Gurdz, (also called Farayin,) receives News of the Slaying of Ardshir, hastenes to Iran, takes Possession of the Throne, and is killed by Shahranguraz
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR>
	<TD COLSPAN="2" STYLE="font-weight:bold;color:white;background-color:gray;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862852" STYLE="color:white;" TARGET="almagest">4.26 PURANDUKHT</A>
	</TD>
</TR>

<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862854" TARGET="almagest" NAME="04.26.01.01.01"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.26.01.01.01]
		</SPAN>
		   <SPAN>
			 Purandukht ascends the Throne and kills Piruz, Son of Khusraw, and how her own Life ends
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR>
	<TD COLSPAN="2" STYLE="font-weight:bold;color:white;background-color:gray;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862858" STYLE="color:white;" TARGET="almagest">4.27 AZARMIDUKHT</A>
	</TD>
</TR>

<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862860" TARGET="almagest" NAME="04.27.01.01.01"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.27.01.01.01]
		</SPAN>
		   <SPAN>
			 Azarmidukht ascends the Throne and how she dies
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR>
	<TD COLSPAN="2" STYLE="font-weight:bold;color:white;background-color:gray;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862862" STYLE="color:white;" TARGET="almagest">4.28 FARRUKHZAD</A>
	</TD>
</TR>

<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862864" TARGET="almagest" NAME="04.28.01.01.01"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.28.01.01.01]
		</SPAN>
		   <SPAN>
			 Farrukhzad ascends the Throne and how he is killed by a Slave
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR>
	<TD COLSPAN="2" STYLE="font-weight:bold;color:white;background-color:gray;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862866" STYLE="color:white;" TARGET="almagest">4.29 YAZDIGIRD</A>
	</TD>
</TR>

<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862868" TARGET="almagest" NAME="04.29.01.01.01"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.29.01.01.01]
		</SPAN>
		   <SPAN>
			 Yazdigird ascends the Throne and addresses the Chiefs
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862871" TARGET="almagest" NAME="04.29.01.01.02"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.29.01.01.02]
		</SPAN>
		   <SPAN>
			 Sa'd-i Vaqqas' invades Iran, how Yazdigird sends Rustam to oppose him, and how Rustam writes a Letter to his Brother
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862873" TARGET="almagest" NAME="04.29.01.01.03"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.29.01.01.03]
		</SPAN>
		   <SPAN>
			 Rustam writes to Sa'd-i Vaqqas, and how he replies
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862875" TARGET="almagest" NAME="04.29.01.01.04"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.29.01.01.04]
		</SPAN>
		   <SPAN>
			 Rustam fights with Sa'd-i Vaqqas, and is killed
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862878" TARGET="almagest" NAME="04.29.01.01.05"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.29.01.01.05]
		</SPAN>
		   <SPAN>
			 Yazdigird consults with the Iranians and goes to Khurasan
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862880" TARGET="almagest" NAME="04.29.01.01.06"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.29.01.01.06]
		</SPAN>
		   <SPAN>
			 Yazdigird writes to Mahuyof Sur
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862882" TARGET="almagest" NAME="04.29.01.01.07"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.29.01.01.07]
		</SPAN>
		   <SPAN>
			 Yazdigird writes to the Marchlords of Tas
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862884" TARGET="almagest" NAME="04.29.01.01.08"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.29.01.01.08]
		</SPAN>
		   <SPAN>
			 Yazdigird goes to Tus and how Mahuyof Sur mets him
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862887" TARGET="almagest" NAME="04.29.01.01.09"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.29.01.01.09]
		</SPAN>
		   <SPAN>
			 Mahuyof Sur incits Bizhan to war with Yazdigird and how Yazdigird flees and hides himself in a Mill
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862890" TARGET="almagest" NAME="04.29.01.01.10"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.29.01.01.10]
		</SPAN>
		   <SPAN>
			 Mahuyof Sur sends the Miller to kill Yazdigird, and how the Archimages counsel Mahuyto forbear
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862892" TARGET="almagest" NAME="04.29.01.01.11"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.29.01.01.11]
		</SPAN>
		   <SPAN>
			 Yazdigird is killed by Khusraw, the Miller
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862895" TARGET="almagest" NAME="04.29.01.01.12"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.29.01.01.12]
		</SPAN>
		   <SPAN>
			 Mahuyof Sur is informed of the Obsequies of Yazdigird and ascends the Throne
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862897" TARGET="almagest" NAME="04.29.01.01.13"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.29.01.01.13]
		</SPAN>
		   <SPAN>
			 Bizhan, hearing of the Slaying of Yazdigird and of Mahuyof Sur's Accession to the Throne, leads forth the Host to fight with him
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862899" TARGET="almagest" NAME="04.29.01.01.14"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.29.01.01.14]
		</SPAN>
		   <SPAN>
			 Mahuyof Sur is taken and killed by Order of Bizhan
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>
<TR STYLE="">
	<TD WIDTH="30%" STYLE="vertical-align:top;">
		<A HREF="/almagest3/main.jsp?cmd=display&proj=SHAHNAMEH&id=862902" TARGET="almagest" NAME="04.29.01.01.15"> <SPAN STYLE="color:green;font-size:9pt;">
			[04.29.01.01.15]
		</SPAN>
		   <SPAN>
			 Account of the Completion of the Shahmama
		</SPAN>
	</TD>
	 <TD>
		&nbsp;
	</TD>
</TR>

	<tr><td colspan="2">Text Map (Experimental)</td></tr>
	<TR>
	<TD>
		KAYUMARS 
	</TD>
	 <TD>
		<A HREF="#01.01.01.01.01" STYLE="font-weight:bold;">&nbsp; 1 -----</A>
	</TD>
</TR>
<TR>
	<TD>
		JAMSHID 
	</TD>
	 <TD>
		<A HREF="#01.04.01.01.01" STYLE="font-weight:bold;">&nbsp; 1 -----</A>
	</TD>
</TR>
<TR>
	<TD>
		JAMSHID 
	</TD>
	 <TD>
		<A HREF="#01.04.01.01.04" STYLE="font-weight:bold;">&nbsp; 1 -----</A>
	</TD>
</TR>
<TR>
	<TD>
		ZAHHAK 
	</TD>
	 <TD>
		<A HREF="#01.05.01.01.01" STYLE="font-weight:bold;">&nbsp; 2 ----------</A>
	</TD>
</TR>
<TR>
	<TD>
		ZAHHAK 
	</TD>
	 <TD>
		<A HREF="#01.05.01.01.04" STYLE="font-weight:bold;">&nbsp; 1 -----</A>
	</TD>
</TR>
<TR>
	<TD>
		ZAHHAK 
	</TD>
	 <TD>
		<A HREF="#01.05.01.01.06" STYLE="font-weight:bold;">&nbsp; 1 -----</A>
	</TD>
</TR>
<TR>
	<TD>
		ZAHHAK 
	</TD>
	 <TD>
		<A HREF="#01.05.01.01.09" STYLE="font-weight:bold;">&nbsp; 4 --------------------</A>
	</TD>
</TR>
<TR>
	<TD>
		 FARIDUN 
	</TD>
	 <TD>
		<A HREF="#01.06.01.01.06" STYLE="font-weight:bold;">&nbsp; 1 -----</A>
	</TD>
</TR>
<TR>
	<TD>
		 FARIDUN 
	</TD>
	 <TD>
		<A HREF="#01.06.01.01.13" STYLE="font-weight:bold;">&nbsp; 2 ----------</A>
	</TD>
</TR>
<TR>
	<TD>
		 FARIDUN 
	</TD>
	 <TD>
		<A HREF="#01.06.01.01.21" STYLE="font-weight:bold;">&nbsp; 2 ----------</A>
	</TD>
</TR>
<TR>
	<TD>
		MANUCHIHR 
	</TD>
	 <TD>
		<A HREF="#01.07.01.01.01" STYLE="font-weight:bold;">&nbsp; 1 -----</A>
	</TD>
</TR>
<TR>
	<TD>
		MANUCHIHR 
	</TD>
	 <TD>
		<A HREF="#01.07.01.01.09" STYLE="font-weight:bold;">&nbsp; 1 -----</A>
	</TD>
</TR>
<TR>
	<TD>
		MANUCHIHR 
	</TD>
	 <TD>
		<A HREF="#01.07.01.01.11" STYLE="font-weight:bold;">&nbsp; 4 --------------------</A>
	</TD>
</TR>
<TR>
	<TD>
		MANUCHIHR 
	</TD>
	 <TD>
		<A HREF="#01.07.01.01.15" STYLE="font-weight:bold;">&nbsp; 1 -----</A>
	</TD>
</TR>
<TR>
	<TD>
		MANUCHIHR 
	</TD>
	 <TD>
		<A HREF="#01.07.01.01.23" STYLE="font-weight:bold;">&nbsp; 1 -----</A>
	</TD>
</TR>
<TR>
	<TD>
		MANUCHIHR 
	</TD>
	 <TD>
		<A HREF="#01.07.01.01.25" STYLE="font-weight:bold;">&nbsp; 1 -----</A>
	</TD>
</TR>
<TR>
	<TD>
		MANUCHIHR 
	</TD>
	 <TD>
		<A HREF="#01.07.01.01.28" STYLE="font-weight:bold;">&nbsp; 1 -----</A>
	</TD>
</TR>
<TR>
	<TD>
		MANUCHIHR 
	</TD>
	 <TD>
		<A HREF="#01.07.01.01.29" STYLE="font-weight:bold;">&nbsp; 2 ----------</A>
	</TD>
</TR>
<TR>
	<TD>
		MANUCHIHR 
	</TD>
	 <TD>
		<A HREF="#01.07.01.01.30" STYLE="font-weight:bold;">&nbsp; 1 -----</A>
	</TD>
</TR>
<TR>
	<TD>
		MANUCHIHR 
	</TD>
	 <TD>
		<A HREF="#01.07.01.01.31" STYLE="font-weight:bold;">&nbsp; 3 ---------------</A>
	</TD>
</TR>
<TR>
	<TD>
		NAWZAR 
	</TD>
	 <TD>
		<A HREF="#01.08.01.01.04" STYLE="font-weight:bold;">&nbsp; 1 -----</A>
	</TD>
</TR>
<TR>
	<TD>
		NAWZAR 
	</TD>
	 <TD>
		<A HREF="#01.08.01.01.11" STYLE="font-weight:bold;">&nbsp; 2 ----------</A>
	</TD>
</TR>
<TR>
	<TD>
		ZAV 
	</TD>
	 <TD>
		<A HREF="#01.09.01.01.01" STYLE="font-weight:bold;">&nbsp; 2 ----------</A>
	</TD>
</TR>
<TR>
	<TD>
		 GARSHASP 
	</TD>
	 <TD>
		<A HREF="#01.10.01.01.02" STYLE="font-weight:bold;">&nbsp; 4 --------------------</A>
	</TD>
</TR>
<TR>
	<TD>
		 GARSHASP 
	</TD>
	 <TD>
		<A HREF="#01.10.01.01.04" STYLE="font-weight:bold;">&nbsp; 1 -----</A>
	</TD>
</TR>
<TR>
	<TD>
		KAY QUBAD 
	</TD>
	 <TD>
		<A HREF="#02.01.01.01.02" STYLE="font-weight:bold;">&nbsp; 1 -----</A>
	</TD>
</TR>
<TR>
	<TD>
		KAI KA'US 
	</TD>
	 <TD>
		<A HREF="#02.02.01.02.01" STYLE="font-weight:bold;">&nbsp; 2 ----------</A>
	</TD>
</TR>
<TR>
	<TD>
		KAI KA'US 
	</TD>
	 <TD>
		<A HREF="#02.02.01.02.03" STYLE="font-weight:bold;">&nbsp; 3 ---------------</A>
	</TD>
</TR>
<TR>
	<TD>
		KAI KA'US 
	</TD>
	 <TD>
		<A HREF="#02.02.01.02.04" STYLE="font-weight:bold;">&nbsp; 2 ----------</A>
	</TD>
</TR>
<TR>
	<TD>
		KAI KA'US 
	</TD>
	 <TD>
		<A HREF="#02.02.01.02.05" STYLE="font-weight:bold;">&nbsp; 1 -----</A>
	</TD>
</TR>
<TR>
	<TD>
		KAI KA'US 
	</TD>
	 <TD>
		<A HREF="#02.02.01.02.06" STYLE="font-weight:bold;">&nbsp; 2 ----------</A>
	</TD>
</TR>
<TR>
	<TD>
		KAI KA'US 
	</TD>
	 <TD>
		<A HREF="#02.02.01.02.07" STYLE="font-weight:bold;">&nbsp; 6 ------------------------------</A>
	</TD>
</TR>
<TR>
	<TD>
		KAI KA'US 
	</TD>
	 <TD>
		<A HREF="#02.02.01.02.10" STYLE="font-weight:bold;">&nbsp; 1 -----</A>
	</TD>
</TR>
<TR>
	<TD>
		KAI KA'US 
	</TD>
	 <TD>
		<A HREF="#02.02.01.02.11" STYLE="font-weight:bold;">&nbsp; 1 -----</A>
	</TD>
</TR>
<TR>
	<TD>
		KAI KA'US 
	</TD>
	 <TD>
		<A HREF="#02.02.02.01.09" STYLE="font-weight:bold;">&nbsp; 3 ---------------</A>
	</TD>
</TR>
<TR>
	<TD>
		KAI KA'US 
	</TD>
	 <TD>
		<A HREF="#02.02.03.01.04" STYLE="font-weight:bold;">&nbsp; 1 -----</A>
	</TD>
</TR>
<TR>
	<TD>
		KAI KA'US 
	</TD>
	 <TD>
		<A HREF="#02.02.03.01.09" STYLE="font-weight:bold;">&nbsp; 1 -----</A>
	</TD>
</TR>
<TR>
	<TD>
		KAI KA'US 
	</TD>
	 <TD>
		<A HREF="#02.02.03.01.16" STYLE="font-weight:bold;">&nbsp; 1 -----</A>
	</TD>
</TR>
<TR>
	<TD>
		KAI KA'US 
	</TD>
	 <TD>
		<A HREF="#02.02.03.01.20" STYLE="font-weight:bold;">&nbsp; 1 -----</A>
	</TD>
</TR>
<TR>
	<TD>
		KAI KA'US 
	</TD>
	 <TD>
		<A HREF="#02.02.03.01.21" STYLE="font-weight:bold;">&nbsp; 1 -----</A>
	</TD>
</TR>
<TR>
	<TD>
		KAI KA'US 
	</TD>
	 <TD>
		<A HREF="#02.02.03.01.23" STYLE="font-weight:bold;">&nbsp; 4 --------------------</A>
	</TD>
</TR>
<TR>
	<TD>
		KAI KA'US 
	</TD>
	 <TD>
		<A HREF="#02.02.03.01.24" STYLE="font-weight:bold;">&nbsp; 1 -----</A>
	</TD>
</TR>
<TR>
	<TD>
		KAI KA'US 
	</TD>
	 <TD>
		<A HREF="#02.02.04.01.02" STYLE="font-weight:bold;">&nbsp; 1 -----</A>
	</TD>
</TR>
<TR>
	<TD>
		KAI KA'US 
	</TD>
	 <TD>
		<A HREF="#02.02.04.01.13" STYLE="font-weight:bold;">&nbsp; 3 ---------------</A>
	</TD>
</TR>
<TR>
	<TD>
		KAI KA'US 
	</TD>
	 <TD>
		<A HREF="#02.02.04.01.33" STYLE="font-weight:bold;">&nbsp; 2 ----------</A>
	</TD>
</TR>
<TR>
	<TD>
		KAI KA'US 
	</TD>
	 <TD>
		<A HREF="#02.02.04.01.34" STYLE="font-weight:bold;">&nbsp; 1 -----</A>
	</TD>
</TR>
<TR>
	<TD>
		KAI KA'US 
	</TD>
	 <TD>
		<A HREF="#02.02.04.01.37" STYLE="font-weight:bold;">&nbsp; 1 -----</A>
	</TD>
</TR>
<TR>
	<TD>
		KAI KA'US 
	</TD>
	 <TD>
		<A HREF="#02.02.04.01.44" STYLE="font-weight:bold;">&nbsp; 1 -----</A>
	</TD>
</TR>
<TR>
	<TD>
		KAI KA'US 
	</TD>
	 <TD>
		<A HREF="#02.02.04.01.54" STYLE="font-weight:bold;">&nbsp; 1 -----</A>
	</TD>
</TR>
<TR>
	<TD>
		KAI KA'US 
	</TD>
	 <TD>
		<A HREF="#02.02.04.01.56" STYLE="font-weight:bold;">&nbsp; 4 --------------------</A>
	</TD>
</TR>
<TR>
	<TD>
		KAI KA'US 
	</TD>
	 <TD>
		<A HREF="#02.02.05.01.08" STYLE="font-weight:bold;">&nbsp; 1 -----</A>
	</TD>
</TR>
<TR>
	<TD>
		KAI KA'US 
	</TD>
	 <TD>
		<A HREF="#02.02.05.01.09" STYLE="font-weight:bold;">&nbsp; 1 -----</A>
	</TD>
</TR>
<TR>
	<TD>
		KAI KA'US 
	</TD>
	 <TD>
		<A HREF="#02.02.05.01.17" STYLE="font-weight:bold;">&nbsp; 1 -----</A>
	</TD>
</TR>
<TR>
	<TD>
		KAI KA'US 
	</TD>
	 <TD>
		<A HREF="#02.02.05.01.19" STYLE="font-weight:bold;">&nbsp; 1 -----</A>
	</TD>
</TR>
<TR>
	<TD>
		KAI KA'US 
	</TD>
	 <TD>
		<A HREF="#02.02.05.01.24" STYLE="font-weight:bold;">&nbsp; 1 -----</A>
	</TD>
</TR>
<TR>
	<TD>
		KAI KA'US 
	</TD>
	 <TD>
		<A HREF="#02.02.05.01.28" STYLE="font-weight:bold;">&nbsp; 1 -----</A>
	</TD>
</TR>
<TR>
	<TD>
		KAI KA'US 
	</TD>
	 <TD>
		<A HREF="#02.02.05.01.35" STYLE="font-weight:bold;">&nbsp; 1 -----</A>
	</TD>
</TR>
<TR>
	<TD>
		KAY KHUSRAW 
	</TD>
	 <TD>
		<A HREF="#02.03.01.01.02" STYLE="font-weight:bold;">&nbsp; 1 -----</A>
	</TD>
</TR>
<TR>
	<TD>
		KAY KHUSRAW 
	</TD>
	 <TD>
		<A HREF="#02.03.01.02.13" STYLE="font-weight:bold;">&nbsp; 1 -----</A>
	</TD>
</TR>
<TR>
	<TD>
		KAY KHUSRAW 
	</TD>
	 <TD>
		<A HREF="#02.03.01.02.17" STYLE="font-weight:bold;">&nbsp; 1 -----</A>
	</TD>
</TR>
<TR>
	<TD>
		KAY KHUSRAW 
	</TD>
	 <TD>
		<A HREF="#02.03.02.01.10" STYLE="font-weight:bold;">&nbsp; 1 -----</A>
	</TD>
</TR>
<TR>
	<TD>
		KAY KHUSRAW 
	</TD>
	 <TD>
		<A HREF="#02.03.02.01.28" STYLE="font-weight:bold;">&nbsp; 3 ---------------</A>
	</TD>
</TR>
<TR>
	<TD>
		KAY KHUSRAW 
	</TD>
	 <TD>
		<A HREF="#02.03.02.01.32" STYLE="font-weight:bold;">&nbsp; 3 ---------------</A>
	</TD>
</TR>
<TR>
	<TD>
		KAY KHUSRAW 
	</TD>
	 <TD>
		<A HREF="#02.03.03.01.02" STYLE="font-weight:bold;">&nbsp; 4 --------------------</A>
	</TD>
</TR>
<TR>
	<TD>
		KAY KHUSRAW 
	</TD>
	 <TD>
		<A HREF="#02.03.03.01.05" STYLE="font-weight:bold;">&nbsp; 1 -----</A>
	</TD>
</TR>
<TR>
	<TD>
		KAY KHUSRAW 
	</TD>
	 <TD>
		<A HREF="#02.03.03.01.23" STYLE="font-weight:bold;">&nbsp; 1 -----</A>
	</TD>
</TR>
<TR>
	<TD>
		KAY KHUSRAW 
	</TD>
	 <TD>
		<A HREF="#02.03.03.01.25" STYLE="font-weight:bold;">&nbsp; 2 ----------</A>
	</TD>
</TR>
<TR>
	<TD>
		KAY KHUSRAW 
	</TD>
	 <TD>
		<A HREF="#02.03.04.01.04" STYLE="font-weight:bold;">&nbsp; 1 -----</A>
	</TD>
</TR>
<TR>
	<TD>
		KAY KHUSRAW 
	</TD>
	 <TD>
		<A HREF="#02.03.04.01.05" STYLE="font-weight:bold;">&nbsp; 1 -----</A>
	</TD>
</TR>
<TR>
	<TD>
		KAY KHUSRAW 
	</TD>
	 <TD>
		<A HREF="#02.03.05.01.21" STYLE="font-weight:bold;">&nbsp; 1 -----</A>
	</TD>
</TR>
<TR>
	<TD>
		KAY KHUSRAW 
	</TD>
	 <TD>
		<A HREF="#02.03.05.01.24" STYLE="font-weight:bold;">&nbsp; 3 ---------------</A>
	</TD>
</TR>
<TR>
	<TD>
		KAY KHUSRAW 
	</TD>
	 <TD>
		<A HREF="#02.03.05.01.26" STYLE="font-weight:bold;">&nbsp; 1 -----</A>
	</TD>
</TR>
<TR>
	<TD>
		KAY KHUSRAW 
	</TD>
	 <TD>
		<A HREF="#02.03.06.01.04" STYLE="font-weight:bold;">&nbsp; 1 -----</A>
	</TD>
</TR>
<TR>
	<TD>
		KAY KHUSRAW 
	</TD>
	 <TD>
		<A HREF="#02.03.06.01.08" STYLE="font-weight:bold;">&nbsp; 1 -----</A>
	</TD>
</TR>
<TR>
	<TD>
		KAY KHUSRAW 
	</TD>
	 <TD>
		<A HREF="#02.03.06.01.16" STYLE="font-weight:bold;">&nbsp; 1 -----</A>
	</TD>
</TR>
<TR>
	<TD>
		KAY KHUSRAW 
	</TD>
	 <TD>
		<A HREF="#02.03.06.01.25" STYLE="font-weight:bold;">&nbsp; 1 -----</A>
	</TD>
</TR>
<TR>
	<TD>
		KAY KHUSRAW 
	</TD>
	 <TD>
		<A HREF="#02.03.06.01.30" STYLE="font-weight:bold;">&nbsp; 1 -----</A>
	</TD>
</TR>
<TR>
	<TD>
		KAY KHUSRAW 
	</TD>
	 <TD>
		<A HREF="#02.03.06.01.39" STYLE="font-weight:bold;">&nbsp; 1 -----</A>
	</TD>
</TR>
<TR>
	<TD>
		KAY KHUSRAW 
	</TD>
	 <TD>
		<A HREF="#02.03.06.01.47" STYLE="font-weight:bold;">&nbsp; 1 -----</A>
	</TD>
</TR>
<TR>
	<TD>
		KAY KHUSRAW 
	</TD>
	 <TD>
		<A HREF="#02.03.07.01.10" STYLE="font-weight:bold;">&nbsp; 2 ----------</A>
	</TD>
</TR>
<TR>
	<TD>
		KAY KHUSRAW 
	</TD>
	 <TD>
		<A HREF="#02.03.07.01.36" STYLE="font-weight:bold;">&nbsp; 1 -----</A>
	</TD>
</TR>
<TR>
	<TD>
		KAY KHUSRAW 
	</TD>
	 <TD>
		<A HREF="#02.03.07.01.44" STYLE="font-weight:bold;">&nbsp; 2 ----------</A>
	</TD>
</TR>
<TR>
	<TD>
		KAY KHUSRAW 
	</TD>
	 <TD>
		<A HREF="#02.03.07.01.56" STYLE="font-weight:bold;">&nbsp; 1 -----</A>
	</TD>
</TR>
<TR>
	<TD>
		LUHRASP 
	</TD>
	 <TD>
		<A HREF="#02.04.01.01.01" STYLE="font-weight:bold;">&nbsp; 1 -----</A>
	</TD>
</TR>
<TR>
	<TD>
		LUHRASP 
	</TD>
	 <TD>
		<A HREF="#02.04.01.01.10" STYLE="font-weight:bold;">&nbsp; 1 -----</A>
	</TD>
</TR>
<TR>
	<TD>
		LUHRASP 
	</TD>
	 <TD>
		<A HREF="#02.04.01.01.12" STYLE="font-weight:bold;">&nbsp; 1 -----</A>
	</TD>
</TR>
<TR>
	<TD>
		LUHRASP 
	</TD>
	 <TD>
		<A HREF="#02.04.01.01.13" STYLE="font-weight:bold;">&nbsp; 1 -----</A>
	</TD>
</TR>
<TR>
	<TD>
		GUSHTASP 
	</TD>
	 <TD>
		<A HREF="#02.05.01.01.17" STYLE="font-weight:bold;">&nbsp; 2 ----------</A>
	</TD>
</TR>
<TR>
	<TD>
		GUSHTASP 
	</TD>
	 <TD>
		<A HREF="#02.05.01.01.33" STYLE="font-weight:bold;">&nbsp; 1 -----</A>
	</TD>
</TR>
<TR>
	<TD>
		GUSHTASP 
	</TD>
	 <TD>
		<A HREF="#02.05.02.01.01" STYLE="font-weight:bold;">&nbsp; 1 -----</A>
	</TD>
</TR>
<TR>
	<TD>
		GUSHTASP 
	</TD>
	 <TD>
		<A HREF="#02.05.02.01.03" STYLE="font-weight:bold;">&nbsp; 2 ----------</A>
	</TD>
</TR>
<TR>
	<TD>
		GUSHTASP 
	</TD>
	 <TD>
		<A HREF="#02.05.02.01.05" STYLE="font-weight:bold;">&nbsp; 2 ----------</A>
	</TD>
</TR>
<TR>
	<TD>
		GUSHTASP 
	</TD>
	 <TD>
		<A HREF="#02.05.02.02.01" STYLE="font-weight:bold;">&nbsp; 1 -----</A>
	</TD>
</TR>
<TR>
	<TD>
		GUSHTASP 
	</TD>
	 <TD>
		<A HREF="#02.05.02.02.07" STYLE="font-weight:bold;">&nbsp; 1 -----</A>
	</TD>
</TR>
<TR>
	<TD>
		GUSHTASP 
	</TD>
	 <TD>
		<A HREF="#02.05.03.01.02" STYLE="font-weight:bold;">&nbsp; 1 -----</A>
	</TD>
</TR>
<TR>
	<TD>
		GUSHTASP 
	</TD>
	 <TD>
		<A HREF="#02.05.03.01.07" STYLE="font-weight:bold;">&nbsp; 1 -----</A>
	</TD>
</TR>
<TR>
	<TD>
		GUSHTASP 
	</TD>
	 <TD>
		<A HREF="#02.05.03.01.08" STYLE="font-weight:bold;">&nbsp; 1 -----</A>
	</TD>
</TR>
<TR>
	<TD>
		GUSHTASP 
	</TD>
	 <TD>
		<A HREF="#02.05.03.01.11" STYLE="font-weight:bold;">&nbsp; 1 -----</A>
	</TD>
</TR>
<TR>
	<TD>
		GUSHTASP 
	</TD>
	 <TD>
		<A HREF="#02.05.03.01.15" STYLE="font-weight:bold;">&nbsp; 1 -----</A>
	</TD>
</TR>
<TR>
	<TD>
		GUSHTASP 
	</TD>
	 <TD>
		<A HREF="#02.05.03.01.21" STYLE="font-weight:bold;">&nbsp; 1 -----</A>
	</TD>
</TR>
<TR>
	<TD>
		GUSHTASP 
	</TD>
	 <TD>
		<A HREF="#02.05.03.01.22" STYLE="font-weight:bold;">&nbsp; 1 -----</A>
	</TD>
</TR>
<TR>
	<TD>
		GUSHTASP 
	</TD>
	 <TD>
		<A HREF="#02.05.03.01.26" STYLE="font-weight:bold;">&nbsp; 1 -----</A>
	</TD>
</TR>
<TR>
	<TD>
		GUSHTASP 
	</TD>
	 <TD>
		<A HREF="#02.05.03.01.27" STYLE="font-weight:bold;">&nbsp; 5 -------------------------</A>
	</TD>
</TR>
<TR>
	<TD>
		GUSHTASP 
	</TD>
	 <TD>
		<A HREF="#02.05.03.01.29" STYLE="font-weight:bold;">&nbsp; 1 -----</A>
	</TD>
</TR>
<TR>
	<TD>
		GUSHTASP 
	</TD>
	 <TD>
		<A HREF="#02.05.04.01.04" STYLE="font-weight:bold;">&nbsp; 4 --------------------</A>
	</TD>
</TR>
<TR>
	<TD>
		GUSHTASP 
	</TD>
	 <TD>
		<A HREF="#02.05.04.01.05" STYLE="font-weight:bold;">&nbsp; 1 -----</A>
	</TD>
</TR>
<TR>
	<TD>
		GUSHTASP 
	</TD>
	 <TD>
		<A HREF="#02.05.04.01.06" STYLE="font-weight:bold;">&nbsp; 1 -----</A>
	</TD>
</TR>
<TR>
	<TD>
		BAHMAN, SON OF ISFANDIYAR 
	</TD>
	 <TD>
		<A HREF="#02.06.01.01.03" STYLE="font-weight:bold;">&nbsp; 1 -----</A>
	</TD>
</TR>
<TR>
	<TD>
		HUMAY 
	</TD>
	 <TD>
		<A HREF="#02.07.01.01.03" STYLE="font-weight:bold;">&nbsp; 2 ----------</A>
	</TD>
</TR>
<TR>
	<TD>
		HUMAY 
	</TD>
	 <TD>
		<A HREF="#02.07.01.01.07" STYLE="font-weight:bold;">&nbsp; 1 -----</A>
	</TD>
</TR>
<TR>
	<TD>
		DARA 
	</TD>
	 <TD>
		<A HREF="#02.09.01.01.09" STYLE="font-weight:bold;">&nbsp; 2 ----------</A>
	</TD>
</TR>
<TR>
	<TD>
		SIKANDAR 
	</TD>
	 <TD>
		<A HREF="#02.10.01.01.01" STYLE="font-weight:bold;">&nbsp; 1 -----</A>
	</TD>
</TR>
<TR>
	<TD>
		SIKANDAR 
	</TD>
	 <TD>
		<A HREF="#02.10.01.01.05" STYLE="font-weight:bold;">&nbsp; 1 -----</A>
	</TD>
</TR>
<TR>
	<TD>
		SIKANDAR 
	</TD>
	 <TD>
		<A HREF="#02.10.01.01.15" STYLE="font-weight:bold;">&nbsp; 2 ----------</A>
	</TD>
</TR>
<TR>
	<TD>
		SIKANDAR 
	</TD>
	 <TD>
		<A HREF="#02.10.01.01.16" STYLE="font-weight:bold;">&nbsp; 2 ----------</A>
	</TD>
</TR>
<TR>
	<TD>
		SIKANDAR 
	</TD>
	 <TD>
		<A HREF="#02.10.01.01.20" STYLE="font-weight:bold;">&nbsp; 1 -----</A>
	</TD>
</TR>
<TR>
	<TD>
		SIKANDAR 
	</TD>
	 <TD>
		<A HREF="#02.10.01.01.24" STYLE="font-weight:bold;">&nbsp; 1 -----</A>
	</TD>
</TR>
<TR>
	<TD>
		SIKANDAR 
	</TD>
	 <TD>
		<A HREF="#02.10.01.01.26" STYLE="font-weight:bold;">&nbsp; 1 -----</A>
	</TD>
</TR>
<TR>
	<TD>
		SIKANDAR 
	</TD>
	 <TD>
		<A HREF="#02.10.01.01.27" STYLE="font-weight:bold;">&nbsp; 2 ----------</A>
	</TD>
</TR>
<TR>
	<TD>
		SIKANDAR 
	</TD>
	 <TD>
		<A HREF="#02.10.01.01.29" STYLE="font-weight:bold;">&nbsp; 5 -------------------------</A>
	</TD>
</TR>
<TR>
	<TD>
		SIKANDAR 
	</TD>
	 <TD>
		<A HREF="#02.10.01.01.30" STYLE="font-weight:bold;">&nbsp; 1 -----</A>
	</TD>
</TR>
<TR>
	<TD>
		SIKANDAR 
	</TD>
	 <TD>
		<A HREF="#02.10.01.01.31" STYLE="font-weight:bold;">&nbsp; 1 -----</A>
	</TD>
</TR>
<TR>
	<TD>
		SIKANDAR 
	</TD>
	 <TD>
		<A HREF="#02.10.01.01.39" STYLE="font-weight:bold;">&nbsp; 2 ----------</A>
	</TD>
</TR>
<TR>
	<TD>
		 ARDISHIR 
	</TD>
	 <TD>
		<A HREF="#03.03.01.01.02" STYLE="font-weight:bold;">&nbsp; 2 ----------</A>
	</TD>
</TR>
<TR>
	<TD>
		 ARDISHIR 
	</TD>
	 <TD>
		<A HREF="#03.03.01.01.06" STYLE="font-weight:bold;">&nbsp; 2 ----------</A>
	</TD>
</TR>
<TR>
	<TD>
		 ARDISHIR 
	</TD>
	 <TD>
		<A HREF="#04.01.01.01.02" STYLE="font-weight:bold;">&nbsp; 1 -----</A>
	</TD>
</TR>
<TR>
	<TD>
		 ARDISHIR 
	</TD>
	 <TD>
		<A HREF="#04.01.01.01.07" STYLE="font-weight:bold;">&nbsp; 1 -----</A>
	</TD>
</TR>
<TR>
	<TD>
		 ARDISHIR 
	</TD>
	 <TD>
		<A HREF="#04.01.01.01.09" STYLE="font-weight:bold;">&nbsp; 1 -----</A>
	</TD>
</TR>
<TR>
	<TD>
		BAHRAM BAHRAMIYAN 
	</TD>
	 <TD>
		<A HREF="#04.06.01.01.01" STYLE="font-weight:bold;">&nbsp; 1 -----</A>
	</TD>
</TR>
<TR>
	<TD>
		SHAPUR SURNAMED ZU'L AKTAF 
	</TD>
	 <TD>
		<A HREF="#04.09.01.01.04" STYLE="font-weight:bold;">&nbsp; 1 -----</A>
	</TD>
</TR>
<TR>
	<TD>
		SHAPUR SURNAMED ZU'L AKTAF 
	</TD>
	 <TD>
		<A HREF="#04.09.01.01.08" STYLE="font-weight:bold;">&nbsp; 1 -----</A>
	</TD>
</TR>
<TR>
	<TD>
		SHAPUR SURNAMED ZU'L AKTAF 
	</TD>
	 <TD>
		<A HREF="#04.09.01.01.12" STYLE="font-weight:bold;">&nbsp; 1 -----</A>
	</TD>
</TR>
<TR>
	<TD>
		YAZDIGIRD, SON OF SHAPUR 
	</TD>
	 <TD>
		<A HREF="#04.13.01.01.04" STYLE="font-weight:bold;">&nbsp; 2 ----------</A>
	</TD>
</TR>
<TR>
	<TD>
		YAZDIGIRD, SON OF SHAPUR 
	</TD>
	 <TD>
		<A HREF="#04.13.01.01.07" STYLE="font-weight:bold;">&nbsp; 1 -----</A>
	</TD>
</TR>
<TR>
	<TD>
		BAHRAM GUR 
	</TD>
	 <TD>
		<A HREF="#04.14.01.01.10" STYLE="font-weight:bold;">&nbsp; 1 -----</A>
	</TD>
</TR>
<TR>
	<TD>
		BAHRAM GUR 
	</TD>
	 <TD>
		<A HREF="#04.14.01.01.12" STYLE="font-weight:bold;">&nbsp; 4 --------------------</A>
	</TD>
</TR>
<TR>
	<TD>
		BAHRAM GUR 
	</TD>
	 <TD>
		<A HREF="#04.14.01.01.16" STYLE="font-weight:bold;">&nbsp; 1 -----</A>
	</TD>
</TR>
<TR>
	<TD>
		BAHRAM GUR 
	</TD>
	 <TD>
		<A HREF="#04.14.01.01.17" STYLE="font-weight:bold;">&nbsp; 3 ---------------</A>
	</TD>
</TR>
<TR>
	<TD>
		BAHRAM GUR 
	</TD>
	 <TD>
		<A HREF="#04.14.01.01.18" STYLE="font-weight:bold;">&nbsp; 2 ----------</A>
	</TD>
</TR>
<TR>
	<TD>
		BAHRAM GUR 
	</TD>
	 <TD>
		<A HREF="#04.14.01.01.20" STYLE="font-weight:bold;">&nbsp; 1 -----</A>
	</TD>
</TR>
<TR>
	<TD>
		BAHRAM GUR 
	</TD>
	 <TD>
		<A HREF="#04.14.01.01.22" STYLE="font-weight:bold;">&nbsp; 1 -----</A>
	</TD>
</TR>
<TR>
	<TD>
		BAHRAM GUR 
	</TD>
	 <TD>
		<A HREF="#04.14.01.01.28" STYLE="font-weight:bold;">&nbsp; 1 -----</A>
	</TD>
</TR>
<TR>
	<TD>
		BAHRAM GUR 
	</TD>
	 <TD>
		<A HREF="#04.14.01.01.30" STYLE="font-weight:bold;">&nbsp; 1 -----</A>
	</TD>
</TR>
<TR>
	<TD>
		BAHRAM GUR 
	</TD>
	 <TD>
		<A HREF="#04.14.01.01.31" STYLE="font-weight:bold;">&nbsp; 3 ---------------</A>
	</TD>
</TR>
<TR>
	<TD>
		BAHRAM GUR 
	</TD>
	 <TD>
		<A HREF="#04.14.01.01.32" STYLE="font-weight:bold;">&nbsp; 1 -----</A>
	</TD>
</TR>
<TR>
	<TD>
		NUSHIRVAN 
	</TD>
	 <TD>
		<A HREF="#04.20.01.01.07" STYLE="font-weight:bold;">&nbsp; 1 -----</A>
	</TD>
</TR>
<TR>
	<TD>
		NUSHIRVAN 
	</TD>
	 <TD>
		<A HREF="#04.20.02.01.01" STYLE="font-weight:bold;">&nbsp; 1 -----</A>
	</TD>
</TR>
<TR>
	<TD>
		NUSHIRVAN 
	</TD>
	 <TD>
		<A HREF="#04.20.02.01.06" STYLE="font-weight:bold;">&nbsp; 1 -----</A>
	</TD>
</TR>
<TR>
	<TD>
		NUSHIRVAN 
	</TD>
	 <TD>
		<A HREF="#04.20.02.01.08" STYLE="font-weight:bold;">&nbsp; 1 -----</A>
	</TD>
</TR>
<TR>
	<TD>
		NUSHIRVAN 
	</TD>
	 <TD>
		<A HREF="#04.20.03.01.02" STYLE="font-weight:bold;">&nbsp; 1 -----</A>
	</TD>
</TR>
<TR>
	<TD>
		NUSHIRVAN 
	</TD>
	 <TD>
		<A HREF="#04.20.03.01.07" STYLE="font-weight:bold;">&nbsp; 1 -----</A>
	</TD>
</TR>
<TR>
	<TD>
		NUSHIRVAN 
	</TD>
	 <TD>
		<A HREF="#04.20.03.01.09" STYLE="font-weight:bold;">&nbsp; 1 -----</A>
	</TD>
</TR>
<TR>
	<TD>
		NUSHIRVAN 
	</TD>
	 <TD>
		<A HREF="#04.20.04.01.01" STYLE="font-weight:bold;">&nbsp; 1 -----</A>
	</TD>
</TR>
<TR>
	<TD>
		NUSHIRVAN 
	</TD>
	 <TD>
		<A HREF="#04.20.06.01.02" STYLE="font-weight:bold;">&nbsp; 1 -----</A>
	</TD>
</TR>
<TR>
	<TD>
		HURMAZD , SON OF NUSHIRVAN 
	</TD>
	 <TD>
		<A HREF="#04.21.01.01.12" STYLE="font-weight:bold;">&nbsp; 1 -----</A>
	</TD>
</TR>
<TR>
	<TD>
		KHUSRAW PARWIZ 
	</TD>
	 <TD>
		<A HREF="#04.22.01.01.05" STYLE="font-weight:bold;">&nbsp; 1 -----</A>
	</TD>
</TR>
<TR>
	<TD>
		KHUSRAW PARWIZ 
	</TD>
	 <TD>
		<A HREF="#04.22.01.01.24" STYLE="font-weight:bold;">&nbsp; 1 -----</A>
	</TD>
</TR>
<TR>
	<TD>
		KHUSRAW PARWIZ 
	</TD>
	 <TD>
		<A HREF="#04.22.01.01.37" STYLE="font-weight:bold;">&nbsp; 1 -----</A>
	</TD>
</TR>
<TR>
	<TD>
		KHUSRAW PARWIZ 
	</TD>
	 <TD>
		<A HREF="#04.22.02.01.02" STYLE="font-weight:bold;">&nbsp; 2 ----------</A>
	</TD>
</TR>
<TR>
	<TD>
		QUBAD (COMMONLY CALLED SHIRUY) 
	</TD>
	 <TD>
		<A HREF="#04.23.01.01.01" STYLE="font-weight:bold;">&nbsp; 1 -----</A>
	</TD>
</TR>

	</table>
	</div><!-- container -->


<!-- End Document
================================================== -->
</body>
</html>{/mask}
