{mask:main}<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html lang="en"> <!--<![endif]-->
<head>

	<!-- Basic Page Needs
  ================================================== -->
	<meta charset="utf-8">
	<title>{header}</title>
	<meta name="description" content="">
	<meta name="author" content="">

	<!-- Mobile Specific Metas
  ================================================== -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<!-- CSS
  ================================================== -->

	<link rel="stylesheet" href="css/{css}">	

	<!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

	<!-- Favicons
	================================================== -->
	<link rel="shortcut icon" href="images/favicon.ico">

<style>
td {
  border:solid 1px black;
}
.content {
  width:700px;
  margin:0 auto;
}

</style>

</head>
<body>



	<div class="content" style="border:solid 1px green;">

    <table width="700">
    <tr>
        <td>
	<h3>Illustrations by Manuscript</h3>
	<hr />
	<a href="">Contact sheet of all images</a>
	<hr />
	<a href="">Full bibliographical information</a>
	<hr />
	<a href="">Garrett 56</a><br />
	<a href="">Garrett 57</a><br />
	<a href="">Garrett 58</a><br />
	<a href="">Garrett 59</a><br />
	<a href="">Peck 310</a><br />
	<hr />
        </td>
	
	<td>
	<img src="images/banner_indices.jpg"/>
	<h2>Introduction for MANUSCRIPTS</h2>
	<p>Welcome</p>

	</td>
    </tr>
   </table>


	</div><!-- container -->


<!-- End Document
================================================== -->
</body>
</html>{/mask}
