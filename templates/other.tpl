{mask:main}<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html lang="en"> <!--<![endif]-->
<head>

	<!-- Basic Page Needs
  ================================================== -->
	<meta charset="utf-8">
	<title>{header}</title>
	<meta name="description" content="">
	<meta name="author" content="">

	<!-- Mobile Specific Metas
  ================================================== -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<!-- CSS
  ================================================== -->

	<link rel="stylesheet" href="css/{css}">	

	<!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

	<!-- Favicons
	================================================== -->
	<link rel="shortcut icon" href="images/favicon.ico">

<style>
td {
  border:solid 1px black;
}
.content {
  width:700px;
  margin:0 auto;
}

</style>

</head>
<body>



	<div class="content" style="border:solid 1px green;">

    <table border="0" width="700">
    <tr>
    <td>
    <a href="#"  onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image1','','images/banner.gif',1)">
    <img src="images/banner.gif"  NAME="Image1" BORDER="0" usemap="#Map"></img>
    </a>
        <map name="Map">
    <area shape="rect" coords="19,88,150,115" href="index.php?a=manuscript"  onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image1','','images/mss.gif',1)"></area>
    <area shape="rect" coords="151,88,282,114" href="javascript: ill()"  onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image1','','images/ill.gif',1)"></area>
    <area shape="rect" coords="281,89,408,118" href="indindex.htm"  onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image1','','images/ind.gif',1)"></area>
    <area shape="rect" coords="409,89,536,117" href="start.epl"  onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image1','','images/help.gif',1)"></area>
          <area shape="rect" coords="536,89,667,116" href="start.epl"  onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image1','','images/home.gif',1)"></area>
        </map>
      </td>
    </tr>
  </table>



  <table width="700" cellpadding="5" cellspacing="0" border="1" >
    <tr>
      <td valign="middle">
        <img border="0" align="left" valign="top" width="150" src="images/first.jpg" />
      </td>
      <td colspan="2">
        <div align="right"><h1>About this site</h1></div>
        <p>This website is an archive of book paintings--commonly known as <i>Persian Miniatures</i>--that were created to
        illustrate scenes from the Persian national epic, the <i>Shahnama</i> (the <i>Book of Kings</i>). 
				The <i>Shahnama</i> is a poem of some 50,000 couplets that was composed by Abu'l Qasim Firdausi over a period of
				several decades in the late tenth and early eleventh centuries. The core of this archive is a fund of 277 illustrations from five
        illustrated manuscripts of the <i>Shahnama</i> that are housed in
        Princeton University's Firestone Library and which were bequeathed to Princeton by Clara S. Peck and by Robert Garrett (Class of 1897). These manuscripts date from
        1544 to 1674 AD, and vary a good deal both in the number and quality
        of paintings each contains, and in the scenes chosen for
        illustration.</p>
      </td>
    </tr>
  </table>

  <table width="700" cellpadding="5" cellspacing="0">
    <tr>
      <td align="center"><img src="images/separator2.gif"></img></td>
    </tr>
	</table>

	<table width="700" cellpadding="5" cellspacing="0">
    <tr>
      <td colspan="2">
        <h1>How to view the material</h1>
        <a href="javascript:mss()"><img src="images/manuscripts.gif"></img></a> links to a page of the paintings in individual manuscripts.<br />
        <a href="javascript:ill()"><img src="images/illustrations.gif"></img></a> links to a page where all the
        illustrated scenes in the <i>Shahnama</i> are listed in sequence by
        Shah.  There you can get all the illustrations from a specific section
        of the text.<br />
        <!-- <a href="indindex.htm"><img src="images/indices.gif"></img></a> links to a thematic index of recurrent features of the paintings.<br /> -->
        <!-- <a href="start.epl"><img src="images/home.gif"></img></a> returns you
        to the this page from which you may access the supplementary materials
        described below.<br /> -->
      </td>
      <td valign="middle">
    <img src="images/second.jpg"  width="150" align="right"></img>
    </td>
    </tr>
    </table>
    
    <table width="700" cellpadding="5" cellspacing="0">
    <tr>
    <td align="center">
    <img src="images/separator.gif"></img></td>
    </tr>
    </table>
    
    <table width="700" cellpadding="5" cellspacing="0">
    <tr>
    <td valign="middle">
        <img src="images/feast1.jpg"  width="150" align="left"></img>
      </td>
      <td colspan="2">
        <div align="right"><h1>Supplementary Material</h1></div>
        <ul>
          <li>A brief <a href="http://www.princeton.edu/~jwc/shahnameh/intro.htm">introduction</A> to the <i>Shahnama</i> for those new to the work.</li>
          <!-- <li><a href="test977.epl"  target="_blank">The Ilkhanid (Thumbnails) </a><img src="images/new1.gif"></img></li> -->
          <li>Warner & Warner Texts:</li>
          <ul>
            <li><a href="v1.htm"  target="_blank">Vol 1</a></li>
            <li><a href="v2.htm"  target="_blank">Vol 2</a></li>
            <li><a href="v3.htm"  target="_blank">Vol 3</a></li>
            <li><a href="v4.htm"  target="_blank">Vol 4</a></li>
            <li><a href="v5.htm"  target="_blank">Vol 5</a></li>
          </ul>
    <li>
    <a href="/alvaradodev/shahnameh/v6/index.epl?pg=special%2fview-shah-episode-page">View of All Available Manuscript Pages, Listed by Dynasty, Shah and Episode.</a> Here's a  <a href="mspages-view-STATIC.html">cached view</a> which runs faster.
						<!--
						<a href="aa_test.epl"  target="_blank">Table of Warner Episodes.</a> The episodes are arranged by Shah.
            <br>Clicking on the EPISODE TITLE will launch a table of the text with the attendant illustrations.
						-->
            <li>A list of all <a href="http://www.princeton.edu/~jwc/shahnameh/pls.htm">illustrated scenes</A> together with the location of those scenes in standard editions and translations of the <I>Shahnama.</I></li>
            <li>An extensive <a href="http://www.princeton.edu/~jwc/shahnameh/bib.htm">bibliography.</A></li>
            <li>Links to other <a href="http://www.princeton.edu/~jwc/shahnameh/shxlnx.htm">online resources.</A> </li>
          </ul>
        </td>
      </tr>
    </table>
    
    <table width="700" cellpadding="5" cellspacing="0">
      <tr>
        <td align="center"><img src="images/separator2.gif"></img></td>
      </tr>
		</table>

		<table width="700" cellpadding="5" cellspacing="0">
      <tr>
        <td colspan="2" valign="top">
        <h1>Technical Information</h1>
					<p>This site presents one set of views of an Oracle database that contains several hundred images of illustrated manuscript pages coupled with contextual information. We employ a data model that captures four kinds of objects: (1) the historical narrative of dynasties, shahs, and episodes; (2) the scenes that define illustration types; (3) manuscripts, comprised of folios and sides (i.e. pages); and (4) and digital media files. Because these data are stored in a relational database, and not a hypertext, it is easy to create queries that data from different objects, to produce hybrid tables that can serve as the basis for a variety of hypertext views (e.g. <a href="mspages-view-STATIC.html">view of pages by episode</a>). <!-- <a href="example.txt">Perl scripts</a> submit the sql queries and format the output that produces the list of Warner episodes, for example. <a href="test22.epl">Click here</a> to run the script. For an detailed introduction to the interface either scroll through the panels below or click here: <a href="interface.htm">interface information</a>. Click here for database and programming <a href="tech.htm">technical information.</a>-->
					</p>
        </td> 
        <td>
          <img src="images/fifth.jpg"  width="150" align="right"></img>
        </td>
      </tr>
    </table>
    
    <table width="700" cellpadding="5" cellspacing="0">
      <tr>
        <td align="center"><img src="images/separator.gif"></img></td>
      </tr>
		</table>

	<!--
		<table width="700" cellpadding="5" cellspacing="0">
      <tr>
        <td>
          <img width="150" src="images/sixth.jpg">
        </td>
        <td colspan="2">
          <p>At present, the database includes 277 images and contextual information embedded in an Oracle database. The data structure operates on <a href="levels.htm">four levels</a>: the episode, the scene, the manuscript page and the file location. To see the entire datastructure,
          <a href="bigmap.jpg">click here</a>. <a href="query.jpg">Queries</a> can cut across data tables, making it possible to divide data for data entry and to pick from various tables to create combined tables. <a href="example.txt">Perl scripts</a> submit the sql queries and format the output that produces the list of Warner episodes, for example. <a href="test22.epl">Click here</a> to run the script.
        </p>
        </td>
      </tr>
    </table>

    <table width="700" cellpadding="5" cellspacing="0">
      <tr>
        <td align="center"><img src="images/separator2.gif"></img></td>
	     </tr>
		</table>
	-->
		<table width="700" cellpadding="5" cellspacing="0">
      <tr>
        <td>
          <img src="images/feast2.jpg"  width="150"></img>
        </td>
        <td colspan="2" valign="top">
	<h1>The Development Team</h1>
<li><a href="mailto:jwc@princeton.edu">Prof. Jerome W. Clinton</a>, Dept of Near Eastern Studies, Princeton</li>
<li><a href="mailto:alvarado@princeton.edu">Dr. Rafael C. Alvarado,</a> Educational Technology Center, Princeton</li>
<li><a href="mailto:batke@princeton.edu">Dr. Peter Batke,</a> Educational Technology Center, Princeton</li>
<li><a href="mailto:abwright@princeton.edu">Audrey Wright, MLS,</a> Firestone Library, Princeton</li>
        </td>
<!--
        <td>
          <img src="images/feast3.jpg"  width="150"></img>
        </td>
-->
      </tr>
    </table>
    <table>
      <tr>
        <td>
<!--
          <hr width="700" align="left"></hr>
          <a href="register-user-form.epl">Please take a moment to register (optional)-but we would like to know our regular user.</a>
          <hr width="700" align="left"></hr>
          <p>Please send us comments and bug reports <a href="mailto:shahnama@princeton.edu">shahnama@princeton.edu</a></p>
          <hr width="700" align="left"></hr>
-->
          <p>Software currently maintained by: <a href="mailto:etcsys@princeton.edu">etcsys@princeton.edu</a></p>
          <hr width="700" align="left"></hr>
        </td>
      </tr>
    </table>
<table width="700"><tr><td>
<hr size="1" noshade>
&copy; 2002 Trustees of Princeton University
<hr size="1" noshade>
</td></tr></table>




	</div><!-- container -->


<!-- End Document
================================================== -->
</body>
</html>{/mask}
